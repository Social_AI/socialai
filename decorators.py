from ServiceUser.models import App_User,  Subscription
from django.shortcuts import  Http404,reverse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from functools import wraps

user_login_required = user_passes_test(lambda user: user.is_staff)
def staff_user_required(view_func):
    decorated_view_func = login_required(user_login_required(view_func))
    return decorated_view_func

def superuser_check(user):
    return user.is_superuser


super_login_required = user_passes_test(superuser_check)
def admin_user_required(view_func):
    decorated_view_func = login_required(super_login_required(view_func))
    return decorated_view_func


def valid_subscription_required(function , upgradeurl=None):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated and (User.objects.get(username=request.user.username).is_staff or User.objects.get(username=request.user.username).is_superuser):
            return function(request, *args, **kwargs)
        else:
            if not request.user.is_authenticated:

                return function(request, *args, **kwargs)
            else:
                try:
                    obj = App_User.objects.get(user_auth=request.user)
                    print(obj)
                    view_to_redirect = reverse('ServiceUser:accountlimit') or upgradeurl
                    if obj.subscription_state == Subscription._VALID \
                        and obj.total_users <= obj.user_subscription.user_range\
                        and obj.total_apps <= obj.user_subscription.app_limit:
                        print(obj.subscription_state)
                        return function(request, *args, **kwargs)
                    else:
                        print(view_to_redirect)
                        return HttpResponseRedirect(view_to_redirect)
                except App_User.DoesNotExist:
                    print(
                        "هیچ کاربر سرویس با مشخصات وارد شده یافت نگردید:" + "\n" + 'username:' + request.user.username)
                    raise Http404(
                        "هیچ واقعیتی با مشخصات زیر یافت نگردید:" + "\n" + 'username:' + request.user.username)
    return wrap