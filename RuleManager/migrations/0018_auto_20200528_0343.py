# Generated by Django 3.0.6 on 2020-05-27 23:13

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0017_auto_20200526_1723'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 5, 28, 3, 43, 37, 53703), help_text='تاریخ ایجاد'),
        ),
    ]
