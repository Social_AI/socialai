# Generated by Django 3.0.6 on 2020-05-29 14:46

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0032_auto_20200529_1754'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 5, 29, 19, 13, 54, 385210), help_text='تاریخ ایجاد'),
        ),
    ]
