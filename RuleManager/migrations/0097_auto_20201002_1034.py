# Generated by Django 2.2.7 on 2020-10-02 10:34

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0096_auto_20201002_0249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 10, 2, 10, 34, 27, 111539), help_text='تاریخ انتشار قانون را وارد کنید', verbose_name='تاریخ انتشار'),
        ),
    ]
