# Generated by Django 2.2.6 on 2020-04-29 21:49

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0015_auto_20200430_0214'),
    ]

    operations = [
        migrations.AddField(
            model_name='facts',
            name='domain',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='factdomian_field', to='RuleManager.Domain'),
        ),
        migrations.AddField(
            model_name='rules',
            name='domain',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='ruledomian_field', to='RuleManager.Domain'),
        ),
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 4, 30, 2, 19, 33, 30795), help_text='تاریخ ایجاد'),
        ),
    ]
