# Generated by Django 3.0.6 on 2020-07-15 20:42

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0059_auto_20200716_0106'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 16, 1, 12, 45, 804585), help_text='تاریخ انتشار قانون را وارد کنید', verbose_name='تاریخ انتشار'),
        ),
    ]
