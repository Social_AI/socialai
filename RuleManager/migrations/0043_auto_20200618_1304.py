# Generated by Django 3.0.6 on 2020-06-18 08:34

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0042_auto_20200608_2347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='brief',
            field=models.CharField(help_text='کد مخفف حوزه دانش را با حروف انگلیسی در حداکثر 20 حرف وارد نمایید', max_length=50, unique=True, verbose_name='کد حوزه دانش'),
        ),
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 6, 18, 13, 4, 11, 118761), help_text='تاریخ انتشار قانون را وارد کنید', verbose_name='تاریخ انتشار'),
        ),
    ]
