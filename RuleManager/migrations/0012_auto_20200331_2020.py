# Generated by Django 2.2.6 on 2020-03-31 15:50

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0011_auto_20200331_2005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rules',
            name='condition',
            field=models.TextField(help_text='شرط را وارد کنید'),
        ),
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 3, 31, 20, 20, 9, 314784), help_text='تاریخ ایجاد'),
        ),
    ]
