# Generated by Django 3.0.6 on 2020-05-28 13:23

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0024_auto_20200528_1739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 5, 28, 17, 53, 24, 872987), help_text='تاریخ ایجاد'),
        ),
    ]
