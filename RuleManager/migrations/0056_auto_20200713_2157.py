# Generated by Django 3.0.6 on 2020-07-13 17:27

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0055_auto_20200713_2150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 13, 21, 57, 13, 806890), help_text='تاریخ انتشار قانون را وارد کنید', verbose_name='تاریخ انتشار'),
        ),
    ]
