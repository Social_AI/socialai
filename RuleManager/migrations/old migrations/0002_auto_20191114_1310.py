# Generated by Django 2.2.5 on 2019-11-14 09:40

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rules',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 11, 14, 13, 10, 3, 990781), help_text='تاریخ ایجاد'),
        ),
    ]
