# Generated by Django 2.2.5 on 2019-10-03 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RuleManager', '0003_auto_20190927_2359'),
    ]

    operations = [
        migrations.AddField(
            model_name='rules',
            name='rule_type',
            field=models.CharField(choices=[('Y/N', 'Y/N'), ('EXP', 'EXP')], default='Y/N', max_length=3),
        ),
    ]
