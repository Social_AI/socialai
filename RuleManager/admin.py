from django.contrib import admin

from .models import Rules ,Facts, Domain

admin.site.register(Rules)
admin.site.register(Facts)
admin.site.register(Domain)