from django.urls import path, include
from . import forms, views
from django.views.generic.edit import CreateView
from RuleManager.models import Rules
from ServiceUser.UserManagement import logoutuser, appuser, loginuser

app_name = 'RuleManager'
urlpatterns = [
    # path('', views.index, name='index'),
    path('', views.index, name='index'),
    path('update_rule_list/', views.filterrules, name='update_rule_list'),
    path('update_fact_list/', views.filterfacts, name='update_fact_list'),
    path('loginuser/', loginuser.verifyuser, name='loginuser'),
    path('register/', appuser.register, name='register'),
    path('logoutuser/', logoutuser.finishconnection, name='logoutuser'),
    # path('create_supervisor/', supervisor.create_supervisor, name='create_supervisor'),
    # path('create_developer/', developer.create_developer, name='create_developer'),
    path('<int:rule_id>/', views.detail, name='detail'),
    path('create_condition/', forms.create_condition, name='create_condition'),
    path('facts/create_fact/', forms.create_fact_by_model, name='create_fact'),
    path('update_facts/', views.update_facts, name='update_facts'),
    path('update_domains/', views.update_domains, name='update_domains'),
    path('create_rule_by_model/', forms.create_rule_by_model, name='create_rule_by_model'),
    path ('create_rule_cbv/', CreateView.as_view(model=Rules, fields= ['condition','consequence','pub_date','rule_type'],
        template_name="RuleManager/create_rule_by_model.html", success_url ='index'), name="create_rule_cbv"),
    path('edit_rule/<int:rule_id>/', forms.edit_rule_by_index, name='edit_rule'),
    path('delete_rule/<int:rul_ind>/', views.delete_rule_by_index, name='delete_rule'),
    path('facts/', views.factlist, name='factlist'),
    path('facts/<int:fact_ind>/', views.factdetail, name='factdetail'),
    path('facts/edit_fact/<int:fact_ind>/', forms.edit_fact_by_index, name='edit_fact'),
    path('facts/delete_fact/<int:fact_ind>/', views.delete_fact_by_index, name='delete_fact'),
    path('domains/', views.domainlist, name='domainlist'),
    path('domains/<int:dom_ind>/', views.domaindetail, name='domaindetail'),
    path('domains/create_domain/', forms.create_domain_by_model, name='create_domain'),
    path('domains/edit_domain/<int:dom_ind>/', forms.edit_domain_by_index, name='edit_domain'),
    path('domains/delete_domain/<int:dom_ind>/', views.delete_domain_by_index, name='delete_domain'),
    path('expertsystem/', include('ExpertSystem.urls')),
    path('ExpertSystem/', include('ExpertSystem.urls')),
    path('loginuser/expertsystem/', include('ExpertSystem.urls')),
     path('loginuser/Expertsystem/', include('ExpertSystem.urls')),
    # path('password_reset/', auth_views.PasswordResetView.as_view(template_name='RuleManager/UserManagement/registration/newpassword_reset_form.html')
    #      , name='password_reset'),
    # path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='RuleManager/UserManagement/registration/newpassword_reset_done.html')
    #      , name='password_reset_done'),
    # path('reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
    #      auth_views.PasswordResetConfirmView.as_view(template_name='RuleManager/UserManagement/registration/newpassword_reset_confirm.html')
    #      , name='password_reset_confirm'),
    # path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name='RuleManager/UserManagement/registration/newpassword_reset_complete.html')
    #      , name='password_reset_complete'),

]

