from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from ExpertSystem.models import Expertmodel

class Rules(models.Model):
    condition = models.TextField(verbose_name='شرط قانون',help_text="شرط را وارد کنید")
    pub_date = models.DateTimeField(default=now , verbose_name='تاریخ انتشار',
                            help_text ='تاریخ انتشار قانون را وارد کنید')
    consequence = models.ForeignKey('Facts', on_delete=models.CASCADE, related_name='consequence_field',
                            verbose_name='نتیجه قانون',help_text='نتیجه قانون را انتخاب کنید')
    domain = models.ForeignKey('Domain', on_delete=models.CASCADE, related_name='ruledomian_field',default=1,
                               verbose_name='نام حوزه دانش', help_text='حوزه دانش را انتخاب کنید')
    ownerexpert = models.ForeignKey(Expertmodel, on_delete=models.CASCADE, related_name='ruleowner_field', default=1,
                verbose_name='سیستم خبره', help_text='سیستم خبره ای که این قانون مربوط به آن است را انتخاب نمایید')
    class Meta:
        db_table = "RuleManager_rules"

    def __str__(self):
        return str(self.condition)+'==>'+str(self.consequence)



class Facts(models.Model):

    _YESNO = 'Y/N'
    _EXPLANING = 'EXP'
    _FACT_TYPE = [
        (_YESNO, 'خیر/بلی'),
        (_EXPLANING, 'تشریحی'),]
    _CONSEQUENCE = 'CONS'
    _CONDITION = 'COND'
    _SUBJECT = 'SUBJ'
    _FACT_EFFECT = [
        (_CONDITION, 'حقیقت بیان کننده ویژگی'),
        (_CONSEQUENCE, 'حقیقت بیان کننده نتیجه گیری'),
        (_SUBJECT,'حقیقت بیان کننده موضوع')
    ]
    fact_effect = models.CharField(max_length=4, choices=_FACT_EFFECT, default=_CONDITION,
                help_text='محدوده اثر حقیقت را انتخاب کنید',verbose_name='اثر حقیقت')
    fact = models.CharField(max_length=2000, blank=False, unique=True,
                help_text=' حقیقت را در حداکثر 2000 کلمه وارد کنید', verbose_name=' حقیقت')
    fact_type = models.CharField(max_length=3, choices=_FACT_TYPE, default=_YESNO,
                help_text=' نوع حقیقت را انتخاب کنید', verbose_name='نوع حقیقت')
    domain = models.ForeignKey('Domain',on_delete=models.CASCADE,related_name='factdomian_field',default=1,
                help_text=' حوزه دانش حقیقت را انتخاب کنید', verbose_name=' حوزه دانش')
    ownerexpert = models.ForeignKey(Expertmodel, on_delete=models.CASCADE, related_name='factowner_field', default=1,
            verbose_name='سیستم خبره', help_text='سیستم خبره ای که این حقیقت مربوط به آن است را انتخاب نمایید')

    def __str__(self):
        return self.fact


class Domain(models.Model):
    desc = models.TextField(max_length=300,blank=False,verbose_name='شرح حوزه دانش',
                help_text='توصیف مختصری از حوزه دانش مرتبط در حداکثر 300 حرف را وارد کنید')
    # brief = models.CharField(max_length=50,blank=False,unique=True, verbose_name='کد حوزه دانش',
                # help_text='کد مخفف حوزه دانش را با حروف انگلیسی در حداکثر 50 حرف وارد نمایید')
    ownerexpert = models.ForeignKey(Expertmodel, on_delete=models.CASCADE, related_name='domainowner_field', default=1,
                verbose_name='سیستم خبره', help_text='سیستم خبره ای که این حوزه دانش مربوط به آن است را انتخاب نمایید')

    def __str__(self):
        return (self.desc[:50] + '...') if len(self.desc) > 50 else self.desc


