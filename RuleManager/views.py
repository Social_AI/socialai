from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.template import loader
from .models import Rules, Facts,Domain
from ExpertSystem.models import Expertmodel
from django.shortcuts import get_object_or_404,reverse, render
from django.core.paginator import Paginator
from django.db.models import Q
from django.contrib.auth.decorators import login_required, user_passes_test,permission_required
from django import template
import json
from Contract_Assistant.utility import to_persian
register = template.Library()


user_login_required = user_passes_test(lambda user: user.is_staff)
def staff_user_required(view_func):
    decorated_view_func = login_required(user_login_required(view_func))
    return decorated_view_func

def superuser_check(user):
    return user.is_superuser


super_login_required = user_passes_test(superuser_check)
def admin_user_required(view_func):
    decorated_view_func = login_required(super_login_required(view_func))
    return decorated_view_func


@login_required
@permission_required('RuleManager.view_rules')
def index(request, exp_id):
    username = request.user.username
    # print(request.user.get_user_permissions())
    latest_rule_list = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(
            ownerexpert_id=exp_id).all().order_by('pk').reverse()
    paginator = Paginator(latest_rule_list, 15)  # Show 15 Rule per page.
    page_number = request.GET.get('page')
    print(str(list(request.GET)))
    print(request.user)
    page_obj = paginator.get_page(page_number)
    domains = Domain.objects.filter(ownerexpert_id=exp_id)
    expert = Expertmodel.objects.get(Q(appuser__user_auth__username=username) & Q(id=exp_id))
    output = {'Rule_list': latest_rule_list, 'page_obj': page_obj, 'domains':domains,'expert':expert,'cuser':username}
    request.session['condition'] = ''
    return render(request, 'RuleManager/index.html', output)

@login_required
@permission_required('RuleManager.view_rules')
def detail(request, exp_id, rule_id):
    username = request.user.username
    arule = get_object_or_404(Rules,Q(id=rule_id)&Q(ownerexpert__appuser__user_auth__username=username)&Q(ownerexpert_id=exp_id))
    output = {'Rule': arule , 'exp_id':exp_id}
    request.session['condition'] = ''
    return render(request, 'RuleManager/detail.html', output)


def filterrules(request):
    print('in filter')
    if len(request.POST):
        print('in post')
        custfilter = ''
        fdomain = request.POST.get('domain', '')
        print('dom:'+fdomain)
        fcondition = request.POST.get('condition', '')
        print('cons:' + fcondition)
        fconsequence = request.POST.get('consequence', '')
        print('cons:'+fconsequence)
        if fdomain != '':
            # custfilter = 'domain__id='+fdomain
            custfilter = Q(domain__id=int(fdomain))
            print('fdomain:'+str(custfilter))
            if fcondition != '':
                custfilter = Q(condition__icontains=fcondition)& custfilter
                print('fdomain-fcondition:'+str(custfilter))
                if fconsequence != '':
                    custfilter = Q(consequence__fact__icontains=fconsequence)&custfilter
                    print('fdomain-fcondition-fconsequence:'+str(custfilter))
            elif fconsequence != '':
                    custfilter = Q(consequence__fact__icontains=fconsequence) &custfilter
                    print('fdomain-fconsequence:'+str(custfilter))
        elif fcondition != '':
                custfilter = Q(condition__icontains=fcondition)
                print('fcondition-:'+str(custfilter))
                if fconsequence != '':
                    custfilter = Q(consequence__fact__icontains=fconsequence) &custfilter
                    print('fcondition-fconsequence:'+str(custfilter))
        elif fconsequence != '':
                custfilter = Q(consequence__fact__icontains=fconsequence)
                print('fconsequence:'+str(custfilter))
        if custfilter != '':
            print(custfilter)
            rules = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(custfilter).order_by('pk').reverse()
            # rules = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(domain_id=2).order_by('pk').reverse()
            print('list filtered')
        else:
            rules = Rules.objects.all().order_by('pk').reverse()
        print(rules.values())
        paginator = Paginator(rules, 15)  # Show 15 rules per page.
        page_number = request.POST.get('page')
        page_obj = paginator.get_page(page_number)
        print(page_obj.paginator)
        print(type(page_obj.object_list.values()))
        rules_to_view = []
        for rule in page_obj:
            persian_item={'condition':to_persian(rule.condition), 'consequence__fact':rule.consequence.fact, 'domain__desc':rule.domain.desc, 'id':rule.id}
            print('pi')
            print(persian_item)
            rules_to_view.append(persian_item)
            print('rules:')
            print(rules_to_view)
        # data = json.dumps(list(page_obj.object_list.values('condition', 'consequence__fact','domain__desc','id')),ensure_ascii=False)
        data = json.dumps(rules_to_view,ensure_ascii=False)
        # data = (list(page_obj.object_list.values('condition', 'consequence__fact', 'domain__desc', 'id')))
        # print('data in updat:')
        output = {'Rule_list': data, 'curr_page': page_obj.number, 'totalpages':paginator.num_pages};
        # data['totalrules']=len(rules)
        print((output))
        return JsonResponse(output)

        # return HttpResponse(output, content_type="text/json-comment-filtered")

        # return HttpResponse(rtemplate.render(output, request))
    else:
        print('GET request')
        return HttpResponse('GET request:')


@login_required
@permission_required('RuleManager.view_facts')
def factlist(request,  exp_id):
    username = request.user.username
    # return HttpResponse("Hello, world. You're at the RuleManager index.")
    if Expertmodel.objects.filter(appuser__user_auth__username=username).filter(id=exp_id).exists():
        template = loader.get_template('RuleManager/factlist.html')
        latest_fact_list = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(ownerexpert__id=exp_id).all().order_by('pk').reverse()
        paginator = Paginator(latest_fact_list, 15)  # Show 15 contacts per page.
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        ftypes = dict(Facts._FACT_TYPE)
        feffects = dict(Facts._FACT_EFFECT)
        print(feffects)
        domains = Domain.objects.filter(ownerexpert__appuser__user_auth__username=username).filter(ownerexpert_id=exp_id)
        expert = Expertmodel.objects.get(Q(appuser__user_auth__username=username) & Q(id=exp_id))
        output ={'Fact_list':latest_fact_list,'facttypes':ftypes,'facteffects':feffects,'domains':domains,
                 'page_obj': page_obj,'exp_id':exp_id, 'cuser':username, 'expert':expert}
        return HttpResponse(template.render(output, request))
    else:
        return HttpResponseRedirect(reverse('RuleManager:loginuser'))


def upload_fact_result(request, exp_id):
    return HttpResponse('.بارگذاری فایل با موفقیت انجام شد')
    # return render(request, 'RuleManager/uploadresult.html', flist)

def filterfacts(request):
    print('in filter')
    if len(request.POST):
        print('in post')
        custfilter = ''
        fdomain = request.POST.get('domain', '')
        print('dom:'+fdomain)
        fact = request.POST.get('fact', '')
        print('fact:' + fact)
        facttype = request.POST.get('facttype', '')
        print('facttype:'+facttype)
        facteffect = request.POST.get('facteffect', '')
        print('facteffect:'+facteffect)

        if fdomain != '':      #fiilter contain( domain)
            # custfilter = 'domain__id='+fdomain
            custfilter = Q(domain__id=int(fdomain))
            print('fdomain:'+str(custfilter))
            if fact != '':      #fiilter contain( domain , fact)
                custfilter = Q(fact__icontains=fact)& custfilter
                print('fact__icontains:'+str(custfilter))
                if facttype != '':       #fiilter contain( domain , fact,facttype)
                    custfilter = Q(fact_type__iexact=facttype)&custfilter
                    print('fact_type__iexact:'+str(custfilter))
                    if facteffect != '':        #fiilter contain( domain , fact,facttype,facteffect)
                        custfilter = Q(fact_effect__iexact=facteffect)&custfilter
                        print('fact_effect__iexact:'+str(custfilter))

                elif facteffect != '':       #fiilter contain( domain , fact,facteffect)
                        custfilter = Q(fact_effect__iexact=facteffect)&custfilter
                        print('fact_effect__iexact:'+str(custfilter))
            elif facttype != '':         #fiilter contain( domain , ,facttype)
                    custfilter = Q(fact_type__iexact=facttype)&custfilter
                    print('fact_type__iexact:'+str(custfilter))
                    if facteffect != '':        #fiilter contain( domain , facttype,facteffect)
                        custfilter = Q(fact_effect__iexact=facteffect)&custfilter
                        print('fact_effect__iexact:'+str(custfilter))

            elif facteffect != '':      #fiilter contain( domain , facteffect)
                        custfilter = Q(fact_effect__iexact=facteffect)&custfilter
                        print('fact_effect__iexact:'+str(custfilter))

        elif fact != '':        #fiilter contain( fact)
            custfilter = Q(fact__icontains=fact)
            print('fact__icontains:'+str(custfilter))
            if facttype != '':      #fiilter contain( fact,facttype)
                custfilter = Q(fact_type__iexact=facttype) & custfilter
                print('fact_type__iexact:' + str(custfilter))
                if facteffect != '':         #fiilter contain( fact,facttype,facteffect)
                    custfilter = Q(fact_effect__iexact=facteffect) & custfilter
                    print('fact_effect__iexact:' + str(custfilter))

            elif facteffect != '':      #fiilter contain( fact,facteffect)
                        custfilter = Q(fact_effect__iexact=facteffect)&custfilter
                        print('fact_effect__iexact:'+str(custfilter))

        elif facttype != '':          #fiilter contain( facttype)
            custfilter = Q(fact_type__iexact=facttype)
            print('fact_type__iexact:' + str(custfilter))
            if facteffect != '':         #fiilter contain( facttype,facteffect)
                custfilter = Q(fact_effect__iexact=facteffect) & custfilter
                print('fact_effect__iexact:' + str(custfilter))

        if custfilter != '':
            print(custfilter)
            facts = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(custfilter).order_by('pk').reverse()
            # rules = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(domain_id=2).order_by('pk').reverse()
            print('fact filtered')
        else:
            facts = Facts.objects.all().order_by('pk').reverse()
        print(facts.values())
        paginator = Paginator(facts, 15)  # Show 15 rules per page.
        page_number = request.POST.get('page')
        page_obj = paginator.get_page(page_number)
        print(page_obj.paginator)
        domains = Domain.objects.all()
        rtemplate = loader.get_template('RuleManager/factlist.html')
        print(type(page_obj.object_list.values()))
        data = json.dumps(list(page_obj.object_list.values('fact', 'fact_type', 'domain__desc', 'id', 'fact_effect')),ensure_ascii=False)
        # data = (list(page_obj.object_list.values('condition', 'consequence__fact', 'domain__desc', 'id')))
        # print('data in updat:')
        output = {'fact_list': data, 'curr_page': page_obj.number, 'totalpages':paginator.num_pages};
        # data['totalrules']=len(rules)
        print((output))
        return JsonResponse(output)

        # return HttpResponse(output, content_type="text/json-comment-filtered")

        # return HttpResponse(rtemplate.render(output, request))
    else:
        print('GET request')
        return HttpResponse('GET request:')


def update_facts(request):
    if len(request.POST):
        domain = request.POST.get('domain', '')
        if domain == 'null':
            domain = 1
        domain = int(domain)
        facteffect = request.POST.get('facteffect', '')
        print(facteffect)
        print(domain)
        if facteffect == 'condition':
            facteffect = Q(fact_effect= Facts._CONDITION) | Q(fact_effect= Facts._SUBJECT)
        else:
            facteffect = Q(fact_effect=Facts._CONSEQUENCE)
        facts = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(domain_id=domain).\
            filter(facteffect)
        data = json.dumps(list(facts.values('fact', 'id')),ensure_ascii=False)
        # print('data in updat:')
        # print(data)
        return HttpResponse(data, content_type="text/json-comment-filtered")
    else:
        return HttpResponse('not valid request')


def update_domains(request):
    if len(request.POST):
        ownerexpert = request.POST.get('ownerexpert', '')
        if ownerexpert == 'null':
            ownerexpert = 1
        ownerexpert = int(ownerexpert)
        doms = Domain.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(ownerexpert_id=ownerexpert)
        data = json.dumps(list(doms.values('desc', 'id')),ensure_ascii=False)
        # print('data in updat:')
        print(data)
        return HttpResponse(data, content_type="text/json-comment-filtered")
    else:
        return HttpResponse('not valid request')


@login_required
@permission_required('RuleManager.view_facts')
def factdetail(request,  exp_id, fact_id):
    username = request.user.username
    template = loader.get_template('RuleManager/factdetail.html')
    if Facts.objects.filter(Q(id=fact_id)&Q(ownerexpert__appuser__user_auth__username=username)).exists():
        fact = get_object_or_404(Facts,Q(id=fact_id)&Q(ownerexpert__appuser__user_auth__username=username))
        output = {'Fact': fact, 'exp_id':exp_id}
        return HttpResponse(template.render(output, request))
    else:
        return HttpResponseRedirect(reverse('ServiceUser:user_expertfacts',args=[exp_id]))

@login_required
@permission_required('RuleManager.delete_facts')
def delete_fact_by_index(request,  exp_id, fact_id):
    username = request.user.username
    if request.method == 'GET':
        afact = get_object_or_404(Facts, id=fact_id)
        relatedrules = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).\
            filter(ownerexpert__id=exp_id).\
            filter(Q(condition__icontains=afact.fact)|Q(consequence__fact__iexact=afact.fact))
        data= {'fact':afact,'relatedrules':relatedrules,'exp_id':exp_id}
        return render(request, 'RuleManager/delete_fact.html',data)
            # return HttpResponse('valid data saved')
    else:
        afact = get_object_or_404(Facts, id=fact_id)
        relatedrules = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user). \
            filter(ownerexpert__id=exp_id). \
            filter(Q(condition__icontains=afact.fact)|Q(consequence__fact__iexact=afact.fact))
        for rule in relatedrules:
            rule.delete()
        Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).get(pk= fact_id).delete()
        return HttpResponseRedirect(reverse('ServiceUser:user_expertfacts', args=[exp_id]))

@login_required
@permission_required('RuleManager.add_rules')
def create_rule(request):
    error = False
    # If form has posted
    if len(request.POST):
    # This line checks if the data was sent in POST.
        if 'condition' in request.POST:
        # This line checks whether a given data named name exists in the
            condition = request.POST.get('condition', '')
        else:
            error=True
            return HttpResponse("condition is invalid" )
        if 'consequence' in request.POST:
            consequence = request.POST.get('consequence', '')
        else:
            error=True
            return HttpResponse("consequence is invalid" )
        if 'Date Created' in request.POST:
            pub_date = request.POST.get('Date Created', '')
        else:
            error=True
            return HttpResponse("Date Created is invalid" )
        if 'domain' in request.POST:
            domain = request.POST.get('domain', '')
        else:
            error=True
            return HttpResponse("domain is invalid" )

        if not error:
            new_rule = Rules(condition=condition, consequence=consequence,
                             pub_date=pub_date,domain=domain)
            new_rule.save()
            return HttpResponse("Rule added")
        else:
            return HttpResponse("خطایی رخ داده است.")
    else:
        rul_list = Rules.objects.objects.filter(ownerexpert__appuser__user_auth=request.user).all()
        return render(request, 'RuleManager/create_rule.html', {'alllist':rul_list})

@login_required
@permission_required('RuleManager.delete_rules')
def delete_rule_by_index(request,  exp_id, rule_id):
    username = request.user.username
    if request.method == 'GET':
        arule = get_object_or_404(Rules, id=rule_id)
        data= {'rule':arule, 'exp_id':exp_id}
        return render(request, 'RuleManager/delete_rule.html',data)
            # return HttpResponse('valid data saved')
    else:
        # return HttpResponse('invalid data:=='+'---'+request.method)
        Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(pk= rule_id).delete()
        return HttpResponseRedirect(reverse('ServiceUser:user_expertrules', args=[exp_id]))

@login_required
@permission_required('RuleManager.view_domain')
def domainlist(request,  exp_id):
    username = request.user.username
    # return HttpResponse("Hello, world. You're at the RuleManager index.")
    template = loader.get_template('RuleManager/domainlist.html')
    if  Expertmodel.objects.filter(appuser__user_auth__username=username).filter(id=exp_id).exists():
        latest_domain_list = Domain.objects.filter(Q(ownerexpert_id=exp_id)&Q(ownerexpert__appuser__user_auth__username=username)).all().order_by('pk').reverse()
        paginator = Paginator(latest_domain_list, 15)  # Show 25 contacts per page.
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        output ={'domain_list':latest_domain_list,'page_obj': page_obj, 'exp_id':exp_id}
        return HttpResponse(template.render(output, request))
    else:
        return HttpResponseRedirect(reverse('ServiceUser:user_expertdetail', args=[ exp_id]))

@login_required
@permission_required('RuleManager.view_domain')
def domaindetail(request,  exp_id, dom_id):
    username = request.user.username
    template = loader.get_template('RuleManager/domaindetail.html')
    if Domain.objects.filter(Q(id=dom_id) & Q(ownerexpert__appuser__user_auth__username=username)).exists():
        adomain = get_object_or_404(Domain,id=dom_id)
        output = {'domain': adomain, 'exp_id':exp_id}
    # request.session['condition'] = ''
        return HttpResponse(template.render(output, request))
    else:
        return HttpResponseRedirect(reverse('ServiceUser:user_expertdomains', args=[ exp_id]))

@login_required
@permission_required('RuleManager.delete_domain')
def delete_domain_by_index(request, exp_id, dom_id):
    username = request.user.username
    if request.method == 'GET':
        adomain = get_object_or_404(Domain, id=dom_id)
        relatedrules = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).\
            filter(ownerexpert__id=exp_id).filter(domain_id=dom_id)
        relatedfacts = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).\
            filter(ownerexpert__id=exp_id).filter(domain_id=dom_id)
        data= {'adomain':adomain, 'relatedrules':relatedrules, 'relatedfacts':relatedfacts,'exp_id':exp_id}
        return render(request, 'RuleManager/delete_domain.html',data)
            # return HttpResponse('valid data saved')
    else:
        relatedrules = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user). \
            filter(ownerexpert__id=exp_id).filter(domain_id=dom_id)
        relatedfacts = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user). \
            filter(ownerexpert__id=exp_id).filter(domain_id=dom_id)
        for rule in relatedrules:
            rule.delete()
        for fact in relatedfacts:
            fact.delete()
        Domain.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(pk= dom_id).delete()
        return HttpResponseRedirect(reverse('ServiceUser:user_expertdomains', args=[exp_id]))


def extractfact(cond):
    foundfacts = []
    splited = cond.partition(' AND ')
    if (' AND ' not in splited):
        if (' OR ' not in splited):
            foundfacts.append(splited[0])
        else:
            foundfacts.append(splited[0])
            extractfact(splited[2])
    else:
        foundfacts.append(splited[0])
        extractfact(splited[2])
    return foundfacts


@login_required
@permission_required('RuleManager.change_rules','RuleManager.change_facts')
def controlpanel(request,  exp_id):
    username = request.user.username
    allrule = Rules.objects.filter(Q(ownerexpert__appuser__user_auth__username=username) & Q(ownerexpert_id=exp_id))
    cid = []
    for rule in allrule:
        foundfacts = extractfact(rule.condition)
        for fact in foundfacts:
            if not (len(Facts.objects.filter(Q(fact__iexact=fact) &
                                             Q(ownerexpert__appuser__user_auth__username=username) &
                                             Q(ownerexpert_id=exp_id)))):
                cid.append(rule.id)
                break
        if not (len(Facts.objects.filter(Q(fact__iexact=rule.consequence) &
                                         Q(ownerexpert__appuser__user_auth__username=username) &
                                         Q(ownerexpert_id=exp_id)))):
            cid.append(rule.id)
    print(cid)
    corruptrules= list(Rules.objects.filter(Q(id__in=tuple(cid)) & Q(ownerexpert__appuser__user_auth__username=username) &
                                                     Q(ownerexpert_id=exp_id)))
    template = loader.get_template('RuleManager/../ServiceUser/templates/ServiceUser/controlpanel.html')
    paginator = Paginator(corruptrules, 15)  # Show 10 contacts per page.
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    ruledata = {'corruptrules': corruptrules,  'page_obj': page_obj, 'exp_id':exp_id}
    return HttpResponse(template.render(ruledata, request))
