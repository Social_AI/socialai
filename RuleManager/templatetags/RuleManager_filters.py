from django import template
from django.template.defaultfilters import stringfilter
from Contract_Assistant.settings import BASE_DIR
from hazm import *
import os

from hazm import Normalizer, POSTagger, word_tokenize

modelpath = os.path.join(os.getcwd(), 'ract_Assistant/static/ExpertSystem/resources')
normalizer = Normalizer()
tagger = POSTagger(model=os.path.join(BASE_DIR,'static/ExpertSystem/resources', 'postagger.model'))

register = template.Library()

@register.filter
@stringfilter
def reversevalue(value):
    if isinstance(value, str):
        return value[::-1]
    else:
        temp = str(value)
        return temp[::-1]

@register.filter
@stringfilter
def facttypedesc(value):
    if value == 'Y/N':
        return 'بلی/خیر'
    elif value =='EXP':
        return 'تشریحی'
    else:
        return 'نوع حقیقت تعریف نشده است!'


@register.filter
@stringfilter
def facteffectdesc(value):
    if value == 'COND':
        return 'حقیقت بیان کننده ویژگی'
    elif value == 'CONS':
        return 'حقیقت بیان کننده نتیجه گیری'
    elif value == 'SUBJ':
        return 'حقیقت بیان کننده موضوع'
    else:
        return 'حوزه اثر حقیقت تعریف نشده است!'


@register.filter
@stringfilter
def topersian(conds):
    # conds is just a  fact
    if ('AND' not in conds) and ('OR' not in conds):
        if 'NOT ' in conds:
            purefact = conds.split('NOT ')[1]
            taggedfact = tagger.tag(word_tokenize(purefact))
            print('taggedfact:')
            print(taggedfact)
            last = taggedfact[len(taggedfact)-1]
            print('last"')
            print(last)
            print(last[0])
            print(last[1])
            if last[1] == 'V':
                Verb = last[0].replace('_', ' ')
                if Verb == 'است':
                    N_Verb = 'نیست'
                    print('N_Verb: '+N_Verb)
                    mainfact = purefact.split(Verb)[0]
                    print('mainfact: '+mainfact)
                    return mainfact + N_Verb
                elif Verb == 'باشد':
                    priorlast = taggedfact[len(taggedfact)-2]
                    Verb = priorlast[0]
                    N_Verb = 'ن'+Verb
                    print('N_Verb: '+N_Verb)
                    N_fact = purefact.replace(Verb, N_Verb)
                    return N_fact
                else:
                    N_Verb = 'ن'+Verb
                    print('N_Verb: '+N_Verb)
                    mainfact = purefact.split(Verb)[0]
                    print('mainfact: '+mainfact)
                    return mainfact + N_Verb

            else:
                return conds
        else:
            return conds
    else:
        tempres = ''
        splited = conds.partition(' AND ')
        if ' AND ' not in splited:
            if ' OR ' not in splited[0]:
                tempres += topersian(splited[0])
                return tempres
            else:
                splited = splited[0].partition(' OR ')
                tempres += topersian(splited[0])
                tempres += ' یا '
                tempres += topersian(splited[2])
                return tempres
        else:
            tempres += topersian(splited[0])
            tempres += ' و '
            tempres += topersian(splited[2])
            return  tempres


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)