from difflib import SequenceMatcher as sm
from django.utils.translation import gettext as _
from Contract_Assistant.settings import MEDIA_ROOT, FILE_UPLOAD_MAX_MEMORY_SIZE
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.paginator import Paginator
from RuleManager.models import Rules , Facts,Domain
from ExpertSystem.models import Expertmodel
from django import forms
from django.urls import reverse
from django.db.models import Q
from django.core.exceptions import ValidationError
import datetime, os,errno, csv
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from datetime import datetime as dt
from jalali_date.fields import JalaliDateField, SplitJalaliDateTimeField
from jalali_date.widgets import AdminJalaliDateWidget, AdminSplitJalaliDateTime


class Form_rule(forms.ModelForm):
    # Here we create a class that inherits from ModelForm.
    class Meta:
        model = Rules
        # We define the model that should be based on the form.
           # exclude = ('date_created', 'last_connexion',)
        fields = '__all__'
        error_messages = {
            'NON_FIELD_ERRORS': {
               'خطایی رخ داده است.',
            }
        }
    def __init__(self, *args, **kwargs):
        super(Form_rule, self).__init__(*args, **kwargs)
            # self.fields['date'] = JalaliDateField(label=_('date'),  # date format is  "yyyy-mm-dd"
            #                                       widget=AdminJalaliDateWidget  # optional, to use default datepicker
            #                                       )

            # you can added a "class" to this field for use your datepicker!
        # self.fields['condition'].widget.attrs.update({'disabled': 'true'})

        self.fields['pub_date'] = JalaliDateField(label=_('تاریخ'),
                    widget=AdminJalaliDateWidget)
                    # required, for decompress DatetimeField to JalaliDateField and JalaliTimeField
        # self.fields['pub_date'].widget.attrs.update({'class': 'persiandp'})
        self.fields['pub_date'].error_messages = {'invalid': 'لطفا یک تاریخ معتبر وارد کنید.',
                                                   'required': 'ورود تاریخ الزامی است.',
                                                   'empty': 'ورود تاریخ الزامی است.',
                                                   'blank': 'ورود تاریخ الزامی است.'
                                                   }
        self.fields['condition'].error_messages={'invalid': 'لطفا یک شرط معتبر وارد کنید.',
                                                'required':'انتخاب(ترکیب) شرط اولیه الزامی است.',
                                                 'empty': 'شرط قانون نمی تواند خالی باشد!',
                                                 'blank': 'شرط قانون نمی تواند خالی باشد!'
                                                 }
        self.fields['consequence'].error_messages = {'required': 'انتخاب نتیجه الزامی می باشد.',
                                                     'empty': 'لطفا نتیجه قانون را انتخاب کنید.',
                                                     'blank': 'لطفا نتیجه قانون را انتخاب کنید.'
        }
        self.fields['ownerexpert'].error_messages = {'required': 'انتخاب سامانه هوشمند الزامی می باشد.',
                                                     'empty': 'سامانه هوشمند نمی تواند خالی باشد',
                                                     'blank': 'سامانه هوشمند نمی تواند خالی باشد'
        }
        self.fields['domain'].error_messages = {'required': 'انتخاب حوزه دانش الزامی می باشد.',
                                                     'empty': 'حوزه دانش نمی تواند خالی باشد',
                                                     'blank': 'حوزه دانش نمی تواند خالی باشد'
        }
    def clean(self):
        cleaned_data = super (Form_rule, self).clean()
        print('cle')
        print(cleaned_data)
        pdate = cleaned_data.get('pub_date')
        if pdate is None  :
            # pdate = dt.now()
            pass
            # self.fields['pub_date'].widget.attrs.update({'class': 'star'})
            # raise ValidationError({'pub_date': _('لطفا یک مقدار صحیح را وارد کنید!')})

        elif pdate > dt.now().date():
            # self.fields['pub_date'].widget.attrs.update({'class': 'error'})
            raise ValidationError({'pub_date': _('فقط مقادیر کمتر یا مساوی با تاریخ روز جاری مجاز می باشند!')})
        else:
            pdate = dt.combine(pdate, dt.now().time())
        print(pdate)
        cleaned_data['pub_date'] = pdate
        print(cleaned_data['pub_date'])
        return cleaned_data


class Form_fact(forms.ModelForm):
    # Here we create a class that inherits from ModelForm.
    class Meta:
        model = Facts
        # We define the model that should be based on the form.
        fields = ('fact','fact_type','fact_effect','domain', 'ownerexpert')


class Form_domain(forms.ModelForm):
    # Here we create a class that inherits from ModelForm.
    class Meta:
        model = Domain
        # We define the model that should be based on the form.
        fields = ('desc', 'ownerexpert')

class UploadFactForm(forms.Form):
    content  = forms.FileField()

    WHITELIST_FILE_EXT = ['csv','xls']

    def clean(self):
        print('in cleaned_data***************************************')
        cleaned_data = super (UploadFactForm, self).clean()
        print(cleaned_data)
        if 'content' in cleaned_data:
            content = cleaned_data.get('content')
            file_size = content.size
            if file_size > FILE_UPLOAD_MAX_MEMORY_SIZE:
                raise forms.ValidationError("حداکثر سایز مجاز %d MBمی باشد." %(FILE_UPLOAD_MAX_MEMORY_SIZE/1024)
                    )
            file_type = content.content_type.split('/')[0]
            print (file_type)
            if len(content.name.split('.')) == 1:
                raise forms.ValidationError("این نوع فایل پشتیبانی نمی شود.")
            if content.name.split('.')[-1] not in self.WHITELIST_FILE_EXT:
                raise forms.ValidationError("فقط فایل های نوع '.csv' و '.xls' مجاز می باشند")
        return cleaned_data

user_login_required = user_passes_test(lambda user: user.is_staff)
def staff_user_required(view_func):
    decorated_view_func = login_required(user_login_required(view_func))
    return decorated_view_func

@login_required
@permission_required('RuleManager.add_facts')
def upload_fact_file(request,  exp_id):
    exp = Expertmodel.objects.get(id=exp_id)
    if request.method == 'POST':
        # print('**********************POST**************************')
        # print(request.POST)
        # print(request.FILES)
        form = UploadFactForm(request.POST, request.FILES)
        if form.is_valid():
            uploadedfacts = handle_uploaded_file(request.FILES['content'], request.user.username)
            paginator = Paginator(uploadedfacts, 15)  # Show 15 contacts per page.
            page_number = request.GET.get('page') or 1
            page_obj = paginator.get_page(page_number)

            return render(request, 'RuleManager/uploadresult.html', {'flist': uploadedfacts, 'page_obj':page_obj})
        else:
            # print('not valid file')
            status=str(form.errors)
            return render(request, 'RuleManager/upload_factfile.html', {'form': form, 'status':status, 'expert':exp})
    else:
        # print('************************GET****************************')
        # print(request)
        status='ناموفق-گت'
        form = UploadFactForm()
        return render(request, 'RuleManager/upload_factfile.html', {'form': form, 'status':status, 'expert':exp})


@login_required
@permission_required('RuleManager.add_facts')
def save_uploaded_fact(request):
    pass

@login_required
@permission_required('RuleManager.add_facts')
def facts_fromfile(request,  fullfilepath):
    uploadedfacts = []
    dom_id = get_object_or_404(Domain, ownerexpert__appuser__user_auth=request.user).id
    exp_id = get_object_or_404(Expertmodel,appuser__user_auth=request.user).id
    with open(fullfilepath) as f:
        reader = csv.reader(f)
        for rownum, (fact, fact_type, fact_effect, domain, ownerexpert) in enumerate(reader):
            if rownum == 0:
            # let's skip the column captions
                continue
            afact = Facts(fact=fact, fact_type=fact_type, fact_effect=fact_effect,
                domain=dom_id,ownerexpert=exp_id)
            uploadedfacts.append(afact)
    return uploadedfacts



def handle_uploaded_file(f, uname):
    f_name = f.name
    print('f_name:'+f_name)
    f_ext = f.content_type.split('/')[0]
    print('f_ext:'+f_ext)
    userfilepath = os.path.join(MEDIA_ROOT,'ServiceUser','uploaded', uname)
    print(userfilepath)
    if not os.path.exists(userfilepath):
        try:
            os.mkdir(userfilepath)
            factpath = os.path.join(userfilepath, 'facts')
            print(1)
            print(factpath)
            if not os.path.exists(factpath):
                try:
                    os.mkdir(factpath)
                    with open(os.path.join(factpath,  f_name), 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)
                    return facts_fromfile(os.path.join(factpath,  f_name))
                except OSError as e:
                    if e.errno == errno.ENOENT:
                        # do your FileNotFoundError code here
                        print('فایل حقایق با موفقیت بارگذاری نگردید!')
        except OSError as e:
            if e.errno == errno.ENOENT:
                        # do your FileNotFoundError code here
                print('پوشه تنظیمات  وجود ندارد یا با موفقیت ایجاد نشد!')
    elif not os.path.exists(os.path.join(userfilepath, 'facts')):
        try:
            factpath = os.path.join(userfilepath, 'facts')
            print(2)
            print(factpath)
            with open(os.path.join(factpath,  f_name), 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
            return facts_fromfile(os.path.join(factpath,  f_name))
        except OSError as e:
            if e.errno == errno.ENOENT:
                        # do your FileNotFoundError code here
                print('فایل حقایق با موفقیت بارگذاری نگردید!')
    else:
        factpath = os.path.join(userfilepath, 'facts')
        print(3)
        print(factpath)
        with open(os.path.join(factpath,  f_name), 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        return facts_fromfile(os.path.join(factpath,  f_name))



@login_required
@permission_required('RuleManager.change_domain')
def edit_domain_by_index(request,  exp_id, dom_id):
    username = request.user.username
    if Domain.objects.filter(ownerexpert__appuser__user_auth__username=username).filter(id=dom_id).exists():
        admoain = get_object_or_404(Domain, id=dom_id)
        if request.method == 'GET':
            data= {'ownerexpert':Expertmodel.objects.filter(id=exp_id),'domain':admoain, 'exp_id':exp_id}
            # form = Form_domain(data)
            return render(request, 'RuleManager/edit_domain.html',data)
        else:
            form = Form_domain(request.POST, instance= admoain)
            if form.is_valid():
                form.save()
                print('domain edited.now is:'+str(admoain)+'------desc is:'+admoain.desc)
                return HttpResponseRedirect(reverse('ServiceUser:user_expertdomains',args=[exp_id]))
            else:
                return HttpResponse(form.as_table())


@login_required
@permission_required('RuleManager.add_domain')
def create_domain_by_model(request, exp_id):
    username = request.user.username
    if len(request.POST) > 0:
        form = Form_domain(request.POST)
        if form.is_valid():
            form.save(commit=True)
            if request.is_ajax:
                return  JsonResponse("success", safe=False)
            else:
                return HttpResponseRedirect(reverse('ServiceUser:user_expertdomains', args=[exp_id]))
        else:
            if request.is_ajax:
                print(str(form.errors))
                return JsonResponse("failure", safe=False)
            else:
                data = {'ownerexpert': form['ownerexpert'], 'desc': form['desc'],
                        }
                return render(request, 'RuleManager/create_domain_by_model.html',data)

    else:
        data = {'ownerexpert': get_object_or_404(Expertmodel, id=exp_id), 'desc': '',
                }
        # print(type(data['ownerexpert']))
        # form = Form_domain(data)
        # print(form['ownerexpert'])
        # request.session['sender'] = 'create_fact'
        return render(request, 'RuleManager/create_domain_by_model.html',data)


@login_required
@permission_required('RuleManager.change_facts')
def edit_fact_by_index(request, exp_id , fact_id):
    username = request.user.username
    if Facts.objects.filter(ownerexpert__appuser__user_auth__username=username).filter(id=fact_id).exists():
        if request.method == 'GET':
            afact = get_object_or_404(Facts, id=fact_id)
            data= {'fact':afact.fact, 'fact_type':Facts._FACT_TYPE,
                    'fact_effect':Facts._FACT_EFFECT,'domain':Domain.objects.
                    filter(ownerexpert__appuser__user_auth__username=username).filter(ownerexpert_id=exp_id)
                    ,'afact':afact,'exp_id':exp_id,
                    'ownerexpert':Expertmodel.objects.filter(appuser__user_auth__username=username),}
            form = Form_fact(data)
            print(str(data))
            return render(request, 'RuleManager/edit_fact.html',data)
        else:
            afact = get_object_or_404(Facts, id=fact_id)
            relatedconds = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(condition__icontains=afact.fact)
            relatedconseqs = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(consequence__fact=afact.fact)
            oldfact = afact.fact
            form = Form_fact(request.POST, instance= afact)
            if form.is_valid():
                form.save()
                print('fact edited.now is:'+str(afact)+'------mode is:'+afact.fact_effect)
                for rule in relatedconds:
                    rule.condition = rule.condition.replace(oldfact,form.cleaned_data['fact'])
                    rule.save()
                    print('related rule cond edited')
                for rule in relatedconseqs:
                # rule.consequence=form.cleaned_data['fact']
                    rule.consequence=afact
                    rule.save()
                    print('related rule cons edited:'+str(rule))
                return HttpResponseRedirect(reverse('ServiceUser:user_expertfacts', args=[exp_id]))
            else:
                return HttpResponse(form.as_table())
    else:
        return HttpResponseRedirect(reverse("ServiceUser:user_expertfacts", args=[exp_id]))


@login_required
@permission_required('RuleManager.add_rules')
def create_rule_by_model(request,  exp_id):
    username = request.user.username
    if len(request.POST) > 0 :
        print("post")
        form = Form_rule(request.POST)
        print(request.POST['consequence'])
        if form.is_valid():
            cond = form.cleaned_data['condition']
            for rule in Rules.objects.filter(Q(ownerexpert__appuser__user_auth__username=username) & Q(ownerexpert_id=exp_id)):
                if sm(a=cond, b=rule.condition).quick_ratio() == 1:
                    currcons = rule.consequence.fact
                    if sm(a= str(form.cleaned_data['consequence']),b=currcons).quick_ratio() == 1:
                        print('not valid rule:' + str(rule))
                        request.session['sender'] = 'ServiceUser:user_expertnewrule'
                        form.add_error('condition', forms.ValidationError(
                            _('شرط و نتیجه وارد شده تکراری می باشد:%s'),
                            code='invalid', params={'عبارت تکراری': cond}, ))
                        dom = Domain.objects.filter(id=request.POST['domain'])
                        factlist = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user). \
                            filter(ownerexpert__id=exp_id).filter(fact_effect=Facts._CONSEQUENCE)
                        data = {'ownerexpert': Expertmodel.objects.get(id=exp_id), 'condition': form['condition'].value,
                            'domain':dom ,'consequence': factlist, 'exp_id': exp_id,'sender': request.session['sender'],
                                'pub_date': form['pub_date'],'errors':form.errors.items,
                            }

                        return render(request, 'RuleManager/create_rule_by_model.html',data)
            print('valid-cond:'+request.session['condition'])
            print(str(form.errors))
            request.session['condition'] = ''
            form.save(commit=True)
            return HttpResponseRedirect(reverse('ServiceUser:user_expertrules', args=[exp_id]))
        else:
            print('not valid-cond:'+request.POST['ownerexpert'])
            print(form.errors.as_data())
            request.session['sender'] = 'ServiceUser:user_expertnewrule'
            # return render(request, 'RuleManager/create_rule_by_model.html',
            #               {'form': form,'sender':request.session['sender']})

            data = {'ownerexpert': Expertmodel.objects.get(id=exp_id), 'condition': form['condition'].value,
                    'domain': Domain.objects.filter(id=request.POST['domain']),
                    'consequence':Facts.objects.filter(id= request.POST['consequence']), 'exp_id': exp_id,
                    'sender': request.session['sender'],'pub_date':form['pub_date']
                    }
            data['form'] = form
            return render(request, 'RuleManager/create_rule_by_model.html',
                      data)     #***** correct here***********
    else :
        print("get")
        request.session['sender'] = 'ServiceUser:user_expertrules'
        if 'condition' in request.session and request.session['condition'] != '':
            print('here')
            domainid = extractfactdomain(request,request.session['condition'])
            print(domainid)
            factlist = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user). \
                filter(Q(domain=domainid) & Q(domain__ownerexpert__appuser__user_auth=request.user)).\
                filter(fact_effect=Facts._CONSEQUENCE)

            data = {'ownerexpert':Expertmodel.objects.get(id=exp_id),'condition':request.session['condition'],
                    'domain':Domain.objects.filter( id=domainid),'consequence':factlist,'exp_id':exp_id,
                    'pub_date':dt.now().date()}
            idata = {'ownerexpert': Expertmodel.objects.get(id=exp_id), 'condition': request.session['condition'],
                    'domain': Domain.objects.filter(id=domainid), 'consequence': factlist,'pub_date':dt.now().date()}
            print(str(data))
            request.session['condition']=''
            form =Form_rule(initial=idata)
            # print('cond now:'+str(form))
            data['form'] = form
        else:
            print('here2')
            if 'domain' in request.session:
                print('here3')
                domain = request.session['domain']
                factlist = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user). \
                    filter(Q(domain=domain) & Q(domain__ownerexpert__appuser__user_auth=request.user)). \
                    filter(fact_effect=Facts._CONSEQUENCE)
            else:
                print('here4')
                domain = Domain.objects.filter( ownerexpert__appuser__user_auth=request.user).\
                    filter(ownerexpert__id=exp_id)
                factlist = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user). \
                    filter(ownerexpert__id=exp_id).filter(fact_effect=Facts._CONSEQUENCE)

            print(list(factlist))
            # print(list(domain))
            data = { 'consequence':factlist, 'domain':domain,
                     'ownerexpert':get_object_or_404(Expertmodel, id=exp_id),'condition':'',
                     'pub_date':datetime.datetime.now(),'sender':request.session['sender'],
                     'exp_id':exp_id}
            idata = {'consequence': factlist, 'domain': domain,
                    'ownerexpert': get_object_or_404(Expertmodel, id=exp_id), 'condition': '',
                    'pub_date': dt.now().date()}
            print(str(data))
            print(type(exp_id))
            form = Form_rule(initial=idata)
            # form = Form_domain()
            data['form'] = form
            print(form['ownerexpert'])
        return render(request, 'RuleManager/create_rule_by_model.html',
                      data)

def extractfactdomain(request, conds):
    tempres = []
    splited = conds.partition(' AND ')
    if ' AND ' not in splited:
        if ' OR ' not in splited[0]:
            tempres.append(splited[0])
        else:
            splited = splited[0].partition(' OR ')
            tempres.append(splited[0])
            # tempres.append(extractfactdomain(request, splited[2]))

    else:
        tempres.append(splited[0])
        # tempres.append(extractfactdomain(request, splited[2]))
    if 'NOT' not  in tempres:
        return  Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).get(fact__iexact=tempres[0]).domain.id
    else:
        splited = tempres[0].partition('NOT')
        if 'NOT' not in splited:
            return Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).get(fact__iexact=splited[0]).domain.id
        else:
            return Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).get(fact__iexact=splited[2]).domain.id


@login_required
@permission_required('RuleManager.change_rules')
def edit_rule_by_index(request,  exp_id, rule_id):
    username = request.user.username
    if request.method == 'GET':
        arule = get_object_or_404(Rules, id=rule_id)
        print(type(arule.domain))
        factlist = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(fact_effect=Facts._CONSEQUENCE)
        data= {'condition':arule.condition, 'consequence':factlist,
               'pub_date':arule.pub_date,'domain':Domain.objects.filter(ownerexpert__appuser__user_auth=request.user),
               'ownerexpert':Expertmodel.objects.filter(appuser__user_auth__username=username)}
        print(data['pub_date'])
        form = Form_rule(initial=data)
        idata = data.copy()
        idata['rule']=arule
        idata['exp_id']=exp_id
        idata['form'] = form
        if 'condition' in request.session and request.session['condition'] != '':
            idata['condition'] = request.session['condition']
        print( str(data['consequence']))
        print(str(data['condition']))
        # form = Form_rule(data)
        print('//////////////////')
        # print(form['consequence'])
        request.session['sender'] = 'ServiceUser:user_expertrule_edit'
        request.session['rulind'] = rule_id
        return render(request, 'RuleManager/edit_rule.html',idata)
    else:
        # return HttpResponse('invalid data:=='+'---'+request.method)
        form = Form_rule(request.POST)
        if form.is_valid():
            cond = form.cleaned_data['condition']
            cons = form.cleaned_data['consequence']
            pub_dt = form.cleaned_data['pub_date']
            domain = form.cleaned_data['domain']
            Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(pk=rule_id).update(condition=cond, consequence=cons, pub_date=pub_dt, domain=domain)
            request.session['condition'] = ''
            return HttpResponseRedirect(reverse('ServiceUser:user_expertrules', args=[exp_id]))
        else:
            arule = get_object_or_404(Rules, id=rule_id)
            formerrors = form.errors
            if 'pub_date' in  formerrors:
                errordata={'condition':request.POST.get('condition', False),
                'consequence':request.POST.get('consequence', False),
                           'pub_date':'***اطلاعات با فرمت صحیح وارد نشده است***',
                           'domain':request.POST.get('domain',False)}
                # form = Form_rule(errordata)
                print(form['pub_date'])
            data = {'ownerexpert': Expertmodel.objects.filter(appuser__user_auth__username=username),
                    'condition': form['condition'].value(),'domain': Domain.objects.filter(id=form['domain'].value()),
                    'consequence': Facts.objects.filter(id=form['consequence'].value()),'pub_date':form['pub_date'].value()}
            data['rule']=arule
            data['exp_id']=exp_id
            data['form'] = form

            data['sender']='ServiceUser:user_expertrule_edit'
            return render(request, 'RuleManager/edit_rule.html',data)
            # return HttpResponse(form.as_table())


@login_required
@permission_required('RuleManager.add_facts')
def create_fact_by_model(request,  exp_id):
    username = request.user.username
    if len(request.POST) > 0:
        form = Form_fact(request.POST)
        if form.is_valid():
            form.save(commit=True)
            if request.is_ajax:
            # return HttpResponseRedirect(reverse(request.POST['sender']))
            # return HttpResponseRedirect(reverse(request.POST['responsepage']))
                return  JsonResponse("success", safe=False)
            else:
                return HttpResponseRedirect(reverse('ServiceUser:user_expertfacts', args=[exp_id]))
        else:
            if request.is_ajax:
                print('ajerr:'+str(list(form.errors)))
                print(form.errors.as_data())
                return JsonResponse("failure", safe=False)
            else:
                return render(request, 'RuleManager/create_fact_by_model.html',{'form': form,})
    else:
        data= {'ownerexpert':get_object_or_404(Expertmodel, id=exp_id),
               'domain':Domain.objects.filter(ownerexpert__appuser__user_auth__username=username)
               }
        form = Form_fact(data)
        print(exp_id)
        print(data['ownerexpert'])
        # request.session['sender'] = 'create_fact'
        return render(request, 'RuleManager/create_fact_by_model.html',{'form': form})


@login_required
@permission_required('RuleManager.add_rules')
def create_condition(request,  exp_id):
    username = request.user.username
    if len(request.POST) > 0:
        error = False
        # form = Compose_Condition(request.POST)
        if 'composedcondition' in request.POST:
            condition = request.POST['composedcondition']
        else:
            error = True
        if not error:
            request.session['condition'] =condition
            if request.session['sender']== 'ServiceUser:user_expertrule_edit':
                return HttpResponseRedirect(reverse(request.session['sender'],
                                                args=( exp_id, request.session['rulind'],)))
            else:
                return HttpResponseRedirect(reverse('ServiceUser:user_expertnewrule',args=[exp_id]))
        else:
            domain = Domain.objects.get(Q(ownerexpert__appuser__user_auth=request.user) & Q(ownerexpert__exact=exp_id)).id
            factlist = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(
                Q(fact_effect=Facts._CONDITION)| Q(fact_effect=Facts._SUBJECT)).filter(domain_id=domain)
            data = {'facts':factlist,'exp_id':exp_id}
            return render(request, 'RuleManager/create_condition.html',data)

    else:
        if 'domain' in request.POST:
            domain = request.POST['domain']
            print('dom:'+domain)
        else:
            try:
                domain = Domain.objects.get(Q(ownerexpert__appuser__user_auth=request.user) & Q(ownerexpert__exact=exp_id)).id
                print(request.GET)
                print('dom in else:' + str(domain))
                print('exp_id:'+str(exp_id))
                print('username:'+username)
            except Domain.DoesNotExist:
                print(
                    "هیچ حوزه دانشی یافت نگردید ")
                # raise Http404(
                    # "هیچ حوزه دانشی یافت نگردید ")
                return HttpResponseRedirect(reverse('ServiceUser:user_expertnewdomain',args=[exp_id]))

        # factlist=['یک یا چند گزینه را انتخاب کنید']
        # for f in Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(fact_effect=Facts._CONDITION).filter(domain_id=domain):
            # print(f)
            # factlist.append(f.fact)
        factlist = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user).\
            filter(Q(fact_effect=Facts._CONDITION)| Q(fact_effect=Facts._SUBJECT)).filter(domain_id=domain)
        data = {'facts':factlist,'exp_id':exp_id}
        # return render(request, 'RuleManager/create_condition.html',{'form': form,'sender':request.session['sender']})
        return render(request, 'RuleManager/create_condition.html',data)


