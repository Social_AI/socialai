import os, sys

from hazm import Normalizer, POSTagger, word_tokenize
from Contract_Assistant.settings import BASE_DIR
normalizer = Normalizer()
tagger = POSTagger(model=os.path.join(BASE_DIR,'static/ExpertSystem/resources', 'postagger.model'))

def to_persian( conds):
    print('IN TO_PERSIAN', file=sys.stderr)
    # conds is just a  fact
    if ('AND' not in conds) and ('OR' not in conds):
        if 'NOT ' in conds:
            purefact = conds.split('NOT ')[1]
            taggedfact = tagger.tag(word_tokenize(purefact))
            print('taggedfact:')
            print(taggedfact)
            last = taggedfact[len(taggedfact)-1]
            print('last"')
            print(last)
            print(last[0])
            print(last[1])
            if last[1] == 'V':
                Verb = last[0].replace('_', ' ')
                if Verb == 'است':
                    N_Verb = 'نیست'
                    print('N_Verb: '+N_Verb)
                    mainfact = purefact.split(Verb)[0]
                    print('mainfact: '+mainfact)
                    return mainfact + N_Verb
                elif Verb == 'باشد':
                    priorlast = taggedfact[len(taggedfact)-2]
                    Verb = priorlast[0]
                    N_Verb = 'ن'+Verb
                    print('N_Verb: '+N_Verb)
                    N_fact = purefact.replace(Verb, N_Verb)
                    return N_fact
                else:
                    N_Verb = 'ن'+Verb
                    print('N_Verb: '+N_Verb)
                    mainfact = purefact.split(Verb)[0]
                    print('mainfact: '+mainfact)
                    return mainfact + N_Verb

            else:
                return conds
        else:
            return conds
    else:
        tempres = ''
        splited = conds.partition(' AND ')
        if ' AND ' not in splited:
            if ' OR ' not in splited[0]:
                tempres += to_persian(splited[0])
                print('tempres:'+tempres)
                return tempres
            else:
                splited = splited[0].partition(' OR ')
                tempres += to_persian(splited[0])
                tempres += ' یا '
                tempres += to_persian(splited[2])
                print('tempres:'+tempres)
                return tempres
        else:
            tempres += to_persian(splited[0])
            tempres += ' و '
            tempres += to_persian(splited[2])
            print('tempres:'+tempres)
            return  tempres
