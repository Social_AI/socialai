"""Contract_Assistant URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import views as auth_views
from ServiceUser.UserManagement import logoutuser, appuser, loginuser

urlpatterns = [
    path('admin/', admin.site.urls),
    path('signin/', loginuser.verifyuser, name='signin'),
    path('signout/', logoutuser.finishconnection, name='sinout'),
    # path('register/', appuser.pricing, name='register'),
    path('register/', appuser.register2, name='register'),
    path('pricing/', appuser.pricing, name='princing'),
    path('RuleManager/', include('RuleManager.urls')),
    path('rulemanager/', include('RuleManager.urls')),
    path('ruleManager/', include('RuleManager.urls')),
    path('Rulemanager/', include('RuleManager.urls')),
    path('ServiceUser/', include('ServiceUser.urls')),
    # path('create-rule/', include('RuleManager.urls')),
    path('expertsystem/', include('ExpertSystem.urls')),
    path('Expertsystem/', include('ExpertSystem.urls')),
    path('', include('ExpertSystem.urls')),
    path('plans/', include('plans.urls')),
    path('zarinpal/', include('zarinpal.urls')),
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='registration/newpassword_reset_form.html',
        email_template_name = 'registration/newpassword_reset_email.html',
        subject_template_name= 'registration/newpassword_reset_subject.html'), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='registration/newpassword_reset_done.html')
         , name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='registration/newpassword_reset_confirm.html')
         , name='password_reset_confirm'),
    path('reset/complete/', auth_views.PasswordResetCompleteView.as_view(template_name='registration/newpassword_reset_completed.html')
         , name='password_reset_complete'),
]
