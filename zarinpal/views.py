# -*- coding: utf-8 -*-
# Github.com/Rasooll
from decimal import Decimal
import jsonpickle, json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect ,reverse
# from zeep import Client
from django import forms
import requests
from plans.models import Order
from ServiceUser.models import Payment

# MERCHANT = '50c422e3-d5d3-4fc3-bf18-7f2569c73d75' اصلی
MERCHANT = '4ced0a1e-4ad8-4309-9668-3ea3ae8e8897'
# client = Client('https://www.zarinpal.com/pg/services/WebGate/wsdl')
SOAP = 'https://api.zarinpal.com/pg/v4/payment/request.json'
# SOAP = 'https://next.zarinpal.com/pg/v4/soap/services/WebGate/service'
VERIFY = 'https://api.zarinpal.com/pg/v4/payment/verify.json'
#https://www.zarinpal.com/pg/services/WebGate/wsdl
# amount = 5000  # Toman / Required
CallbackURL = 'https://socialai.pythonanywhere.com/zarinpal/'

class Payment_form(forms.Form):
    amount = forms.DecimalField(min_value=1000.00,max_digits=11,decimal_places=2,label='جمع کل سفارش',required=True)
    description = forms.CharField(label='توضیحات تراکنش',required=True)
    email = forms.EmailField(required=False,label='ایمیل(اختیاری)')
    mobile = forms.CharField(required=False,max_length=11,min_length=11,label='شماره همراه(اختیاری)')

    def clean(self):
        cleaned_data = super(Payment_form, self).clean()
        phone = cleaned_data.get('mobile')
        print('phone in clean form :'+phone)
        print(cleaned_data.get('description'))
        cleaned_data['amount'] = cleaned_data['amount']/Decimal(10)
        print(type(cleaned_data['amount']))
        print('cleaned_data amount in clean form :'+str(cleaned_data['amount']))
        if not phone.isnumeric() and len(phone):
            # if not phone is None:
            raise forms.ValidationError("فقط اعداد مجاز می باشند.")
            # self.add_error('phone', "فقط اعداد مجاز می باشند.")
        return cleaned_data


def send_request(request, pk):
    request.session['order_ind'] = pk
    print('ord_ind:'+str(pk))
    if len(request.POST):
        payform = Payment_form(request.POST)
        if payform.is_valid():
            amount = payform.cleaned_data['amount']
            # amount = 1010
            print('amount in payform.is_valid :'+str(amount))
            request.session['amount'] = jsonpickle.encode(amount)
            description = payform.cleaned_data['description']
            email = payform.cleaned_data['email']
            mobile = payform.cleaned_data['mobile']
            print('POST is:'+str(request.POST))
            print('Pay description is:'+str(payform.cleaned_data['description']))
            request_data = {'amount':amount, 'description':description,
                'email':email, 'mobile':mobile,'CallbackURL':CallbackURL
                }
            result = requests.post(SOAP, data = request_data)
            # result = result.json()
            # result = client.service.PaymentRequest(MERCHANT, amount, description, email, mobile, CallbackURL)
            print('returend from PaymentRequest')
            print(result)
            result.raise_for_status()
            # if result.Status == 100:
            if result['data']['Status'] == 100:
                # return redirect('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
                return redirect('https://www.zarinpal.com/pg/StartPay/' + str(result['data']["authority"]))
            else:
                return HttpResponse('Error code: ' + str(result['data']['Status']))
        else:
            return HttpResponse('Form not valid: ' + str(payform.errors))
    else:
        return HttpResponse('Error Not Post')



def verify(request):
    ord_ind = request.session['order_ind']
    if request.GET.get('Status') == 'OK':
        ord_amount = jsonpickle.decode(request.session['amount'])
        verify_data = {'merchant_id':MERCHANT, 'authority':request.GET['Authority'],
            'amount':ord_amount
            }
        result = requests.post(VERIFY, data = verify_data)
        result = result.json()
        print(result)
        # result = client.service.PaymentVerification(MERCHANT, request.GET['Authority'], ord_amount )
        # if result.Status == 100:
        if result['data']['code'] == 100:
            # return HttpResponse('Transaction success.\nRefID: ' + str(result.RefID))
            # request.session['ref_id'] = result.RefID
            request.session['ref_id'] = result['data']['ref_id']
            curr_ord = Order.objects.get(id=ord_ind)
            curr_ord.complete_order()
            # ord_pay = Payment(ref_id=result.RefID, user =request.user, order=curr_ord,
                # amount=ord_amount*Decimal(10), completed= curr_ord.completed )
            ord_pay = Payment(ref_id=result['data']['ref_id'], user =request.user, order=curr_ord,
                amount=ord_amount*Decimal(10), completed= curr_ord.completed )
            ord_pay.save()
            return HttpResponseRedirect(reverse('order_payment_success', args=[ord_ind]))
        # elif result.Status == 101:
        elif result['data']['code'] == 101:
            request.session['ref_id'] = result.RefID
            return HttpResponse('Transaction submitted : ' + str(result['data']['code']))
            # return HttpResponseRedirect(reverse('order_payment_failure', args=[1]))
        else:
            # return HttpResponse('Transaction failed.\nStatus: ' + str(result.Status))
            return HttpResponseRedirect(reverse('order_payment_failure', args=[ord_ind]))
    else:
        return HttpResponseRedirect(reverse('order_payment_cancel', args=[ord_ind]))




    # pass