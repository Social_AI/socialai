# Github.com/Rasooll
from django.conf.urls import url
from . import views
from django.urls import path
app_name = 'zarinpal'

urlpatterns = [
    path('request/', views.send_request, name='request'),
    path('', views.verify , name='verify'),
]
