��    ,      |  ;   �      �  2   �     �  	             ,     5     F     X     _     g     x  �   �            
   -     8     =     C     R     W     h     m  	   �  	   �     �     �     �     �     �     �     �            	             5  #   J     n     s     y     �     �     �  j  �  A        ]     o     �  
   �     �     �     �     �     �     		  �   	  
   �	  &   �	     
     1
     :
     A
     Y
  %   b
     �
     �
     �
     �
     �
  $   �
  
   �
  
   �
  >        A     N  
   e     p     }     �     �  @   �        
     !   !     C     Y     b        %   "      $                    )                                 &      #                
       (         +                                   *   !   '   	             ,                               
            VAT is not applied to order.
         Cena&nbsp;j.&nbsp;netto Completed Confirm order Continue Data sprzedaży: Data wystawienia: EU VAT Faktura Faktura PROFORMA Faktura VAT DUPLIKAT If you downgrade your plan please remember that new lower limits are used immediately after
                finishing the order. Ilość Invoice (duplicate) ID Invoice ID J.m. KOPIA Kwota&nbsp;VAT L.p. List of payments Name Nazwa&nbsp;usługi Net price ORYGINAŁ Odwrotne obciążenie. Order confirmation ID Rabat Razem: See our great value plans Sposób zapłaty: Termin zapłaty: Total VAT VAT total Wartość&nbsp;brutto Wartość&nbsp;netto You do not have any payment so far. n.p. order płatność elektroniczna ref_id sztuk zapłacono dnia Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-03-30 10:32+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
مالیات به سفارش اعمال نشده است.         قیمت واحد تاریخ پرداخت تایید سفارش ادامه تاریخ سفارش ارسال شده مالیات ایران فاکتور پیش فاکتور کپی فاکتور لطفا به یاد داشته باشید اگر شما اشتراک تان را تنزل دهید، محدودیت های کمتر جدید بعد از آن بلافاصله اعمال می شوند. تعداد شماره(کپی شده) فاکتور شماره فاکتور واحد کپی میزان مالیات ردیف لیست پرداخت های موفق نام شرح قیمت خالص اصلی -شارژ معکوس. شماره تاییدیه سفارش تخفیف مجموع سایر اشتراک های مناسب ما را ببینید پرداخت مهلت پرداخت: مجموع مالیات مجموع مالیات جمع کل با مالیات جمع کل شما تاکنون پرداخت موفقی نداشته اید! بدون مالیات سفارش پرداخت الکترونیکی شماره ارجاع ریال پرداخت شده 