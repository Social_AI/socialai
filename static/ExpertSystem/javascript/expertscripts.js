var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("activecontent");
    var content = this.nextElementSibling;
    // var content = this.children[0] ;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  });
}
function gotosettings() {
    if (user === 'demouser') {
        if (run_mode === 'modern') {
            window.location.href = "/expertsystem/contractdemo";
        } else {
            window.location.href = "/expertsystem/classiccontractdemo";
        }
    } else {
        if (run_mode === 'modern') {
             window.location.href = '/ServiceUser/experts/' + expert_id + '/start';

        } else {
           window.location.href = '/ServiceUser/experts/' + expert_id + '/startclassic';
        }
    }
}
function showmewhy() {
    document.getElementById('whythis').innerHTML='<strong>'+whythisdesc;
}
function showmehow() {
    document.getElementById('howthis').innerHTML='<strong>'+howthisdesc;
}
function gotopreviouslevel() {
    if (sysstep === 'beginsession'){
        gotosettings();
    }
    else {
        var  xmlhttp, ResponseObj;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            ResponseObj = JSON.parse(this.responseText);
            sysstep = ResponseObj[4][1];
            // alert('in rewind script:'+sysstep);
            if (sysstep === 'beginsession'){
                // alert('in rewind script go to expertsystem');
                // window.location.href = "/expertsystem/";
                 gotosettings();
            }else {
                whythisdesc = ResponseObj[2][1];
                howthisdesc = ResponseObj[3][1];
                question = ResponseObj[0][1];
                // var sysmsghtml =  'پیام متخصص:' + '<br><br>'
                var sysmsghtml="<div class='notemsg'>";
                sysmsghtml += "<div class='noteheader'>"+"پیام متخصص"+"</div>";
                // sysmsghtml += question + '<br><br>';
                sysmsghtml +='<p>'+question+ '<br><br>';
                var qtype = ResponseObj[1][1];
                if (qtype === 'Y/N') {
                    sysmsghtml += '        <select name="ynselect" id="ynselect">' +
                                             '<option value="بلی" >'+
                                                    'بلی'+
                                             '</option>'+
                                             '<option value="خیر" >'+
                                                    'خیر'+
                                             '</option>'+
                                '        </select>';
                    sysmsghtml += '        <br>' +
                        '        <br>' +
                        '        <button type="button" id="prevBtn" name="prevBtn"  class="button button1" onclick="gotopreviouslevel()">مرحله قبل</button>'+
                        '        <button type="button" id="nextBtn" name="nextBtn" class="button button1" onclick="getajaxresponce()">مرحله بعد</button>'+'</p></div><br>';
                    if (!question.includes('آیا ادامه می دهید'))
                    {
                        sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
                        // sysmsghtml+='<p>'
                        sysmsghtml +='<p >        <button  type="button" id="whythisbtn" class="button button1" onclick="showmewhy()">دلیل این سوال</button>'+
                        '            <br>' +
                        '            <h4 id="whythis" ></h4></p></div>';
                    }else{
                        sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
                        sysmsghtml +='<p >            <button  type="button" id="howthisbtn"   class="button button1" onclick="showmehow()" >دلیل این پاسخ</button>'+
                        '            <br>' +
                        '            <h4 id="howthis" ></h4></p></div></div>';
                    }
                } else if (qtype === 'EXP') {
                              sysmsghtml +='<label for="expanswer">'+
                                  '        پاسخ شما:'+
                        '        <input name="expanswer" type="number" id="expanswer" min="0" max="100" placeholder="فقط اعداد مجاز می باشند"></label>'+
                        '        <br>' +
                        '        <br>' ;
                    sysmsghtml +=
                        '        <button type="button" id="prevBtn" name="prevBtn"  class="button button1" onclick="gotopreviouslevel()">مرحله قبل</button>'+
                        '        <button type="button" id="nextBtn" name="nextBtn" class="button button1" onclick="getajaxresponce()">مرحله بعد</button>'+'</p></div><br>';
                    if (!question.includes('آیا ادامه می دهید'))
                    {
                        sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
                        sysmsghtml +='<p >        <button  type="button" id="whythisbtn" class="button button1" onclick="showmewhy()">دلیل این سوال</button>'+
                        '            <br>' +
                        '            <h4 id="whythis" class="infomsg"></h4></p></div>';
                    }else{
                        sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
                        sysmsghtml +='<p >            <button  type="button" id="howthisbtn"   class="button button1" onclick="showmehow()" >دلیل این پاسخ</button>'+
                        '            <br>' +
                        '            <h4 id="howthis" class="infomsg" ></h4></p></div></div>';
                    }
                } else {
                    sysmsghtml += '            <br>' +
                        '      <button type="button" id="prevBtn" name="prevBtn"  class="button button1" onclick="gotopreviouslevel()">مرحله قبل</button>'+'</p></div>'+
                        '            <br>' ;
                    sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
                    sysmsghtml +='<p >            <button  type="button" id="howthisbtn"   class="button button1" onclick="showmehow()" >دلیل این پاسخ</button>'+
                        '            <br>' +
                        '            <h4 id="howthis" ></h4></p></div></div>';
                    document.getElementById('howthis').innerText = '';
                }
                document.getElementById('sysmsg').innerHTML = sysmsghtml;
                // document.getElementById('sysmsg').innerHTML= 'sysmsghtml';
            }
        }else if(this.status==500){
          alert('پردازش درخواست شما با خطا مواجه گردید');
        }else if(this.status==404){
          alert('ارتباط با سرور با خطا مواجه گردید!');
        }
        };
    xmlhttp.open("POST", '/rewindengine/' , true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlhttp.setRequestHeader('HTTP_X_REQUESTED_WITH','XMLHttpRequest');
    xmlhttp.send('state=stepback');

    }
}

function getajaxresponce() {
    var userinputval,sysmsghtml,xmlhttp, ResponseObj,question,qtype;
    if (document.getElementById('ynselect')){
      userinputval = document.getElementById('ynselect').value;
    //   alert(userinputval);
    //   alert('yn');
    }
    else {
      userinputval = document.getElementById('expanswer').value;
    //   alert(userinputval);
    //   alert('exp');
          }
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
        // alert('here2');
      ResponseObj = JSON.parse(this.responseText);
      question = ResponseObj[0][1];
      qtype = ResponseObj[1][1];
      whythisdesc = ResponseObj[2][1];
      howthisdesc = ResponseObj[3][1];
      sysstep = ResponseObj[4][1];
      // sysmsghtml =  'پیام متخصص:' + '<br><br>'
      var sysmsghtml="<div class='notemsg'>";
      sysmsghtml += "<div class='noteheader'>"+"پیام متخصص"+"</div>";
      // sysmsghtml += question + '<br><br>';
        sysmsghtml +='<p>'+question+ '<br><br>';
      if (qtype === 'Y/N'){

        sysmsghtml+='        <select name="ynselect" id="ynselect">' +
             '<option value="بلی" >'+
                 'بلی'+
             '</option>'+
             '<option value="خیر" >'+
                 'خیر'+
             '</option>'+
                    '        </select>';
              sysmsghtml += '        <br>' +
                        '        <br>' +
                        '        <button type="button" id="prevBtn" name="prevBtn"  class="button button1" onclick="gotopreviouslevel()">مرحله قبل</button>'+
                        '        <button type="button" id="nextBtn" name="nextBtn" class="button button1" onclick="getajaxresponce()">مرحله بعد</button></p></div><br>';
        if (!question.includes('آیا ادامه می دهید'))
        {
                        sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
                        sysmsghtml +='<p >        <button  type="button" id="whythisbtn" class="button button1" onclick="showmewhy()">دلیل این سوال</button>'+
                        '            <br>' +
                        '            <h4 id="whythis" ></h4></p></div>';
        }else {
            sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
            sysmsghtml +='<p >            <button  type="button" id="howthisbtn"   class="button button1" onclick="showmehow()" >دلیل این پاسخ</button>'+
                        '            <br>' +
                        '            <h4 id="howthis" ></h4></p></div></div>';
        }
      }else if (qtype === 'EXP'){

                              sysmsghtml +='<label for="expanswer">'+
                                  '        پاسخ شما:'+
                        '        <input name="expanswer" type="number" id="expanswer" min="0" max="100" placeholder="فقط اعداد مجاز می باشند"></label>'+
                        '        <br>' +
                        '        <br>' ;
                    sysmsghtml +=
                        '        <button type="button" id="prevBtn" name="prevBtn"  class="button button1" onclick="gotopreviouslevel()">مرحله قبل</button>'+
                        '        <button type="button" id="nextBtn" name="nextBtn" class="button button1" onclick="getajaxresponce()">مرحله بعد</button></p></div><br>';
        if (!question.includes('آیا ادامه می دهید'))
        {
                        sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
                        sysmsghtml +='<p >        <button  type="button" id="whythisbtn" class="button button1" onclick="showmewhy()">دلیل این سوال</button>'+
                        '            <br>' +
                        '            <h4 id="whythis" ></h4></p></div>';
        }else {
            sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
            sysmsghtml +='<p >            <button  type="button" id="howthisbtn"   class="button button1" onclick="showmehow()" >دلیل این پاسخ</button>'+
                        '            <br>' +
                        '            <h4 id="howthis" ></h4></p></div></div>';
        }
      }else {
                    // alert('im here');
                    sysmsghtml += '            <br>' +
                        '            <button type="button" id="prevBtn" name="prevBtn"  class="button button1" onclick="gotopreviouslevel()">مرحله قبل</button>'+'</p></div><br>'+
                        '            <br>' ;
                    sysmsghtml+='<div class="infomsg"><div class="infoheader ">مبانی استنتاج</div>';
                    sysmsghtml +='<p >            <button  type="button" id="howthisbtn"   class="button button1" onclick="showmehow()" >دلیل این پاسخ</button>'+
                        '            <br>' +
                        '            <h4 id="howthis" ></h4></p></div></div>';
                    if(document.getElementById('howthis')!= null){
                        document.getElementById('howthis').innerText = '';
                    }
        }
      document.getElementById('sysmsg').innerHTML= sysmsghtml;
      //   document.getElementById('sysmsg').innerHTML= 'sysmsghtml';
      }else if(this.status===500){
          alert('پردازش درخواست شما با خطا مواجه گردید');
      }else if(this.status===404){
          alert('ارتباط با سرور با خطا مواجه گردید!');
      }
    };
    xmlhttp.open("POST", '/expert_contract_byajax/' , true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlhttp.setRequestHeader('HTTP_X_REQUESTED_WITH','XMLHttpRequest');
    xmlhttp.send('userinput='+userinputval);
}

function updatedetailfacts() {
    var  xmlhttp, ResponseObj;
    var csrftoken = facttoken;
    let domid = document.getElementById('datalist').value;
    var subjval ;
    let detailfact = document.getElementById('factlist-detail[]');
    if (document.getElementById('related_detail').checked === true ){
         subjval = document.getElementById('factlist-subject').value;
    }else {
         subjval = 'all_detail';  //all_detail
    }
    detailfact.innerHTML='';
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        // alert(this.responseText);
        if (this.readyState === 4 && this.status === 200) {
            ResponseObj = JSON.parse(this.responseText);
            if(ResponseObj !== '') {
                for (x in ResponseObj) {
                    var opt = document.createElement('option');
                    // if (ResponseObj[x].includes('NOT ')){
                    //     alert('not inclided');
                    //     opt.text =topersian(ResponseObj[x]);
                    // }else{
                    //     opt.text = ResponseObj[x];
                    // }
                    // alert(ResponseObj[x]);
                    // alert(typeof ResponseObj[x]);
                    // alert(ResponseObj[x]);
                    if(typeof ResponseObj[x] ==="object" || typeof ResponseObj[x] ==="Object"){
                        opt.text = ResponseObj[x].persian_fact;
                        opt.value = ResponseObj[x].fact;
                    }else{
                        opt.text = ResponseObj[x];
                        opt.value = ResponseObj[x];
                    }
                    detailfact.options.add(opt);
                }
            }
            else{
                detailfact.title="هیچ مشخصه مرتبط با موضوع انتخاب شده در سامانه ثبت نشده است!";
            }
        }else if(this.status===500){    //Server Error
            alert('پردازش درخواست شما با خطا مواجه گردید');
        }else if(this.status===404){    //Client Error
            alert('ارتباط با سرور با خطا مواجه گردید!');
        }else {
        }
    };
    // alert('post');
    xmlhttp.open("POST", "update_detailfacts/", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
    var data= {
        domainid: domid,
        subject_fact: subjval,
    };
    xmlhttp.send('domainid='+data.domainid+'&subject_fact='+data.subject_fact);
    // alert('send');
}

