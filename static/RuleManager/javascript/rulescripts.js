if(window.attachEvent) {
    if (window.location.href.includes('facts')){
        window.attachEvent('onload', managefactpagination);
    }else {
        window.attachEvent('onload', managepagination);
    }
} else {
    if(window.onload) {
        var curronload = window.onload;
        var newonload = function(evt) {
            curronload(evt);
            if (window.location.href.includes('facts')){
                managefactpagination(evt);
            }else {
                managepagination(evt);
            }
        };
        window.onload = newonload;
    } else {
        if (window.location.href.includes('facts')){
            window.onload = managefactpagination;
        }else {
            window.onload = managepagination;
        }
    }
}


function managepagination(total=totalpages, currpage) {
    // alert('in pagin:'+total);
    if (typeof (total) == "object")
    {
        // alert('object')
        var curr = document.getElementsByClassName('current');
        for (i = 0; i < curr.length; i++) {
            curr[i].innerText = '';
            var cp =currentpage.toString();
            cp = cp.split("").reverse().join("");
            var tp = totalpages.toString();
            tp = tp.split("").reverse().join("");
            curr[i].innerText = 'صفحه ' + cp + ' از ' + tp;
        }
        return;
    }
    total = Number(total);
    currpage = Number(currpage);
    // alert('dff');
    var steps = document.getElementsByClassName('step-links');
    for (i = 0; i < steps.length; i++) {
        steps[i].innerHTML = '';
        if (currpage > 1){
          var fa = document.createElement('a');
                // Create the text node for anchor element.
          var flink = document.createTextNode('اولین <<');
                // Append the text node to anchor element.
          fa.appendChild(flink);
                // Set the title.
          fa.title = 'اولین صفحه';
          fa.text =  'اولین <<';
                // Set the href property.
          fa.href = "javascript:filterrules(1)";
          steps[i].appendChild(fa);
          var pa = document.createElement('a');
                // Create the text node for anchor element.
          var plink = document.createTextNode('قبلی');
                // Append the text node to anchor element.
          pa.appendChild(plink);
                // Set the title.
          pa.title = ' صفحه قبلی';
          pa.text =  '  قبلی';
                // Set the href property.
          pa.href = "javascript:filterrules("+(currpage-1).toString()+")";
                // Append the anchor element to the body.
          steps[i].appendChild(pa);
    }
        var s = document.createElement('span');
        s.className = 'current';
        var currtext = document.createTextNode('صفحه '+(currpage).toString()+' از '+(total).toString())
        s.appendChild(currtext);
        steps[i].appendChild(s);
        if (currpage >= 1 && currpage < total){
          var na = document.createElement('a');
                // Create the text node for anchor element.
          var nlink = document.createTextNode('بعدی <<');
                // Append the text node to anchor element.
          na.appendChild(nlink);
                // Set the title.
          na.title = ' صفحه بعدی';
          na.text =  '  بعدی';
                // Set the href property.
          na.href = "javascript:filterrules("+(currpage+1).toString()+")";
          steps[i].appendChild(na);
          var la= document.createElement('a');
                // Create the text node for anchor element.
          var llink = document.createTextNode('آخرین>>');
                // Append the text node to anchor element.
          la.appendChild(llink);
                // Set the title.
          la.title = ' آخرین صفحه ';
          la.text =  'آخرین>>';
                // Set the href property.
          la.href = "javascript:filterrules("+(total+1).toString()+")";
                // Append the anchor element to the body.
          steps[i].appendChild(la);
    }
    }
}

function managefactpagination(total=totalpages, currpage) {
    // alert('in pagin:'+total);
    if (typeof (total) == "object")
    {
        // alert('object')
        var curr = document.getElementsByClassName('current');
        for (i = 0; i < curr.length; i++) {
            curr[i].innerText = '';
            var cp =currentpage.toString();
            cp = cp.split("").reverse().join("");
            var tp = totalpages.toString();
            tp = tp.split("").reverse().join("");
            curr[i].innerText = 'صفحه ' + cp + ' از ' + tp;
        }
        return;
    }
    total = Number(total);
    currpage = Number(currpage);
    // alert('dff');
    var steps = document.getElementsByClassName('step-links');
    for (i = 0; i < steps.length; i++) {
        steps[i].innerHTML = '';
        if (currpage > 1){
          var fa = document.createElement('a');
                // Create the text node for anchor element.
          var flink = document.createTextNode('اولین <<');
                // Append the text node to anchor element.
          fa.appendChild(flink);
                // Set the title.
          fa.title = 'اولین صفحه';
          fa.text =  'اولین <<';
                // Set the href property.
          fa.href = "javascript:filterfacts(1)";
          steps[i].appendChild(fa);
          var pa = document.createElement('a');
                // Create the text node for anchor element.
          var plink = document.createTextNode('قبلی');
                // Append the text node to anchor element.
          pa.appendChild(plink);
                // Set the title.
          pa.title = ' صفحه قبلی';
          pa.text =  '  قبلی';
                // Set the href property.
          pa.href = "javascript:filterfacts("+(currpage-1).toString()+")";
                // Append the anchor element to the body.
          steps[i].appendChild(pa);
    }
        var s = document.createElement('span');
        s.className = 'current';
        var currtext = document.createTextNode('صفحه '+(currpage).toString()+' از '+(total).toString())
        s.appendChild(currtext);
        steps[i].appendChild(s);
        if (currpage >= 1 && currpage < total){
          var na = document.createElement('a');
                // Create the text node for anchor element.
          var nlink = document.createTextNode('بعدی <<');
                // Append the text node to anchor element.
          na.appendChild(nlink);
                // Set the title.
          na.title = ' صفحه بعدی';
          na.text =  '  بعدی';
                // Set the href property.
          na.href = "javascript:filterfacts("+(currpage+1).toString()+")";
          steps[i].appendChild(na);
          var la= document.createElement('a');
                // Create the text node for anchor element.
          var llink = document.createTextNode('آخرین>>');
                // Append the text node to anchor element.
          la.appendChild(llink);
                // Set the title.
          la.title = ' آخرین صفحه ';
          la.text =  'آخرین>>';
                // Set the href property.
          la.href = "javascript:filterfacts("+(total+1).toString()+")";
                // Append the anchor element to the body.
          steps[i].appendChild(la);
    }
    }
}

function filterrules(page=1) {
        //alert('in filter');
        let cond = document.getElementById('id_condition').value;
        let cons = document.getElementById('id_consequence').value;
        let dom = document.getElementById('id_domain').value;
        let csrftoken =  csrf;
        //alert(csrftoken);
        var  xmlhttp, ResponseObj;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
       if (this.readyState === 4 && this.status === 200) {
            ResponseObj = JSON.parse(this.responseText);
            var tablerule = document.getElementById('id_tablerule');
            var rulebody = document.getElementById('id_tbody');
            rulebody.innerText = '';
            var pagerules =JSON.parse( ResponseObj['Rule_list']);
            for (x in pagerules) {
                var tr = document.createElement("TR");

                // ********id section********
                var td_id = document.createElement("TD");
                // Create anchor element.
                var a = document.createElement('a');
                // Create the text node for anchor element.
                var link = document.createTextNode(pagerules[x].id);
                // Append the text node to anchor element.
                a.appendChild(link);
                // Set the title.
                a.title = pagerules[x].id;
                a.text = pagerules[x].id;
                // Set the href property.
                a.href = '/ServiceUser/experts/'+exp_id+'/RuleManager/'+(pagerules[x].id).toString();
                // Append the anchor element to the body.
                td_id.appendChild(a);
                tr.appendChild(td_id);
                //********id section********

                //condition section
                var td_cond = document.createElement("TD");
                //   var condtext = document.createTextNode(truncchars(topersian(pagerules[x].condition),100));
                var condtext = document.createTextNode(truncchars(pagerules[x].condition,100));
                td_cond.appendChild(condtext);
                tr.appendChild(td_cond);
                //condition section

                //********consequence section************
                var td_cons = document.createElement("TD");
                var constext = document.createTextNode(pagerules[x].consequence__fact);
                td_cons.appendChild(constext);
                tr.appendChild(td_cons);
                //********consequence section************

                //********domain section*********
                var td_dom = document.createElement("TD");
                var domtext = document.createTextNode(truncchars(pagerules[x].domain__desc,50));
                td_dom.appendChild(domtext);
                tr.appendChild(td_dom);
                //********domain section*********

                //********edit button section*********
                var td_edtbtn = document.createElement("TD");
                var a_edit = document.createElement('a');
                a_edit.title ='ویرایش این قانون';
                a_edit.className = 'button button1';
                a_edit.innerHTML = '<i class="fa fa-pencil-square-o fa-lg" ></i>';
                a_edit.style.borderStyle = "hidden" ;
                a_edit.href = a.href+'/edit';
                td_edtbtn.appendChild(a_edit);
                tr.appendChild(td_edtbtn);
                //********edit button section*********

                //********delete button section*********
                var td_delbtn = document.createElement("TD");
                var a_del = document.createElement('a');
                a_del.title ='حذف این قانون';
                a_del.className = 'button button1';
                a_del.innerHTML = '<i class="fa fa-trash fa-lg" ></i>';
                a_del.style.borderStyle= "hidden" ;
                a_del.href = a.href+'/delete';
                td_delbtn.appendChild(a_del);
                tr.appendChild(td_delbtn);
                //********delete button section*********
                rulebody.appendChild(tr);
            }  //end for loop
                 // document.getElementById("myTable").appendChild(rulebody);
            // alert('result-len:'+(pagerules.length).toString());
            var totalpages = ResponseObj.totalpages;
            // alert('t:'+totalpages);
            var currpage = ResponseObj.curr_page;
            // alert('c:'+currpage);
            managepagination(totalpages,currpage);
        }else{
     // alert('st:'+(this.status).toString()+'---rs'+this.readyState.toString());
    }
    };
  xmlhttp.open("POST", "/RuleManager/update_rule_list/", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
  //alert('curr filter::'+'condition='+cond+'&consequence='+cons+'&domain='+dom);
  xmlhttp.send('condition='+cond+'&consequence='+cons+'&domain='+dom+'&page='+page);
    }

function filterfacts(page=1) {
        // alert('in filter');
        let fact = document.getElementById('id_fact').value;
        let ftype = document.getElementById('id_facttype').value;
        let feffect = document.getElementById('id_facteffect').value;
        let fdomain = document.getElementById('id_domain').value;
        let csrftoken =  csrf;
        // alert(csrftoken);
        var  xmlhttp, ResponseObj;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
       if (this.readyState === 4 && this.status === 200) {
            ResponseObj = JSON.parse(this.responseText);
            var tablefact = document.getElementById('id_tablefact');
            var factbody = document.getElementById('id_tbody');
            factbody.innerText = '';
            var pagefacts =JSON.parse( ResponseObj['fact_list']);
            for (x in pagefacts) {
          var tr = document.createElement("TR");
          // ********id section********
          var td_id = document.createElement("TD");
          // Create anchor element.
          var a = document.createElement('a');
                // Create the text node for anchor element.
          // alert(pagerules[x].id);
          var link = document.createTextNode(pagefacts[x].id);
          // alert(pagefacts[x].id);
                // Append the text node to anchor element.
          a.appendChild(link);
                // Set the title.
          a.title = pagefacts[x].id;
          a.text = pagefacts[x].id;
                // Set the href property.
          a.href = '/ServiceUser/experts/'+exp_id+'/RuleManager/facts/'+(pagefacts[x].id).toString();

                // Append the anchor element to the body.
          td_id.appendChild(a);
          tr.appendChild(td_id);

          //********id section********
          //********fact section*********
          var td_fact = document.createElement("TD");
          var facttext = document.createTextNode(truncchars(pagefacts[x].fact,100));
           // alert(pagefacts[x].fact);
          td_fact.appendChild(facttext);
          tr.appendChild(td_fact);
          //*******fact section**********
          //********fact type section************
          var td_ftype = document.createElement("TD");
          var ftypetext = document.createTextNode(facttypedesc(pagefacts[x].fact_type));
          // alert(pagefacts[x].fact_type);
          td_ftype.appendChild(ftypetext);
          tr.appendChild(td_ftype);
          //********fact type section************
          //********fact effect section*********
          //       alert('here');
          var td_ffeffect = document.createElement("TD");
          // alert('here');
          // alert(pagefacts[x].toString());
          var feffecttext = document.createTextNode(facteffectdesc(pagefacts[x].fact_effect));
          // alert(pagefacts[x].fact_effect);
          td_ffeffect.appendChild(feffecttext);
          tr.appendChild(td_ffeffect);
          //********fact effect section*********
          //********fact domain section*********
          var td_fdomain = document.createElement("TD");
          var fdomaintext = document.createTextNode(truncchars(pagefacts[x].domain__desc,50));
           // alert(pagefacts[x].domain__desc);
          td_fdomain.appendChild(fdomaintext);
          tr.appendChild(td_fdomain);
          //********fact domain section*********

          //********edit button section*********
                var td_edtbtn = document.createElement("TD");
                var a_edit = document.createElement('a');
                a_edit.title ='ویرایش این حقیقت';
                a_edit.className = 'button button1';
                a_edit.innerHTML = '<i class="fa fa-pencil-square-o fa-lg" ></i>';
                a_edit.style.borderStyle = "hidden" ;
                a_edit.href = a.href+'/edit';
                td_edtbtn.appendChild(a_edit);
                tr.appendChild(td_edtbtn);
             //********edit button section*********
          //********delete button section*********
                var td_delbtn = document.createElement("TD");
                var a_del = document.createElement('a');
                a_del.title ='حذف این حقیقت';
                a_del.className = 'button button1';
                a_del.innerHTML = '<i class="fa fa-trash fa-lg" ></i>';
                a_del.style.borderStyle= "hidden" ;
                a_del.href = a.href+'/delete';
                td_delbtn.appendChild(a_del);
                tr.appendChild(td_delbtn);
            //********edit button section*********
            factbody.appendChild(tr);
      }
                 // document.getElementById("myTable").appendChild(rulebody);
            // alert('result-len:'+(pagefacts.length).toString());
            var totalpages = ResponseObj.totalpages;
            // alert('t:'+totalpages);
            var currpage = ResponseObj.curr_page;
            // alert('c:'+currpage);
            managefactpagination(totalpages,currpage);
        }else{
     // alert('st:'+(this.status).toString()+'---rs'+this.readyState.toString());
    }
    };
  xmlhttp.open("POST", "/RuleManager/update_fact_list/", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
  //alert('curr filter::'+'condition='+cond+'&consequence='+cons+'&domain='+dom);
  xmlhttp.send('fact='+fact+'&facttype='+ftype+'&facteffect='+feffect+'&page='+'&page='+page+'&domain='+fdomain);
    }

function truncchars(astr , val) {
        // """Truncate a string after `arg` number of characters."""
    //alert('in trun');
    var len = Number(val);
    // alert(len);
    var strlen =astr.length;
    // alert('in trun1:'+astr);
    // alert(strlen);
    if (strlen <= len){
        return astr;
    }
    else
        {
        var tempstr = astr.substring(0,len);
        // alert('in trun2:'+tempstr);
        tempstr+= '...';
        // alert('in trun3:'+tempstr);
        return tempstr;
    }
}

// function topersian(engstr) {
//     if( !(engstr.includes('AND')) && !(engstr.includes('OR')) ){
//         if (engstr.includes('NOT ')) {
//             var purefact = engstr.split('NOT ')[1]
//             if (purefact.includes('است')) {
//                 var mainfact = purefact.split('است')[0];
//                 return mainfact + 'نیست';
//             } else {
//                 mainfact = purefact.split('می باشد')[0];
//                 return mainfact + 'نمی باشد';
//             }
//         }else{
//             return engstr;
//             }
//     }else
//         {
//         if (engstr.includes(' AND ')){
//             var splitpos = engstr.indexOf(' AND ');
//             var splited1 = engstr.slice(0,splitpos);
//             var splited2 = engstr.slice(splitpos+5);
//             return topersian(splited1)+' و '+topersian(splited2);

//         }else {
//              splitpos = engstr.indexOf(' OR ');
//              splited1 = engstr.slice(0,splitpos);
//              splited2 = engstr.slice(splitpos+4);
//             return topersian(splited1)+' یا '+topersian(splited2);
//         }
//         }
//     }

function AddAndContinue() {
        var  xmlhttp, ResponseObj;

        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
        ResponseObj = JSON.parse(this.responseText);
            if (ResponseObj ==='success')
            {
                document.getElementById('result').innerHTML='حوزه دانش '+'<b>'+document.getElementById('id_description').value+'</b>'+' با موفقیت ایجاد گردید';
                // document.getElementById('id_brief').value = '';
                document.getElementById('id_description').value = '';
            }else
            {
            // window.location.reload();
            document.getElementById('result').innerHTML='حوزه دانش '+'<b>'+document.getElementById('id_description').value+'</b>'+' با موفقیت ایجاد نگردید';
            }
        }else{
            // document.getElementById('result').innerHTML='<strong>'+(this.readyState).toString()+'--'+(this.status).toString()+'</strong>';
        }
        };
        // alert('post');
        xmlhttp.open("POST", "add"   , true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("X-CSRFToken", ruletoken);
        var data= {
            description: document.getElementById('id_description').value,
            // brief: document.getElementById('id_brief').value,
            ownerexpert: document.getElementById('id_ownerexpert').value
        };
        xmlhttp.send('ownerexpert='+data.ownerexpert+'&desc='+data.description);
    }

function AddAndReturn() {
        var redirectto = window.sessionStorage.getItem('responsepage');
        // alert(redirectto);
        var  xmlhttp, ResponseObj;
        // alert('here1');

        xmlhttp = new XMLHttpRequest();
        // alert(ruletoken);
        xmlhttp.onreadystatechange = function() {
            // alert('here3');
        if (this.readyState === 4 && this.status === 200) {
            // alert('here4');
        ResponseObj = JSON.parse(this.responseText);
        // alert(ResponseObj)
            if (ResponseObj ==='success')
            {
                // alert('success'+this.responseText);
                window.location.href =redirectto;
            }else
            {
            // window.location.reload();
             document.getElementById('result').innerHTML='حوزه دانش '+'<b>'+document.getElementById('id_description').value+'</b>'+' با موفقیت ایجاد نگردید';
             // alert('no success'+this.responseText);
            }
        }else
            {
            //  document.getElementById('result').innerHTML='<b> '+this.responseText+'</b>';
             // alert(this.responseText);
            }
        };
         // alert('post');
        xmlhttp.open("POST", "add"   , true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("X-CSRFToken", ruletoken);
        var data= {
            description: document.getElementById('id_description').value,
            // brief: document.getElementById('id_brief').value,
            ownerexpert: document.getElementById('id_ownerexpert').value
        };
        // alert(data.description);
        xmlhttp.send('ownerexpert='+data.ownerexpert+'&desc='+data.description);
    }

function addfactforeditrule() {
    let curexpid = document.getElementById('id_ownerexpert').value;
    window.sessionStorage.setItem('responsepage',"/ServiceUser/experts/"+curexpid+"/RuleManager/"+
        rule_id+"/edit");
    window.location.href ="/ServiceUser/experts/"+curexpid+"/RuleManager/facts/add";
}

function adddomainforeditrule() {
    let curexpid = document.getElementById('id_ownerexpert').value;
    window.sessionStorage.setItem('responsepage', "/ServiceUser/experts/" + curexpid + "/RuleManager/" +
        rule_id + "/edit");
    window.location.href = "/ServiceUser/experts/" + curexpid + "/RuleManager/domains/add";
}

function  facttypedesc(value) {
    if (value === 'Y/N') {
        return 'بلی/خیر';
    }
    else if (value ==='EXP') {
        return 'تشریحی';
    }
    else {
        return 'نوع حقیقت تعریف نشده است!';
    }
}

function facteffectdesc(value){
    if (value === 'COND') {
        return 'حقیقت بیان کننده ویژگی';
    }
    else if (value === 'CONS') {
        return 'حقیقت بیان کننده نتیجه گیری';
    }
    else if (value === 'SUBJ') {
        return 'حقیقت بیان کننده موضوع'
    }
    else{
        return 'حوزه اثر حقیقت تعریف نشده است!';
    }
}