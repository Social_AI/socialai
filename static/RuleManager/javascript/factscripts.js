 if(window.attachEvent) {
    if (window.location.href.includes('facts')){
        window.attachEvent('onload', initform);
    }else {
        window.attachEvent('onload', initform);
    }
} else {
    if(window.onload) {
        var curronload = window.onload;
        var newonload = function(evt) {
            curronload(evt);
            if (window.location.href.includes('facts')){
                initform(evt);
            }else {
                initform(evt);
            }
        };
        window.onload = newonload;
    } else {
        if (window.location.href.includes('facts')){
            window.onload = initform;
        }else {
            window.onload = initform;
        }
    }
}

    function AddFactAndContinue() {
    // alert('here');
        var  xmlhttp, ResponseObj;
        var csrftoken = facttoken;
        // alert('here2');
        document.getElementById('result').style.backgroundColor='none';
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            // alert('good resp');
            ResponseObj = JSON.parse(this.responseText);
            if (ResponseObj ==='success')
            {
                document.getElementById('result').style.backgroundColor='#ebfaeb';
                document.getElementById('result').innerHTML='حقیقت '+'<b>'+document.getElementById('id_fact').value+'</b>'+' با موفقیت ایجاد گردید';
                document.getElementById('id_fact').value = '';
            }else
            {
                document.getElementById('result').style.backgroundColor='#ffe6e6';
                document.getElementById('result').innerHTML='حقیقت '+'<b>'+document.getElementById('id_fact').value+'</b>'+' با موفقیت ایجاد نگردید';
            }
        }else{
            // alert('bad resp');
            document.getElementById('result').style.backgroundColor='#f3f3d3';
            document.getElementById('result').innerHTML='<b> '+this.responseText+'</b>';
        }
        };
        // alert('post');
        xmlhttp.open("POST", "add", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
        var data= {
            fact: document.getElementById('id_fact').value,
            fact_type: document.getElementById('id_fact_type').value,
            fact_mode: document.getElementById('id_fact_effect').value,
            domain: document.getElementById('id_domain').value,
            ownerexpert: document.getElementById('id_ownerexpert').value
        };
        xmlhttp.send('fact='+data.fact+'&fact_type='+data.fact_type+'&fact_effect='+data.fact_mode+'&domain='+data.domain+'&ownerexpert='+data.ownerexpert);
    }

    function AddFactAndReturn() {
        var redirectto = window.sessionStorage.getItem('responsepage');
        // alert(redirectto);
        var  xmlhttp, ResponseObj;
         var csrftoken = facttoken;
        document.getElementById('result').style.backgroundColor='none';
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            ResponseObj = JSON.parse(this.responseText);
            if (ResponseObj ==='success')
            {
                window.location.href =redirectto;
            }else {
                document.getElementById('result').style.backgroundColor='#ffe6e6'
                document.getElementById('result').innerHTML='حقیقت '+'<b>'+document.getElementById('id_fact').value+'</b>'+' با موفقیت ایجاد نگردید';
            }
        }else{
            document.getElementById('result').style.backgroundColor= '#f3f3d3';
            document.getElementById('result').innerHTML='<b> '+this.responseText+'</b>';
        }
        };
        xmlhttp.open("POST", "add", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
        var data= {
            fact: document.getElementById('id_fact').value,
            fact_type: document.getElementById('id_fact_type').value,
            fact_mode: document.getElementById('id_fact_effect').value,
            domain: document.getElementById('id_domain').value,
            ownerexpert: document.getElementById('id_ownerexpert').value
        };
        xmlhttp.send('fact='+data.fact+'&fact_type='+data.fact_type+'&fact_effect='+data.fact_mode+'&domain='+data.domain+'&ownerexpert='+data.ownerexpert);
    }

    function initform() {
        let domval = window.sessionStorage.getItem('domain');
        domainselect = document.getElementById('id_domain');
        var opt = document.createElement('option');
        opt.value = domval.toString();
        for(i= 0; i< domainselect.getElementsByTagName('option').length;i++)
        {
            var tempopt = domainselect.getElementsByTagName('option')[i];
            if (tempopt.value === domval.toString())
            {
                opt.text = tempopt.innerText;
                break;
            }
        }
        domainselect.innerHTML = '';
        domainselect.appendChild(opt);
     }


    function adddomainforeditfact() {
        let curexpid = document.getElementById('id_ownerexpert').value;
     window.sessionStorage.setItem('responsepage', "/ServiceUser/experts/" + curexpid + "/RuleManager/facts/" +
            fact_id + "/edit");
     window.location.href = "/ServiceUser/experts/" + curexpid + "/RuleManager/domains/add";
}
