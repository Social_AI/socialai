    function managerules(uname, eid) {
        window.location.href = "/ServiceUser/experts/"+eid.toString()+"/RuleManager";
    }
    function managefacts(uname, eid) {
        window.location.href = "/ServiceUser/experts/"+eid.toString()+"/RuleManager/facts";
    }
    function expert_detail(uname, eid) {
        window.location.href = "/ServiceUser/experts/"+eid.toString();
    }
    function managedomains(uname, eid) {
        window.location.href = "/ServiceUser/experts/"+eid.toString()+"/RuleManager/domains";
    }
    function editexpert(uname, eid) {
        window.location.href = "/ServiceUser/experts/"+eid.toString()+"/edit";
    }
    function deleteexpert(uname, eid) {
        window.location.href = "/ServiceUser/experts/"+eid.toString()+"/delete";
    }

    function newexpert() {
        window.location.href = "/ServiceUser/experts/add";
    }

    function runexpert(uname, eid) {
        window.location.href = "/ServiceUser/experts/"+eid.toString()+"/start";
    }

    function engineconfig() {
        window.location.href = "/ServiceUser/experts/config";
    }
    function controlpanel( eid) {
        window.location.href = "/ServiceUser/experts/"+eid.toString()+"/controlpanel";
    }

    function restoredefaults() {
    var  xmlhttp, ResponseObj;
    var csrftoken = facttoken;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            ResponseObj = JSON.parse(this.responseText);
            if (ResponseObj !=='failure')
            {
                document.getElementById('id_no_knowledge_msg').value = ResponseObj.no_knowledge_msg;
                document.getElementById('id_no_question_msg').value = ResponseObj.no_question_msg;
                document.getElementById('id_no_more_rules_msg').value = ResponseObj.no_more_rules_msg;
                document.getElementById('id_continue_system_msg').value = ResponseObj.continue_system_msg;
                document.getElementById('id_no_knowledge_base_msg').value = ResponseObj.no_knowledge_base_msg;
                document.getElementById('id_session_finished_msg').value = ResponseObj.session_finished_msg;
                document.getElementById('result').style.backgroundColor='#ebfaeb'
                document.getElementById('result').innerHTML='<b>'+'بازیابی اطلاعات با موفقیت صورت گرفت.'+'</b>';

            }else {
                document.getElementById('result').style.backgroundColor='#ffe6e6'
                document.getElementById('result').innerHTML='<b>'+'بازیابی اطلاعات با موفقیت صورت نگرفت!'+'</b>';
            }
        }else{
            document.getElementById('result').style.backgroundColor= '#f3f3d3';
            document.getElementById('result').innerHTML='<b>در حال پردازش ...</b>';
        }
    };
    xmlhttp.open("POST", "config/restore", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlhttp.send('username='+curr_user);

}

    function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

function opencontent(mode) {
// alert('here1');

// alert(mode);
if (mode==='password')
{
    document.getElementById("SequrityOpen").click();
}else if (mode==='email')
{
    document.getElementById("EmailOpen").click();
}else
{
    document.getElementById("DefaultOpen").click();
}
// alert('here2');

}
opencontent(pmode);
    function newuserpop() {
      window.open("/ServiceUser/create_user",'popup','width=600,height=600');
        return false;
    }
