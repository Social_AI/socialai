from django.contrib import admin
from .models import Supervisor, Developer,App_User,UserProfile, Subscription,Employee, Payment
from .forms import EmployeeForm
# Register your models here.
admin.site.register(Supervisor)
admin.site.register(Developer)
admin.site.register(App_User)
admin.site.register(Subscription)
admin.site.register(Payment)
class EmployeeAdmin(admin.ModelAdmin):
    class Media:
        js = ("ServiceUser/js/admin.js",)

    # form = EmployeeForm
admin.site.register(Employee, EmployeeAdmin)
