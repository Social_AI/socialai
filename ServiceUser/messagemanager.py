import os, csv, json
from django.http import JsonResponse, HttpResponse
# from ServiceUser.forms import Form_config


def load_messages(uname):
    basepath = os.path.join(os.getcwd(),'Contract_Assistant', 'ServiceUser','UserManagement','RegisteredUsers', uname)
    custompath = os.path.join(basepath, 'customise')
    defaultpath = os.path.join(os.getcwd(),'Contract_Assistant', 'ServiceUser','baseconfig')
    try:
        with open(os.path.join(custompath, 'engine_msg.csv'), 'r', 1, 'utf-8', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            custom_msg = {}
            for row in reader:
                # print('in load:')
                # print(row)
                custom_msg = row.copy()
            if custom_msg:
                return custom_msg
            else:
                with open(os.path.join(defaultpath, 'default_msg.csv'), 'r+', 1, 'utf-8', newline='') as csvfile:
                    reader = csv.DictReader(csvfile)
                    if reader.line_num > 0:
                        for row in reader:
                            return row
                    else:
                        fieldnames = ['no_knowledge_msg', 'no_question_msg', 'no_more_rules_msg', 'continue_system_msg',
                                      'no_knowledge_base_msg', 'session_finished_msg']
                        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                        writer.writeheader()
                        print('header writ1')
                        data = {'no_knowledge_msg': 'پایگاه دانش فاقد قوانین و دانش مرتبط با موضوع انتخاب شده می باشد! لطفا موضوع را به مدیر سیستم اطلاع دهید.',
                                'no_question_msg': 'سوال مورد نظر یافت نشد!',
                                'no_more_rules_msg': """با توجه به اطلاعات وارد شده توسط کاربر، پاسخی که همه شرایط را براورده کند یافت نگردید.چنانچه از صحت اطلاعات وارده اطمینان ندارید با
                                     برگشت به مراحل قبل و انتخاب پاسخ های صحیح مجددا عملیات را تکرار نمایید.""",
                                 'continue_system_msg': '*** در صورتی که مایل به ادامه کار سیستم بر اساس اطلاعات وارده هستید گزینه بلی و در غیر اینصورت گزینه خیر را انتخاب کنید. ***',
                                'no_knowledge_base_msg': 'پایگاه دانش پیدا نشد!!',
                                'session_finished_msg': '*** اتمام جلسه کاری ***'}
                        writer.writerow(data)
                        return data
    except FileNotFoundError:
        print('فایل تنظیمات شخصی موتور استنتاج وجود ندارد!')
        with open(os.path.join(defaultpath, 'default_msg.csv'), 'r', 1, 'utf-8', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            if reader.line_num > 1:
                for row in reader:
                    no_knowledge_msg = row['no_knowledge_msg']
                    no_question_msg = row['no_question_msg']
                    no_more_rules_msg = row['no_more_rules_msg']
                    continue_system_msg = row['continue_system_msg']
                    no_knowledge_base_msg = row['no_knowledge_base_msg']
                    session_finished_msg = row['session_finished_msg']
                    default_msg = {'no_knowledge_msg': no_knowledge_msg, 'no_question_msg': no_question_msg,
                                   'no_more_rules_msg': no_more_rules_msg,
                                   'continue_system_msg': continue_system_msg,
                                   'no_knowledge_base_msg': no_knowledge_base_msg,
                                   'session_finished_msg': session_finished_msg}
                    return default_msg
            else:
                return None


def save_messages(data):
    for item in data:
        if ',' in data[item]:
            data[item] = '"'+data[item]+'"'
    appuser = data['app_user']
    del data['app_user']
    basepath = os.path.join(os.getcwd(),'Contract_Assistant', 'ServiceUser','UserManagement','RegisteredUsers')
    userpath = os.path.join(basepath, appuser)
    if not os.path.exists(userpath):
        try:
            os.mkdir(userpath)
            custompath = os.path.join(userpath, 'customise')
            if not os.path.exists(custompath):
                try:
                    os.mkdir(custompath)
                    with open(os.path.join(custompath, 'engine_msg.csv'), 'w', 1, 'utf-8', newline='') as csvfile:
                        fieldnames = ['no_knowledge_msg', 'no_question_msg', 'no_more_rules_msg', 'continue_system_msg',
                                      'no_knowledge_base_msg', 'session_finished_msg']
                        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                        writer.writeheader()
                        print('header writ1')
                        writer.writerow(data)
                        print('data writ1')
                except FileNotFoundError():
                    print('فایل تنظیمات شخصی موتور استنتاج وجود ندارد یا با موفقیت ایجاد نشد!')
        except FileNotFoundError():
            print('پوشه تنظیمات شخصی موتور استنتاج وجود ندارد یا با موفقیت ایجاد نشد!')
    elif not os.path.exists(os.path.join(userpath, 'customise')):
        try:
            os.mkdir(os.path.join(userpath, 'customise'))
            with open(os.path.join(os.path.join(userpath, 'customise'), 'engine_msg.csv'), 'w', 1, 'utf-8', newline='') as csvfile:
                fieldnames = ['no_knowledge_msg', 'no_question_msg', 'no_more_rules_msg', 'continue_system_msg',
                              'no_knowledge_base_msg', 'session_finished_msg']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                print('header writ2')
                writer.writerow(data)
                print('data writ2')
        except FileNotFoundError('فایل تنظیمات شخصی موتور استنتاج وجود ندارد یا با موفقیت ایجاد نشد!'):
            print('فایل تنظیمات شخصی موتور استنتاج وجود ندارد یا با موفقیت ایجاد نشد!')
    else:
        custompath = os.path.join(userpath, 'customise')
        with open(os.path.join(custompath, 'engine_msg.csv'), 'w', 1, 'utf-8', newline='') as csvfile:
            fieldnames = ['no_knowledge_msg', 'no_question_msg', 'no_more_rules_msg', 'continue_system_msg',
                          'no_knowledge_base_msg', 'session_finished_msg']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            print('header writ3')
            writer.writerow(data)
            print('data writ3')

def restore_default_messages(request):
    username = request.user.username
    print('curuser:'+username)
    if len(request.POST):
        print('in post')
        defaultpath = os.path.join(os.getcwd(),'Contract_Assistant', 'ServiceUser', 'baseconfig')
        try:
            print('in try')
            with open(os.path.join(defaultpath, 'default_msg.csv'), 'r', 1, 'utf-8', newline='') as csvfile:
                reader = csv.DictReader(csvfile)
                print('in with')
                if reader:
                    print('in num')
                    for row in reader:
                        # return row
                        return JsonResponse(row)
                else:
                    print('in else')
                    return JsonResponse("failure", safe=False)
                    # raise DefaultMessagesNotDefinedError
        except FileNotFoundError:
            print('فایل  پیام های پیش فرض موتور استنتاج پیدا نشد!')
    else:
        print('in get')
        return HttpResponse('not valid request')


class DefaultMessagesNotDefinedError(BaseException ):
    """Exception raised for errors when default messages not defined

    Attributes:
        message -- explanation of the error
    """

    def __init__(self,  message="پیام های پیش فرض موتور استنتاج پیدا نشد!"):
        self.message = message
        print("پیام های پیش فرض موتور استنتاج پیدا نشد!")
        super().__init__(self.message)

    def __str__(self):
        return f' {self.message}'
        # pass
