from RuleManager.models import Rules, Facts, Domain
from Contract_Assistant.settings import PLANS_CURRENCY
from .models import Supervisor, App_User, Developer, Employee, Subscription, Payment
from ExpertSystem.models import Expertmodel
from ServiceUser.UserManagement.loginuser import Form_connection
from django.template import loader
from django.http import HttpResponse,HttpResponseRedirect, JsonResponse
from django.shortcuts import render, reverse,get_object_or_404
from django.db.models import Q
from .forms import Form_Password
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required,  permission_required
from decorators import valid_subscription_required,staff_user_required, admin_user_required
from plans.quota import get_user_quota
from plans.models import Plan, UserPlan, Quota
from plans.importer import import_name
from django.conf import settings
import json




# Create your views here.
def create_user_result(request,username):
    return render(request, 'ServiceUser/user_creation_result.html', {'username':username})


def supervisorlist(request):
    rtemplate = loader.get_template('ServiceUser/supervisorlist.html')
    latest_supervisor_list = Supervisor.objects.all().order_by('pk').reverse()
    paginator = Paginator(latest_supervisor_list, 15)  # Show 15 supervisor per page.
    page_number = request.GET.get('page')
    # print(str(list(request.GET)))
    page_obj = paginator.get_page(page_number)
    output ={'supervisor_list':latest_supervisor_list,'page_obj': page_obj}
    return HttpResponse(rtemplate.render(output, request))


def supervisor_detail(request, sup_id):
    template = loader.get_template('ServiceUser/supervisor_detail.html')
    asuper = get_object_or_404(Supervisor,user_auth_id=sup_id)
    output = {'supervisor': asuper}
    return HttpResponse(template.render(output, request))

@admin_user_required
def delete_supervisor_by_index(request,sup_id):
    if request.method == 'GET':
        asup = get_object_or_404(Supervisor, user_auth=sup_id)
        developers = Developer.objects.filter(asupervisor__user_auth__username=asup.user_auth.username)
        data= {'supervisor':asup,'developers':developers}
        return render(request, 'ServiceUser/delete_supervisor.html',data)
            # return HttpResponse('valid data saved')
    else:
        asup = get_object_or_404(Supervisor, user_auth=sup_id)
        developers = Developer.objects.filter(asupervisor__user_auth__username=asup.user_auth.username)
        for dev in developers:
            dev.delete()
        Supervisor.objects.get(pk= sup_id).delete()
        return HttpResponseRedirect(reverse('ServiceUser:supervisorlist'))


def developerlist(request):
    rtemplate = loader.get_template('ServiceUser/developerlist.html')
    latest_developer_list = Developer.objects.all().order_by('pk').reverse()
    paginator = Paginator(latest_developer_list, 15)  # Show 15 developer per page.
    page_number = request.GET.get('page')
    # print(str(list(request.GET)))
    page_obj = paginator.get_page(page_number)
    output ={'developer_list':latest_developer_list,'page_obj': page_obj}
    return HttpResponse(rtemplate.render(output, request))


def developer_detail(request, dev_id):
    template = loader.get_template('ServiceUser/developer_detail.html')
    adev = get_object_or_404(Developer,user_auth_id=dev_id)
    output = {'developer': adev}
    return HttpResponse(template.render(output, request))

def delete_developer_by_index(request,dev_id):
    if request.method == 'GET':
        adev = get_object_or_404(Developer,user_auth_id=dev_id)
        data= {'developer':adev}
        return render(request, 'ServiceUser/delete_developer.html',data)
            # return HttpResponse('valid data saved')
    else:
        Developer.objects.get(pk= dev_id).delete()
        return HttpResponseRedirect(reverse('ServiceUser:developerlist'))


@login_required
@permission_required('ServiceUser.can_list_expert')
def dashboard2(request):
    username = request.user.username
    if App_User.objects.filter(user_auth__username=username).exists():
        curruser = App_User.objects.get(user_auth__username=username)
        installedapp = curruser.total_apps
        usercount = len(Employee.objects.filter(manager__user_auth__username=username))
        if curruser.user_subscription.subscription_type == Subscription._CUSTOM:
            userlimit = curruser.user_subscription.custom_user_limit
        else:
            userlimit = curruser.user_subscription.user_range
        print(installedapp)
        data = {  'installedapp':installedapp, 'maxapp':curruser.user_subscription.app_limit,
                  'usercount':usercount,'userlimit':userlimit}
    # print(user)
        return render(request, 'ServiceUser/dashboard.html', data)
    elif Employee.objects.filter(user_auth__username=username).exists() :
        curruser = Employee.objects.get(user_auth__username=username)
        allowed = curruser.alowedexperts.all()
        print(allowed.all())
        data = { 'allowed':allowed,'serviceuser':curruser.manager.user_auth.username}
    # print(user)
        return render(request, 'ServiceUser/subscriber_dashboard.html', data)

    else:
        form = Form_connection()
        request.session['redirect']='ServiceUser:dashbord'
        return render(request, 'ServiceUser/UserManagement/loginuser.html',
                      {'form' : form, 'msg':username})

        # return HttpResponseRedirect(reverse('RuleManager:loginuser'))


def dashboard(request):
    username = request.user.username
    if App_User.objects.filter(user_auth__username=username).exists():
        curr_appuser = App_User.objects.get(user_auth__username=username)
        curr_authuser = request.user
        userquata = get_user_quota(curr_authuser)
        print('uq:'+str(userquata))
        userusedquata = {}
        validators = getattr(settings, 'PLANS_VALIDATORS', {})
        for quota in validators:
            validator = import_name(validators[quota])
            quata_validator = validator()
            print(quata_validator)
            userusedquata[quata_validator.code] = quata_validator.get_queryset(curr_authuser).count()
        # userusedquata['Max_User'] = curr_appuser.total_users
        # userusedquata['Max_Assist'] = curr_appuser.total_apps
        print(userusedquata)
        # print(userusedquata[0])
        # print(userusedquata[1])
        allquata = Quota.objects.all()
        data = {'userquata':userquata, 'allquata':allquata, 'userusedquata':userusedquata}
    # print(user)
        return render(request, 'ServiceUser/dashboard2.html', data)
    elif Employee.objects.filter(user_auth__username=username).exists() :
        curruser = Employee.objects.get(user_auth__username=username)
        allowed = curruser.alowedexperts.all()
        print(allowed.all())
        data = { 'allowed':allowed,'serviceuser':curruser.manager.user_auth.username}
    # print(user)
        return render(request, 'ServiceUser/subscriber_dashboard.html', data)

    else:
        form = Form_connection()
        request.session['redirect']='ServiceUser:user_dashboard2'
        return render(request, 'ServiceUser/UserManagement/loginuser.html',
                      {'form' : form, 'msg':username})

        # return HttpResponseRedirect(reverse('RuleManager:loginuser'))

@login_required
@permission_required(['ServiceUser.can_use_restricted_expert', 'ServiceUser.can_use_public_expert'])
def subscriber_dashboard(request,  subscribername):
    username = request.user.username
    print(username+':'+subscribername)
    print(request.user.get_user_permissions())
    if Employee.objects.filter(user_auth__username=subscribername).exists() :
        curruser = Employee.objects.get(user_auth__username=subscribername)
        allowed = curruser.alowedexperts.all()
        print(allowed.all())
        data = {  'allowed':allowed,'serviceuser':username}
    # print(user)
        return render(request, 'ServiceUser/subscriber_dashboard.html', data)
    else:
        form = Form_connection()
        request.session['redirect']='ServiceUser:subscriber_dashboard'
        return render(request, 'ServiceUser/UserManagement/loginuser.html',
                      {'form' : form, 'msg':subscribername})


@login_required
def account(request):
    username = request.user.username
    if request.user.is_authenticated and not request.user.is_superuser:
        if request.user.username == username:
            curruser = App_User.objects.get(user_auth__username=username)
            useraccount = curruser.user_subscription
            allaccounttype = Subscription.objects.filter(creation_mode=Subscription._SYS_MODE)
            passform = Form_Password()
            # print('p')
            # print(passform)
            data = {'allaccounttype':allaccounttype, 'useraccount':useraccount, 'passform':passform}
            return render(request,'ServiceUser/useraccount.html',data )
        else:
            return HttpResponseRedirect(reverse('ServiceUser:user_account'))
    else:
        return HttpResponseRedirect(reverse( 'signin'))


@login_required
def accountlimit(request):
    username = request.user.username
    curr_authuser = request.user
    if request.user.is_authenticated and not request.user.is_superuser:
        if request.user.username == username:
            curruser = App_User.objects.get(user_auth__username=request.user.username)
            userquata = get_user_quota(curr_authuser)
            print('uq:'+str(userquata))
            userusedquata = {}
            validators = getattr(settings, 'PLANS_VALIDATORS', {})
            for quota in validators:
                validator = import_name(validators[quota])
                quata_validator = validator()
                userusedquata[quata_validator.code] = quata_validator.get_queryset(curr_authuser).count()
            accountdesc = UserPlan.objects.get(user= curr_authuser).plan
            # maxapp = curruser.user_subscription.app_limit
            maxapp = userquata['Max_Assist']
            # maxuser = curruser.user_subscription.user_range
            maxuser = userquata['Max_User']
            ismanager = curruser.ismanager
            expired = False
            expirtion_msg_to_user = []
            # if curruser.subscription_state != Subscription._VALID:
            #     expired = True
            #     expirtion_msg_to_user.append( 'مدت اعتبار اشتراک رایگان 30 روزه شما به پایان رسیده است. جهت ادامه استفاده از امکانات سامانه حساب خود را ارتقا دهید.')
            if curruser.total_apps >= maxapp:
                expired = True
                expirtion_msg_to_user.append( 'شما به حداکثر سامانه هوشمند قابل ایجاد خود رسیده اید! برای افزودن سامانه های جدید، اشتراک خود را ارتقا دهید. ')
            if curruser.total_users >= maxuser and maxuser > 0:
                expired = True
                expirtion_msg_to_user.append( 'شما به حداکثر کاربران زیرمجموعه خود رسیده اید! برای افزودن کاربران جدید، اشتراک خود را ارتقا دهید. ')
            # print(curruser.subscription_state)
            # print(expired)
            data = {'maxapp': maxapp, 'accountdesc': accountdesc,
                'maxuser':maxuser, 'ismanager':ismanager, 'expired':expired,
                    'expireduser':username, 'expmsg':expirtion_msg_to_user}
            return render(request,'ServiceUser/accountlimit.html',data )
        else:
            return HttpResponseRedirect(reverse('ServiceUser:accountlimit'))
    else:
        return HttpResponseRedirect(reverse('signin'))


@login_required
def paymentlist(request ):
    if request.user.is_authenticated and not request.user.is_superuser:
        userpayments = Payment.objects.filter(user=request.user)
        paginator = Paginator(userpayments, 15)  # Show 15 payment per page.
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        output = {'page_obj': page_obj, 'payments':userpayments, 'CURRENCY':PLANS_CURRENCY}
        return render(request, 'ServiceUser/payments.html', output)
    else:
        return HttpResponseRedirect(reverse( 'signin'))


@login_required
@permission_required('ServiceUser.can_list_expert')
def userexperts(request ):
    username = request.user.username
    if request.user.is_authenticated and not request.user.is_superuser:
        if request.user.username == username:
            userexpertmodels = Expertmodel.objects.filter(appuser__user_auth__username=username)
            if len(userexpertmodels):
                exp_id = userexpertmodels.first().id
                return HttpResponseRedirect(reverse('ServiceUser:user_expertdetail',args=[ exp_id]))
            # userexpertdetail(request, username, exp_id)
            else:
                data = { 'expertmodels':userexpertmodels}
                return render(request,'ServiceUser/experts.html', data)
        else:
            return HttpResponseRedirect(reverse('ServiceUser:user_expertmodel'))
    else:
        return HttpResponseRedirect(reverse( 'signin'))


@login_required
@permission_required('ServiceUser.can_list_expert')
def userstaffs(request ):
    username = request.user.username
    # print(username)
    if request.user.is_authenticated and not request.user.is_superuser:
        if request.user.username == username:
            curruser = App_User.objects.get(user_auth__username=request.user.username)
            # maxuser = curruser.user_subscription.user_range
            totaluser = curruser.total_users
            if curruser.ismanager or totaluser <= 5:     #and totaluser <= maxuser
                latest_staff_list = Employee.objects.filter(manager__user_auth__username=username)\
                    .all().order_by('pk').reverse()
                paginator = Paginator(latest_staff_list, 15)  # Show 15 staff per page.
                page_number = request.GET.get('page')
                # print(str(list(request.GET)))
                # print(request.user)
                page_obj = paginator.get_page(page_number)
                # domains = Domain.objects.filter(ownerexpert_id=exp_id)
                experts = Expertmodel.objects.filter(appuser__user_auth__username=username)
                output = {'staff_list': latest_staff_list, 'page_obj': page_obj,'experts':experts}
                return render(request, 'ServiceUser/stafflist.html', output)
            else:
                return HttpResponseRedirect(reverse('ServiceUser:accountlimit'))
        else:
            return HttpResponseRedirect(reverse('ServiceUser:user_staffs'))
    else:
        return HttpResponseRedirect(reverse( 'signin'))


@login_required
@permission_required('RuleManager.delete_rules')
def delete_employee_by_index(request, staff_id):
    if request.method == 'GET':
        astaff = get_object_or_404(Employee,user_auth_id=staff_id)
        data= {'staff': astaff}
        return render(request, 'ServiceUser/delete_staff.html',data)
            # return HttpResponse('valid data saved')
    else:
        Employee.objects.get(pk= staff_id).delete()
        return HttpResponseRedirect(reverse('ServiceUser:user_staffs'))



@login_required
@permission_required('RuleManager.delete_rules')
def staffdetail(request, staff_id):
    template = loader.get_template('ServiceUser/staff_detail.html')
    astaff = get_object_or_404(Employee, pk=staff_id)
    output = {'staff': astaff}
    return HttpResponse(template.render(output, request))



@login_required
@permission_required('ServiceUser.can_list_expert')
def userexpertdetail(request, exp_id):
    username = request.user.username
    if App_User.objects.filter(user_auth__username=username).exists() :
        # print(username)
        # print(request.user.username)
        userexpertmodels = Expertmodel.objects.filter(appuser__user_auth__username=username)
        if Expertmodel.objects.filter(Q(id=exp_id) & Q(appuser__user_auth__username=username)).exists():
            exp = get_object_or_404(Expertmodel,Q(id=exp_id) & Q(appuser__user_auth__username=username))
            output = {'expert': exp,'allexperts':userexpertmodels}
            return render(request , 'ServiceUser/expertdetail.html',output)
        else:
            return HttpResponseRedirect(reverse('ServiceUser:user_expertmodel'))
    else:
        # return HttpResponseRedirect(reverse('RuleManager:loginuser'))
        form = Form_connection()
        request.session['redirect'] = 'ServiceUser:dashbord'
        return render(request, 'ServiceUser/UserManagement/loginuser.html',
                      {'form': form, 'msg': username})

@login_required
@permission_required(['RuleManager.change_rules','RuleManager.change_facts'])
def controlpanel(request, exp_id):
    username = request.user.username
    allrule = Rules.objects.filter(Q(ownerexpert__appuser__user_auth__username=username) & Q(ownerexpert_id=exp_id))
    cid = []
    for rule in allrule:
        foundfacts = extractfact(rule.condition)
        for fact in foundfacts:
            if not (len(Facts.objects.filter(Q(fact__iexact=fact) &
                                             Q(ownerexpert__appuser__user_auth__username=username) &
                                             Q(ownerexpert_id=exp_id)))):
                cid.append(rule.id)
                break
        if not (len(Facts.objects.filter(Q(fact__iexact=rule.consequence) &
                                         Q(ownerexpert__appuser__user_auth__username=username) &
                                         Q(ownerexpert_id=exp_id)))):
            cid.append(rule.id)
    print(cid)
    corruptrules= list(Rules.objects.filter(Q(id__in=tuple(cid)) & Q(ownerexpert__appuser__user_auth__username=username) &
                                                     Q(ownerexpert_id=exp_id)))
    template = loader.get_template('ServiceUser/controlpanel.html')
    paginator = Paginator(corruptrules, 15)  # Show 10 rule per page.
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    ruledata = {'corruptrules': corruptrules,  'page_obj': page_obj, 'exp_id':exp_id}
    return HttpResponse(template.render(ruledata, request))

def extractfact(cond):
    foundfacts = []
    splited = cond.partition(' AND ')
    if (' AND ' not in splited):
        if (' OR ' not in splited):
            foundfacts.append(splited[0])
        else:
            foundfacts.append(splited[0])
            extractfact(splited[2])
    else:
        foundfacts.append(splited[0])
        extractfact(splited[2])
    return foundfacts

@login_required
@permission_required('ExpertSystem.delete_expertmodel')
def userexpertdelete(request, exp_id):
    username = request.user.username
    if App_User.objects.filter(user_auth__username=username).exists() :
        aexpert = get_object_or_404(Expertmodel, id=exp_id)
        relatedrules = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user). \
            filter(ownerexpert__id=exp_id)
        relatedfacts = Facts.objects.filter(ownerexpert__appuser__user_auth=request.user). \
            filter(ownerexpert__id=exp_id)
        relateddomains = Domain.objects.filter(ownerexpert__appuser__user_auth=request.user). \
            filter(ownerexpert__id=exp_id)

        if request.method == 'GET':
            data = {'aexpert': aexpert,'relatedrules': relatedrules, 'relatedfacts':relatedfacts,
                    'relateddomains':relateddomains, 'exp_id':exp_id}
            return render(request, 'ServiceUser/userexpert_delete.html', data)
        else:
            for fact in relatedfacts:
                Facts.objects.filter(id=fact.id).delete()
            for rul in relatedrules:
                Rules.objects.filter(id=rul.id).delete()
            for dom in relateddomains:
                Domain.objects.filter(id=dom.id).delete()
            aexpert.delete()
            print('deleted ex')
            curruser = App_User.objects.get(user_auth__username=username)
            ta = curruser.total_apps
            print('ex ta')
            print(ta)
            curruser.total_apps = ta-1
            curruser.save()
            ta = curruser.total_apps
            print('ex ta after de')
            print(ta)
            return HttpResponseRedirect(reverse('ServiceUser:user_expertmodel'))
    else:
        form = Form_connection()
        request.session['redirect'] = 'ServiceUser:dashbord'
        return render(request, 'RuleManager/templates/ServiceUser/UserManagement/loginuser.html',
                      {'form': form, 'msg': username})


@login_required
@permission_required('RuleManager.view_rules')
def userexpertrules(request, exp_id):
    username = request.user.username
    print(request.user.get_user_permissions())
    latest_rule_list = Rules.objects.filter(ownerexpert__appuser__user_auth=request.user).filter(
            ownerexpert_id=exp_id).all().order_by('pk').reverse()
    paginator = Paginator(latest_rule_list, 15)  # Show 15 rules per page.
    page_number = request.GET.get('page')
    print(str(list(request.GET)))
    print(request.user)
    page_obj = paginator.get_page(page_number)
    domains = Domain.objects.filter(ownerexpert_id=exp_id)
    expert = Expertmodel.objects.get(Q(appuser__user_auth__username=username) & Q(id=exp_id))
    output = {'Rule_list': latest_rule_list, 'page_obj': page_obj, 'domains':domains,'expert':expert}
    request.session['condition'] = ''
    return render(request, 'ServiceUser/expertrules.html', output)


@login_required
def userexpertfacts(request, exp_id):
    return HttpResponse('ok')


def permission(request):
    data= {'msg':'شما مجوز دسترسی به این صفحه را ندارید.'}
    return render(request,'ServiceUser/permissionfailed.html', data)

def update_domain_facts(request):
    if len(request.POST):
        db_id = request.POST.get('db_id', '')
        facts = Facts.objects.filter(domain_id=db_id).filter(fact_effect=Facts._CONDITION).all()
        data = json.dumps(list(facts.values('fact', 'id')),ensure_ascii=False)
        # print('data in updat:')
        # print(data)
        return HttpResponse(data, content_type="text/json-comment-filtered")

    else:
        return HttpResponse('not valid request')
