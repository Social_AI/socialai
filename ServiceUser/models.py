from django.db import models
from django.contrib.auth.models import User
from plans.models import Order
from django.utils.timezone import now
from datetime import datetime as dt
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

# Create your models here.
class UserProfile(models.Model):
    # id = models.IntegerField(primary_key=True, auto_created=True,db_index=True)
    user_auth = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, verbose_name="کاربر سیستمی")
    identifyid = models.CharField(verbose_name="شماره ملی",
                                     default=None, blank=True,max_length=10, unique=True)
    phone = models.CharField(max_length=11, verbose_name="شماره تلفن همراه",
                                    default=None, blank=True, unique=False)
    last_connection = models.DateTimeField(verbose_name="تاریخ ایجاد",
        null=True, blank=True)
    email_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.user_auth.username

class Payment(models.Model):
    ref_id = models.CharField(max_length=12,verbose_name='شماره ارجاع', blank = True,unique=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('user'), on_delete=models.CASCADE)
    order = models.ForeignKey(Order, verbose_name="سفارش",on_delete=models.DO_NOTHING,
                                       related_name='payment')
    amount = models.DecimalField(
        _('amount'), max_digits=11, decimal_places=2, db_index=True)
    completed = models.DateTimeField(
        _('completed'), null=True, blank=True, db_index=True)

    def __str__(self):
        return _("%(user)s-Payment #%(id)d") % {'id': self.id, 'user':self.user}

class Subscription(models.Model):
    _SYS_MODE = 'MANAGEMENT'
    _USER_MODE = 'USER'
    _SUBSCRIPTION_MODE = [
        (_SYS_MODE, 'اشتراک پایه سیستم'), (_USER_MODE, 'اشتراک کاربر'),]
    #################################
    _VALID = '_VALID'
    _EXPIRED = 'EXPIRED'
    _SUBSCRIPTION_STATE = [
        (_VALID, 'معتبر'), (_EXPIRED, 'خاتمه یافته'),]
    ###################################

    ###################################
    _TRIAL = 'TRIAL'
    _BASIC = 'BASIC'
    _PRO = 'PRO'
    _PREMIUM = 'PREMIUM'
    _CUSTOM = 'CUSTOM'
    _SUBSCRIPTION_TYPE = [
        (_TRIAL, 'آزمایشی رایگان'),(_BASIC, 'استاندارد'),
        (_PRO, 'حرفه ای'),(_PREMIUM, 'ویژه'),(_CUSTOM, 'شخصی سازی شده')]
    ###################################
    _APP_TRIAL = 1
    _APP_BASIC = 2
    _APP_PRO = 5
    _APP_PREMIUM = 10
    _APP_CUSTOM = 100
    _APP_NUM = [(_APP_TRIAL, 'فقط یک برنامه'), (_APP_BASIC, '2 برنامه'),
                (_APP_PRO, '5 برنامه'), (_APP_PREMIUM, '10 برنامه'),(_APP_CUSTOM, 'مطابق نیاز کاربر')]
    ####################################
    _SUP_TRIAL = 30
    _SUP_FULL = 365
    _SUP_RANGE = [(_SUP_TRIAL, 'یک ماهه'), (_SUP_FULL, 'نامحدود')]
    ####################################
    _TOTAL_USER_TRIAL = 0       # کاربر آزمایشی
    _TOTAL_USER_BASIC = 25        # تعداد کاربران از 1 نفر تا 25 نفر
    _TOTAL_USER_PRO = 100       # تعداد کاربران از 26 نفر تا 100 نفر
    _TOTAL_USER_PREMIUM = 250     # تعداد کاربران از 101 نفر تا 250 نفر
    _TOTAL_USER_HYPER = 500      # تعداد کاربران از 251 نفر تا 500 نفر
    _TOTAL_USER_MEGA = 501      # تعداد کاربران  از 501 نفر به بالا
    _TOTAL_USER_CUSTOM = 5000
    _USER_RANGE =[(_TOTAL_USER_TRIAL, 'یک نفر بصورت آزمایشی'), (_TOTAL_USER_BASIC, 'از یک نفر تا 25 نفر'),
                   (_TOTAL_USER_PRO, 'از 26 نفر تا 100 نفر'), (_TOTAL_USER_PREMIUM, 'از 101 نفر تا 250 نفر'),
                   (_TOTAL_USER_HYPER, 'از 251 نفر تا 500 نفر'), (_TOTAL_USER_MEGA, 'بیشتر از 500 نفر'),
                   (_TOTAL_USER_CUSTOM, 'مطابق نیاز کاربر')]
    ###################################
    _COST_USERS_MULTIPLY_TRIAL = 0
    _COST_USERS_MULTIPLY_BASIC = 1.1
    _COST_USERS_MULTIPLY_PRO = 1.3
    _COST_USERS_MULTIPLY_PREMIUM = 1.5
    _COST_USERS_MULTIPLY_HYPER = 1.9
    _COST_USERS_MULTIPLY_MEGA = 2.6
    ###################################
    _UNIT_FEE = 2000              # هر واحد سرمایه معادل 20000 ریال می باشد
    ###################################
    _COST_TRIAL = 0*_UNIT_FEE
    _COST_BASIC = 10*_UNIT_FEE
    _COST_PRO = 25*_UNIT_FEE        #25*2000*100*1.1 = 5500000
    _COST_PREMIUM = 50*_UNIT_FEE        # 50*2000*250*1.5 = 37500000
    _COST_HYPER = 70 * _UNIT_FEE
    _COST_MEGA = 90 * _UNIT_FEE
    _COST_CUSTOM = 15 * _UNIT_FEE
    _SUBSCRIPTION_COST = [(_COST_TRIAL, 'رایگان'), (_COST_BASIC, '10 واحد سرمایه'),
                          (_COST_PRO, '25 واحد سرمایه'), (_COST_PREMIUM, '50 واحد سرمایه'),
                          (_COST_CUSTOM , 'شخصی سازی شده(15 واحد سرمایه)')]
    ######################################
    creation_mode = models.CharField(choices=_SUBSCRIPTION_MODE, default=_SYS_MODE, max_length=25,
                                         verbose_name='نوع اشتراک')
    subscription_type = models.CharField(choices=_SUBSCRIPTION_TYPE , default=_BASIC,max_length=20,
                                         verbose_name='نوع ثبت نام')
    app_limit = models.IntegerField(choices=_APP_NUM, default=_APP_BASIC, verbose_name='تعداد سیستم های خبره')
    support_period = models.IntegerField(choices=_SUP_RANGE, default=_SUP_FULL, verbose_name='دوره پشتیبانی')
    user_range = models.IntegerField(choices=_USER_RANGE, default=_TOTAL_USER_BASIC, verbose_name='تعداد کاربران')
    subscription_rate = models.IntegerField(choices=_SUBSCRIPTION_COST, default=_COST_BASIC, verbose_name='نرخ پایه اشتراک')
    custom_app_limit = models.IntegerField(default=1, verbose_name='تعداد سامانه دلخواه')
    custom_user_limit = models.IntegerField(default=1, verbose_name='تعداد کاربر دلخواه')

    def __str__(self):
        if self.subscription_type == self._TRIAL :
            return 'آزمایشی رایگان'
        elif self.subscription_type == self._BASIC:
            return 'استاندارد'
        elif self.subscription_type == self._PRO:
            return 'حرفه ای'
        elif self.subscription_type == self._PREMIUM:
            return 'ویژه'
        else:
            if self.creation_mode == self._SYS_MODE:
                return 'شخصی سازی شده:سیستمی'+'['+str(self.custom_app_limit)+':'+str(self.custom_user_limit)+']'
            else:
                return 'شخصی سازی شده:کاربر' + '[' + str(self.custom_app_limit) + ':' + str(
                    self.custom_user_limit) + ']'

    def monthlycost(self):
        if self.user_range ==self._TOTAL_USER_TRIAL:    # if is trial user
            return int(self.subscription_rate * self.user_range * self._COST_USERS_MULTIPLY_TRIAL )      # its free   0*2000*0*0
        elif self.user_range== self._TOTAL_USER_BASIC:    #if has 1 to 25 employee
            return int(self.subscription_rate *  self.user_range/10 * self._COST_USERS_MULTIPLY_BASIC )      # 10*2000*100*1.1 = 2200000
        elif self.user_range== self._TOTAL_USER_PRO:    #if has 26 to 100 employee
            return int(self.subscription_rate *  self.user_range/13 * self._COST_USERS_MULTIPLY_PRO)
        elif self.user_range == self._TOTAL_USER_PREMIUM:
            return int(self.subscription_rate *  self.user_range/17 * self._COST_USERS_MULTIPLY_PREMIUM)
        else:
            return int(self.subscription_rate *  self.custom_app_limit/self._COST_USERS_MULTIPLY_HYPER *
                       self.custom_user_limit/self._COST_USERS_MULTIPLY_PREMIUM )


class Supervisor(UserProfile):
    # this class is for managers of application
    specialisation = models.CharField(max_length=50, verbose_name="توصیف تخصص")


class Developer(UserProfile):
    # this class is for developers of application
    senior_supervisor = models.ForeignKey(Supervisor, verbose_name="متخصص مافوق",on_delete=models.CASCADE,
                                       related_name='developer')


class App_User(UserProfile):
    # this class is for real ServiceUser(such as employee of an organisation) of application
    _VALID = '_VALID'
    _EXPIRED = 'EXPIRED'
    _SUBSCRIPTION_STATE = [
        (_VALID, 'معتبر'), (_EXPIRED, 'خاتمه یافته'), ]
    organisation = models.CharField(max_length=200, verbose_name='سازمان/شرکت', blank = False)
    user_subscription = models.ForeignKey(Subscription, default=Subscription.objects.get(subscription_type=Subscription._TRIAL).id,
                    on_delete=models.DO_NOTHING ,verbose_name='نوع اشتراک',related_name='appuser',blank = True)
    total_users = models.IntegerField(default=0, verbose_name='تعداد کاربران',blank = True)
    total_apps = models.IntegerField(default=0, verbose_name='تعداد سرویس های نصب شده',blank = True)
    # max_users = models.IntegerField(default=1, verbose_name='حداکثر تعداد کاربران')
    # max_apps = models.IntegerField(default=1, verbose_name='حداکثر تعداد سرویس های قابل اجرا')
    ismanager = models.BooleanField(default=False, verbose_name='آیا مجوز ایجاد کاربران سازمانی را دارد؟',blank = True)
    subscription_state = models.CharField(choices=_SUBSCRIPTION_STATE, default=_VALID, max_length=20, blank = True,
                                          verbose_name='وضعیت اعتبار اشتراک')
    # _PAYMENT_CASH = 1
    # _PAYMENT_CARD = 2
    # _PAYMENT_TYPE = [(_PAYMENT_CASH, 'پرداخت بصورت فیش بانکی'), (_PAYMENT_CARD, 'پرداخت با کارت بانکی')]
    # payment_type = models.IntegerField(choices=_PAYMENT_TYPE, default=2, verbose_name='انتخاب روش پرداخت')

    class Meta:
        permissions = [('can_add_condition', 'می تواند یک شرط ایجاد کند'),
                       ]
    def __str__(self):
        return self.user_auth.username

class Employee(UserProfile):
    # this class is for real ServiceUser(such as employee of an organisation) of application
    manager = models.ForeignKey(App_User, default=1, on_delete=models.DO_NOTHING ,verbose_name='سازمان/شرکت مافوق',
                                          related_name='employee_appuser',
                                )
    alowedexperts = models.ManyToManyField('ExpertSystem.Expertmodel',
                    related_name='employee_expert_field',verbose_name='سامانه های خبره مجاز',
                                           )

    class Meta:
        permissions = [('can_use_public_expert','می تواند از سیستم خبره عمومی استفاده نماید'),
                       ('can_use_restricted_expert','می تواند از سیستم خبره خاص استفاده نماید'),]
    def __str__(self):
        return self.manager.organisation+':'+ self.user_auth.username

