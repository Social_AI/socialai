from django.contrib.auth.password_validation import MinimumLengthValidator,NumericPasswordValidator\
    ,CommonPasswordValidator,UserAttributeSimilarityValidator
from django.core.exceptions import ValidationError, FieldDoesNotExist
from django.utils.translation import ngettext, gettext as _
import re
from difflib import SequenceMatcher
from plans.validators import ModelCountValidator
from ServiceUser.models import Employee
from ExpertSystem.models import Expertmodel

# https://docs.djangoproject.com/en/2.0/_modules/django/contrib/auth/password_validation/#MinimumLengthValidator


class MyMinimumLengthValidator(MinimumLengthValidator):
    # update this definition with your custom messages
    def validate(self, password, user=None):
        if password==None:
            return
        if len(password) < self.min_length:
            raise ValidationError(
                     "این رمز خیلی کوتاه است. رمز باید شامل حداقل "+str(self.min_length)+" کاراکتر باشد",
            code='password_too_short',
            params={'min_length': self.min_length},
        )

    # update this definition with your custom messages
    def get_help_text(self):
       return ngettext(
           "رمز شما باید حداقل شامل "+str(self.min_length)+" کاراکتر باشد",
           "رمز شما باید حداقل شامل "+str(self.min_length)+" کاراکتر باشد",
           self.min_length
       ) % {'min_length': self.min_length}

class MyNumericPasswordValidator(NumericPasswordValidator):
    def validate(self, password, user=None):
        if password==None:
            return

        if password.isdigit():
            raise ValidationError(
                _("این رمز بطور کامل عددی است."),
                code='password_entirely_numeric',
            )

    def get_help_text(self):
        return _("رمز شما نمی تواند بطور کامل عددی باشد.")

class MyCommonPasswordValidator(CommonPasswordValidator):
    def validate(self, password, user=None):
        if password==None:
            return

        if password.lower().strip() in self.passwords:
            raise ValidationError(
                _("این رمز خیلی عمومی است."),
                code='password_too_common',
            )

    def get_help_text(self):
        return _("رمز شما نمی تواند یک رمز پرکاربرد و عمومی باشد.")

class MyUserAttributeSimilarityValidator(UserAttributeSimilarityValidator):
    def validate(self, password, user=None):
        if not user:
            return
        if password==None:
            return

        for attribute_name in self.user_attributes:
            value = getattr(user, attribute_name, None)
            if not value or not isinstance(value, str):
                continue
            value_parts = re.split(r'\W+', value) + [value]
            for value_part in value_parts:
                if SequenceMatcher(a=password.lower(), b=value_part.lower()).quick_ratio() >= self.max_similarity:
                    try:
                        verbose_name = str(user._meta.get_field(attribute_name).verbose_name)
                    except FieldDoesNotExist:
                        verbose_name = attribute_name
                    raise ValidationError(
                        _("رمز خیلی شبیه به "+verbose_name+" است."),
                        code='password_too_similar',
                        params={'verbose_name': verbose_name},
                    )

    def get_help_text(self):
        return _("رمز شما نمی تواند خیلی شبیه به سایر اطلاعات شخصی شما باشد.")

class MaxUserValidator(ModelCountValidator):
    code = 'Max_User'
    model = Employee

    def get_queryset(self, user):
        print('in maxuser:'+str(user))
        return super(MaxUserValidator, self).get_queryset(user).filter(manager__user_auth=user)


max_user_validator = MaxUserValidator()

class MaxExpertValidator(ModelCountValidator):
    code = 'Max_Assist'
    model = Expertmodel

    def get_queryset(self, user):
        print('in maxepert:'+str(user))
        return super(MaxExpertValidator, self).get_queryset(user).filter(appuser__user_auth=user)

max_expert_validator = MaxExpertValidator()