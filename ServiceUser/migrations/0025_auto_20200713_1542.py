# Generated by Django 3.0.6 on 2020-07-13 11:12

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ServiceUser', '0024_auto_20200712_2108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='app_user',
            name='ismanager',
            field=models.BooleanField(default=False, verbose_name='آیا مجوز ایجاد کاربران سازمانی را دارد؟'),
        ),
        migrations.AlterField(
            model_name='app_user',
            name='max_apps',
            field=models.IntegerField(default=1, verbose_name='حداکثر تعداد سرویس های قابل اجرا'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='last_connection',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 13, 15, 42, 22, 286423), verbose_name='تاریخ ایجاد'),
        ),
    ]
