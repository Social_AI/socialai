# Generated by Django 3.0.6 on 2020-08-04 14:04

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ServiceUser', '0055_auto_20200804_1831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='app_user',
            name='user_subscription',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.DO_NOTHING, related_name='appuser', to='ServiceUser.Subscription', verbose_name='نوع اشتراک'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='last_connection',
            field=models.DateTimeField(default=datetime.datetime(2020, 8, 4, 18, 34, 40, 221540), verbose_name='تاریخ ایجاد'),
        ),
    ]
