from django.urls import path, include
from . import forms, views
from .UserManagement import loginuser, appuser
from RuleManager import views as Rule_views
from RuleManager import forms as Rule_forms
from .messagemanager import restore_default_messages
from plans.views import PricingView
from django.contrib.auth.decorators import login_required

app_name = 'ServiceUser'
urlpatterns = [
    # *** auth user urls ***
    path('create_user/', forms.create_user, name='create_user'),
    path('create_user/result', views.create_user_result, name='create_user-result'),
    path('edit_user/', appuser.edit_user, name='edit_user'),
    # *** auth user urls ***

    # *** supervisor urls ***
    path('supervisors/', views.supervisorlist, name='supervisorlist'),
    path('supervisors/create/', forms.create_supervisor, name='create_supervisor'),
    path('supervisors/<int:sup_id>/', views.supervisor_detail, name='supervisordetail'),
    path('supervisors/edit/<int:sup_id>/', forms.edit_supervisor_by_index, name='edit_supervisor'),
    path('supervisors/delete/<int:sup_id>/', views.delete_supervisor_by_index, name='delete_supervisor'),
    # *** supervisor urls ***

    # *** developer urls ***
    path('developers/', views.developerlist, name='developerlist'),
    path('developers/create/', forms.create_developer, name='create_developer'),
    path('developers/<int:dev_id>/', views.developer_detail, name='developerdetail'),
    path('developers/edit/<int:dev_id>/', forms.edit_developers_by_index, name='edit_developer'),
    path('developers/delete/<int:dev_id>/', views.delete_developer_by_index, name='delete_developer'),
    # *** developer urls ***

    # *** appuser urls ***
    path('plans/', include('plans.urls')),
    # path('', views.dashboard2, name='user_dashboard'),
    path('', views.dashboard, name='user_dashboard2'),
    path('noaccess/', views.permission, name='Forbidden_content'),
    path('registration/<mode>/', appuser.register, name='registration'),
    # path('registration/', appuser.register2, name='registration'),
    path('registration/activate/<uidb64>/<token>/', appuser.activate, name='activate'),
    # path('account/', views.account, name='user_account'),
    path('account/',login_required(PricingView.as_view(template_name='ServiceUser/useraccount2.html')) , name='user_account2'),
    path('account/upgrade/', forms.upgradeaccount, name='upgrade_user_account'),
    path('account/limitations/', views.accountlimit, name='accountlimit'),
    path('payments/', views.paymentlist, name='user_payments'),
    path('staffs/', views.userstaffs, name='user_staffs'),
    path('staffs/add/', forms.create_employee, name='create_userstaff'),
    path('staffs/<int:staff_id>/edit/',forms.edit_employee_by_index, name='edit_userstaff'),
    path('staffs/<int:staff_id>/delete/', views.delete_employee_by_index, name='delete_userstaff'),
    path('staffs/<int:staff_id>/detail/', views.staffdetail, name='user_staffdetail'),
    path('experts/', views.userexperts, name='user_expertmodel'),
    path('experts/config', forms.configengine, name='engineconfig'),
    path('experts/config/restore', restore_default_messages, name='restoredefaults'),
    path('experts/add/', forms.create_expertmodel, name='create_expertmodel'),
    path('experts/<int:exp_id>/', views.userexpertdetail, name='user_expertdetail'),
    path('experts/<int:exp_id>/controlpanel/', views.controlpanel, name='controlpanel'),
    path('experts/<int:exp_id>/update_facts/', views.update_domain_facts, name='update_facts'),
    path('experts/<int:exp_id>/edit/', forms.userexpertedit, name='user_expert_edit'),
    path('experts/<int:exp_id>/delete/', views.userexpertdelete, name='user_expert_delete'),
    path('experts/<int:exp_id>/start/', forms.userexpertstart, name='user_expert_start'),
    path('experts/<int:exp_id>/start/update_detailfacts/', forms.update_detailfacts, name='update_detailfacts'),
    path('experts/<int:exp_id>/startclassic/', forms.userexpertstartclassic, name='user_expert_start_classic'),
    path('experts/<int:exp_id>/startclassic/update_detailfacts/', forms.update_detailfacts, name='update_classic_detailfacts'),
    path('experts/<int:exp_id>/RuleManager/', Rule_views.index, name='user_expertrules'),
    path('experts/<int:exp_id>/RuleManager/create_condition', Rule_forms.create_condition,
         name='user_expert_createcondition'),
    path('experts/<int:exp_id>/RuleManager/controlpanel', Rule_views.controlpanel,
         name='user_expert_controlpanel'),
    path('experts/<int:exp_id>/RuleManager/add/',Rule_forms.create_rule_by_model ,
         name='user_expertnewrule'),
    path('experts/<int:exp_id>/RuleManager/<int:rule_id>/',Rule_views.detail ,
         name='user_expertrule_detail'),
    path('experts/<int:exp_id>/RuleManager/<int:rule_id>/edit/',Rule_forms.edit_rule_by_index,
         name='user_expertrule_edit'),
    path('experts/<int:exp_id>/RuleManager/<int:rule_id>/delete/',Rule_views.delete_rule_by_index,
         name='user_expertrule_delete'),
    path('experts/<int:exp_id>/RuleManager/facts',Rule_views.factlist , name='user_expertfacts'),
    path('experts/<int:exp_id>/RuleManager/facts/add',Rule_forms.create_fact_by_model ,
         name='user_expertnewfact'),
    path('experts/<int:exp_id>/RuleManager/facts/upload',Rule_forms.upload_fact_file ,
         name='user_expert_uploadfact'),
    path('experts/<int:exp_id>/RuleManager/facts/upload/result',Rule_views.upload_fact_result ,
         name='factupload_result'),
    path('experts/<int:exp_id>/RuleManager/facts/<int:fact_id>/',Rule_views.factdetail ,
         name='user_expertfact_detail'),
    path('experts/<int:exp_id>/RuleManager/facts/<int:fact_id>/edit/',Rule_forms.edit_fact_by_index ,
         name='user_expertfact_edit'),
    path('experts/<int:exp_id>/RuleManager/facts/<int:fact_id>/delete/',Rule_views.delete_fact_by_index ,
         name='user_expertfact_delete'),
    path('experts/<int:exp_id>/RuleManager/domains', Rule_views.domainlist, name='user_expertdomains'),
    path('<username>/experts/<int:exp_id>/RuleManager/domains/add', Rule_forms.create_domain_by_model,
         name='user_expertnewdomain'),
    path('experts/<int:exp_id>/RuleManager/domains/<int:dom_id>/', Rule_views.domaindetail,
         name='user_expertdomain_detail'),
    path('experts/<int:exp_id>/RuleManager/domains/<int:dom_id>/edit/', Rule_forms.edit_domain_by_index,
         name='user_expertdomain_edit'),
    path('experts/<int:exp_id>/RuleManager/domains/<int:dom_id>/delete/', Rule_views.delete_domain_by_index,
         name='user_expertdomain_delete'),
    path('experts/<int:exp_id>/RuleManager/domains/add',Rule_forms.create_domain_by_model ,
         name='user_expertnewdomain'),

    # path('<username>/experts/create', views.developer_detail, name='developerdetail'),
    # path('developers/edit/<int:dev_id>/', forms.edit_developers_by_index, name='edit_developer'),
    # path('developers/delete/<int:dev_id>/', views.delete_developer_by_index, name='delete_developer'),
    # *** appuser urls ***

]


