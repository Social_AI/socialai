from django.shortcuts import render, redirect,reverse, HttpResponseRedirect,get_object_or_404
from django import forms
from django.db.models import Q
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from  ServiceUser.models import Subscription,App_User
from  ExpertSystem.models import Expertmodel
from django.contrib.auth import authenticate, login
import datetime
from django.contrib.auth import get_user_model
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from ServiceUser.tokens import account_activation_token
from django.core.mail import send_mail
from Contract_Assistant.settings import EMAIL_HOST_USER
from plans.signals import activate_user_plan
import os, csv


def register(request, mode):
    User = get_user_model()
    if request.POST:
        print('in post')
        form = Form_appuser(request.POST)
        if form.is_valid():
            print('valid')
            # data for auth_user --a real user in system
            fname = form.cleaned_data['fname']
            lname = form.cleaned_data['lname']
            email = form.cleaned_data['email']
            ulogin = form.cleaned_data['login']
            password = form.cleaned_data['password']
            if User.objects.filter(email__iexact=email).count() == 0:
                new_user = User.objects.create_user(username = ulogin, password=password)
                # data for auth_user --a real user in system
                # in future we make user active when validate phone number
                new_user.is_active = False
                # in future we make user active when validate phone number
                new_user.is_staff = False
                new_user.first_name=fname
                new_user.last_name=lname
                new_user.email = email
                content_type = ContentType.objects.get_for_model(Expertmodel)
                # Expertmodel.
                # add_exp = Permission.objects.get(content_type=content_type, codename='can add expertmodel')
                # change_exp = Permission.objects.get(content_type=content_type, codename='can change expertmodel')
                # listexp_perm = Permission.objects.get(content_type=content_type, codename='can viewexpert')
                # del_exp = Permission.objects.get(content_type=content_type, codename='can delete expertmodel')
                # new_user.user_permissions.set([add_exp, change_exp,listexp_perm,del_exp])
                srvusr = Group.objects.get(name='مشترک سرویس')
                new_user.groups.set([srvusr])
                new_user.save()
                # data for appuser --a user in ServiceUser
                identifyid = form.cleaned_data['identifyid']
                phone = form.cleaned_data['phone']
                create_date = datetime.datetime.now()
                organisation = form.cleaned_data['organisation']
                if mode == 'free':
                    # max_users = 1
                    # max_app = 1
                    total_apps = 0
                    total_users =1
                    manager = False
                    usersub = Subscription.objects.get(subscription_type=Subscription._TRIAL)
                elif mode == 'standard':
                    # max_users = Subscription.objects.filter(subscription_type=Subscription._BASIC)[0].user_range
                    # max_app = Subscription.objects.filter(subscription_type=Subscription._BASIC)[0].app_limit
                    total_apps = 0
                    total_users = 1
                    usersub = Subscription.objects.get(subscription_type=Subscription._BASIC)
                    manager = True
                elif mode == 'pro':
                    # max_users = Subscription.objects.filter(subscription_type=Subscription._PRO)[0].user_range
                    # max_app = Subscription.objects.filter(subscription_type=Subscription._PRO)[0].app_limit
                    total_apps = 0
                    total_users = 1
                    usersub = Subscription.objects.get(subscription_type=Subscription._PRO)
                    manager = True
                elif mode =='special':
                    # max_users = Subscription.objects.filter(subscription_type=Subscription._PREMIUM)[0].user_range
                    # max_app = Subscription.objects.filter(subscription_type=Subscription._PREMIUM)[0].app_limit
                    total_apps = 0
                    total_users = 1
                    usersub = Subscription.objects.get(subscription_type=Subscription._PREMIUM)
                    manager = True
                else:
                    max_users = form.cleaned_data['max_users']
                    max_app = form.cleaned_data['max_apps']
                    total_apps = 0
                    total_users = 1
                    usersub = Subscription.objects.create(creation_mode=Subscription._USER_MODE,
                                                   subscription_type=Subscription._CUSTOM,
                                                   app_limit=Subscription._APP_CUSTOM,
                                                   support_period=Subscription._SUP_FULL,
                                                   user_range=Subscription._TOTAL_USER_BASIC,
                                                   subscription_rate=Subscription._COST_CUSTOM,
                                                   custom_app_limit=max_app, custom_user_limit=max_users)
                    manager = True
                new_appuser = App_User.objects.create(user_auth=new_user,identifyid=identifyid,phone=phone,
                                last_connection=create_date,organisation=organisation,
                                user_subscription=usersub, total_users=total_users, total_apps=total_apps,
                                subscription_state= Subscription._VALID,ismanager=manager)
                new_appuser.save()
                create_userconfig(ulogin)
                if new_user.is_active == True:
                    authuser =authenticate( username=ulogin, password=password)
                    if authuser is not None:
                        login(request, authuser)
                        if request.session['redirect'] != '' and  request.session['redirect'] !=None:
                            return redirect(request.session['redirect'])
                        else:
                            return HttpResponseRedirect(reverse('ServiceUser:user_dashboard'))
                    else:
                        return HttpResponseRedirect(reverse('signin'))
                else:
                    current_site = get_current_site(request)
                    mail_subject = 'Activate your account.'
                    message = render_to_string('registration/email_activation_template.html', {
                            'user': new_appuser,
                            'domain': current_site.domain,
                            'uid': urlsafe_base64_encode(force_bytes(new_appuser.pk)),
                            'token': account_activation_token.make_token(new_appuser),
                        })
                    to_email = form.cleaned_data.get('email')
                    send_mail(mail_subject, message, EMAIL_HOST_USER, [to_email])
                    # return HttpResponseRedirect(reverse('ServiceUser:create_user-result', args=(username,status)))
                    return render(request, 'ServiceUser/user_creation_result.html',{'username':ulogin, 'status':'disabled'})
            else:
                form.add_error('email','این ایمیل تکراری می باشد!')
                return render(request, 'ServiceUser/UserManagement/create_appuser.html', {'form' : form, 'registmode':mode,})
        else:
            print('not valid')
            print(str(form.errors.as_data()))

            return render(request, 'ServiceUser/UserManagement/create_appuser.html', {'form' : form, 'registmode':mode,})
    else:
        print('get')
        form = Form_appuser()
        # return render(request, 'RuleManager/UserManagement/create_appuser.html', {'form' : form})
        return render(request,
                      'ServiceUser/UserManagement/create_appuser.html', {'form' : form, 'registmode':mode})


def register2(request):
    User = get_user_model()
    if request.POST:
        print('in post')
        form = Form_appuser(request.POST)
        if form.is_valid():
            print('valid')
            # data for auth_user --a real user in system
            fname = form.cleaned_data['fname']
            lname = form.cleaned_data['lname']
            email = form.cleaned_data['email']
            ulogin = form.cleaned_data['login']
            password = form.cleaned_data['password']
            if User.objects.filter(email__iexact=email).count() == 0:
                new_user = User.objects.create_user(username = ulogin, password=password)
                # data for auth_user --a real user in system
                # in future we make user active when validate phone number
                new_user.is_active = False
                # in future we make user active when validate phone number
                new_user.is_staff = False
                new_user.first_name=fname
                new_user.last_name=lname
                new_user.email = email
                content_type = ContentType.objects.get_for_model(Expertmodel)
                # Expertmodel.
                # add_exp = Permission.objects.get(content_type=content_type, codename='can add expertmodel')
                # change_exp = Permission.objects.get(content_type=content_type, codename='can change expertmodel')
                # listexp_perm = Permission.objects.get(content_type=content_type, codename='can viewexpert')
                # del_exp = Permission.objects.get(content_type=content_type, codename='can delete expertmodel')
                # new_user.user_permissions.set([add_exp, change_exp,listexp_perm,del_exp])
                srvusr = Group.objects.get(name='مشترک سرویس')
                new_user.groups.set([srvusr])
                new_user.save()
                # data for appuser --a user in ServiceUser
                identifyid = form.cleaned_data['identifyid']
                phone = form.cleaned_data['phone']
                create_date = datetime.datetime.now()
                organisation = form.cleaned_data['organisation']
                new_appuser = App_User.objects.create(user_auth=new_user,identifyid=identifyid,phone=phone,
                                last_connection=create_date,organisation=organisation)
                new_appuser.save()
                create_userconfig(ulogin)
                if new_user.is_active == True:
                    authuser =authenticate( username=ulogin, password=password)
                    if authuser is not None:
                        login(request, authuser)
                        if request.session['redirect'] != '' and  request.session['redirect'] !=None:
                            return redirect(request.session['redirect'])
                        else:
                            return HttpResponseRedirect(reverse('ServiceUser:user_account2'))
                    else:
                        return HttpResponseRedirect(reverse('signin'))
                else:
                    user_mail = form.cleaned_data.get('email')
                    print(user_mail)
                    send_confirm_mail(request, new_appuser, user_mail)
                    return render(request, 'ServiceUser/user_creation_result.html',{'username':ulogin, 'status':'disabled'})
            else:
                form.add_error('email','این ایمیل تکراری می باشد!')
                return render(request, 'ServiceUser/UserManagement/create_appuser2.html', {'form' : form})
        else:
            print('not valid')
            print(str(form.errors.as_data()))

            return render(request, 'ServiceUser/UserManagement/create_appuser2.html', {'form' : form})
    else:
        print('get')
        form = Form_appuser()
        # return render(request, 'RuleManager/UserManagement/create_appuser.html', {'form' : form})
        return render(request,
                      'ServiceUser/UserManagement/create_appuser2.html', {'form' : form})

def edit_user(request):
    srvuser =  get_object_or_404(App_User, user_auth__username=request.user.username)
    if request.POST:
        print('in post')
        form = Appuser_Form(request.POST,instance=srvuser)
        if form.is_valid():
            print('valid')
            form.save();
            return HttpResponseRedirect(reverse('ServiceUser:user_dashboard2'))

        else:
            print('not valid')
            return render(request,
                      'ServiceUser/UserManagement/edit_appuser.html', {'form' : form, 'srvuser':srvuser})
    else:
        print('get')
        print(srvuser)
        form = Appuser_Form(instance=srvuser)
        return render(request,
                      'ServiceUser/UserManagement/edit_appuser.html', {'form' : form, 'srvuser':srvuser})


def activate(request, uidb64, token):

    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        appuser = App_User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        appuser = None
    if appuser is not None and account_activation_token.check_token(appuser, token):
        appuser.email_confirmed = True
        appuser.save()
        auth_user = appuser.user_auth
        auth_user.is_active = True
        auth_user.save()
        activate_user_plan.send(sender= auth_user, user= auth_user)
        return render(request,'ServiceUser/user_creation_result.html',{'username':appuser.user_auth.username,'status':'activated'})
    else:
        return render(request,'ServiceUser/user_creation_result.html',{'username':'','status':'error'})


def pricing(request):
    freecost = Subscription.objects.get(subscription_type=Subscription._TRIAL).monthlycost()
    basiccost = Subscription.objects.get(subscription_type=Subscription._BASIC).monthlycost()
    procost = Subscription.objects.get(subscription_type=Subscription._PRO).monthlycost()
    premiumcost = Subscription.objects.get(subscription_type=Subscription._PREMIUM).monthlycost()
    customacc = Subscription.objects.get(Q(subscription_type='CUSTOM') & Q(creation_mode=Subscription._SYS_MODE))
    print(customacc)
    customcust = customacc.monthlycost()
    data = {'free':freecost, 'standard':basiccost, 'pro':procost,'premium':premiumcost,'custom':customcust,
            'curracc':customacc}
    return render(request, 'ServiceUser/pricing.html',data)


class Form_appuser(forms.Form):
    fname = forms.CharField(label="نام", max_length=30,required=False)
    lname = forms.CharField(label="نام خانوادگی", max_length=150,required=False)
    login = forms.CharField(label="نام کاربری", max_length=150,required=True)
    email = forms.EmailField(label="رایانامه",max_length=150,required=True)
    password = forms.CharField(label="گذرواژه", widget=forms.PasswordInput,required=True)
    password_bis = forms.CharField(label="تکرارگذرواژه", widget=forms.PasswordInput,required=True)
    identifyid = forms.CharField(label='شماره ملی را وارد کنید',max_length=10,min_length=10,required=False)
    phone = forms.CharField(label='شماره تلفن همراه خود را وارد کنید',max_length=11,min_length=11,required=False)
    organisation = forms.CharField(label='نام سازمان/شرکت',max_length=200,empty_value='نام سازمان یا شرکت ',required=False)
    # total_users = forms.IntegerField(label='تعداد کاربران زیر مجموعه',min_value=1)


    def clean(self):
        cleaned_data = super (Form_appuser, self).clean()
        password = cleaned_data.get('password')
        password_bis = cleaned_data.get('password_bis')
        ulogin = cleaned_data.get('login')
        identifyid = cleaned_data.get('identifyid')
        phone = cleaned_data.get('phone')
        if phone and not phone.isnumeric():
            # raise forms.ValidationError("فقط اعداد مجاز می باشند.")
            self.add_error('phone',"فقط اعداد مجاز می باشند.")
        if identifyid and not identifyid.isnumeric():
            self.add_error('identifyid', "فقط اعداد مجاز می باشند.")
        if password and password_bis and password != password_bis:
            # raise forms.ValidationError("گذرواژه ها یکسان نمی باشند.")
            self.add_error(None, "گذرواژه ها یکسان نمی باشند.")
        if  User.objects.filter(username__iexact=ulogin).exists():
            # raise forms.ValidationError("این نام کاربری از قبل ثبت شده است.")
            self.add_error('login', "این نام کاربری از قبل ثبت شده است.")
        return cleaned_data


class Appuser_Form(forms.ModelForm):
    class Meta:
        model = App_User
        fields = '__all__'


    def clean(self):
        cleaned_data = super (Appuser_Form, self).clean()
        login = cleaned_data.get('login')
        identifyid = cleaned_data.get('identifyid')
        phone = cleaned_data.get('phone')
        if phone and not phone.isnumeric():
             self.add_error('phone',"فقط اعداد مجاز می باشند.")
        if identifyid and not identifyid.isnumeric():
            self.add_error('identifyid', "فقط اعداد مجاز می باشند.")
        if  User.objects.filter(username__iexact=login).exists():
            self.add_error('login', "این نام کاربری از قبل ثبت شده است.")
        return cleaned_data

class Form_User(forms.ModelForm):
    class Meta:
        model = App_User
        # model = User
        exclude = ['max_users','max_apps']
        # widgets={'password':forms.PasswordInput}
    # def clean(self):
    #     cleaned_data = super(Form_supervisor, self).clean()
    #     password = self.cleaned_data.get('password')
    #     password_bis = self.cleaned_data.get('password_bis')
    #     if password and password_bis and password != password_bis:
    #         raise forms.ValidationError("Passwords are not identical.")
    #     return self.cleaned_data

def create_userconfig(uname):
    basepath = os.path.join(os.getcwd(),'Contract_Assistant', 'ServiceUser','UserManagement','RegisteredUsers')
    userpath = os.path.join(basepath, uname)
    if not os.path.exists(userpath):
        os.mkdir(userpath)
        custompath = os.path.join(userpath, 'customise')
        if not os.path.exists(custompath):
            os.mkdir(custompath)
            with open(os.path.join(custompath, 'engine_msg.csv'), 'w', 1, 'utf-8') as csvfile:
                fieldnames = [
                    'no_knowledge_msg', 'no_question_msg', 'no_more_rules_msg', 'continue_system_msg',
                      'no_knowledge_base_msg', 'session_finished_msg']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()

def send_confirm_mail(request, new_appuser, user_mail):
    current_site = get_current_site(request)
    print(current_site)
    mail_subject = 'فعال سازی حساب'
    message = render_to_string('registration/email_activation_template.html', {
            'user': new_appuser,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(new_appuser.pk)),
            'token': account_activation_token.make_token(new_appuser),
            })
    to_email = user_mail
    send_mail(mail_subject, message, EMAIL_HOST_USER, [to_email])
    # return render(request, 'ServiceUser/user_creation_result.html',{'username':username, 'status':'disabled'})

