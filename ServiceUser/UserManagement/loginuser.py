from django.shortcuts import render, redirect,reverse, HttpResponseRedirect
from django import forms
from django.contrib.auth import authenticate, login, logout
from .appuser import create_userconfig
from ServiceUser.models import App_User, Subscription
from datetime import datetime as dt , timedelta, timezone
from django.http import Http404


def verifyuser2(request):
    if request.POST:
        # This line is used to check if the Form_connection form has been
        # posted. If mailed, the form will be treated, otherwise it will be
        # displayed to the user.
        form = Form_connection(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            # This line verifies that the username exists and the password
            # is correct.
            if user:
                # In this line, the authenticate function returns None if
                # authentication has failed, otherwise it returns an object that
                # validates the condition.
                login(request, user)
                if not user.is_superuser and not user.is_staff:     #user isnt admin or staff
                    try:
                        # ************* modify here ***************
                        userobj = App_User.objects.get(user_auth=user)
                        # ************* modify here ***************
                        email_state = userobj.email_confirmed
                        if not email_state:
                            logout(request)
                            form.add_error(None,'اعتبار رایانامه شما تایید نشده است! لینک تایید اعتبار  در هنگام ثبت نام اولیه به رایانامه شما ارسال شده است. ')
                            return render(request, 'ServiceUser/UserManagement/loginuser.html',
                                          {'form': form, 'expired': False})
                        usr_regiser_date = userobj.last_connection or dt.now(timezone.utc)
                        if (dt.now(timezone.utc) - usr_regiser_date) < timedelta(days=Subscription._SUP_TRIAL) \
                                or userobj.user_subscription.subscription_type != Subscription._TRIAL: #subscription not expired
                            userobj.subscription_state = Subscription._VALID
                            userobj.save(force_update=True)
                            if 'redirect' in request.session and request.session['redirect'] != '' and request.session['redirect'] != None:
                                print(request.session['redirect'])
                                create_userconfig(username)
                                return redirect(request.session['redirect'])
                            else:
                                print('empty redirect' )
                                if user.is_superuser:
                                    print('go to admin site')
                                    return HttpResponseRedirect(reverse('admin:index'))
                                # return HttpResponseRedirect(reverse('ExpertSystem:expert_initial'))
                                elif (username == 'demouser' or username == 'amozesh'):
                                    print('go to expert initial page')
                                    create_userconfig(username)
                                    return HttpResponseRedirect(reverse('ExpertSystem:expert_initial'))
                                else:
                                    print('go to user dashboard')
                                    create_userconfig(username)
                                    return HttpResponseRedirect(reverse('ServiceUser:user_dashboard'))
                        else:
                            # (subscription)trial period has expired
                            userobj.subscription_state = Subscription._EXPIRED
                            userobj.save(force_update=True)
                            print(userobj)
                            print(usr_regiser_date)
                            print('subs expired')
                            if user.is_authenticated:
                                print(user.is_authenticated)
                            else:
                                print('not auth')
                            # trial period has expired
                            if 'redirect' in request.session and request.session['redirect'] != '' and request.session['redirect'] != None:
                                print(request.session['redirect'])
                                create_userconfig(username)
                                return redirect(request.session['redirect'])
                            else:
                                print('empty redirect' )
                                #     # if user.is_superuser:
                                #     #     print('go to admin site')
                                #     #     return HttpResponseRedirect(reverse('admin:index'))
                                # # return HttpResponseRedirect(reverse('ExpertSystem:expert_initial'))
                                if (username == 'demouser' or username == 'amozesh'):
                                    print('go to expert initial page')
                                    create_userconfig(username)
                                    return HttpResponseRedirect(reverse('ExpertSystem:expert_initial'))
                                else:
                                    print('go to user dashboard')
                                    create_userconfig(username)
                                    return HttpResponseRedirect(reverse('ServiceUser:user_dashboard'))  # not admin
                    except App_User.DoesNotExist:
                        print(
                            "هیچ کاربر سرویس با مشخصات وارد شده یافت نگردید:" + "\n" + 'username:' + username)
                        raise Http404(
                            "هیچ کاربر سرویس با مشخصات وارد شده یافت نگردید:" + "\n" + 'username:' + username)
                else:
                    if 'redirect' in request.session and request.session['redirect'] != '' and request.session['redirect'] != None:
                        print(request.session['redirect'])
                        # create_userconfig(username)
                        return redirect(request.session['redirect'])
                    else:
                        print('empty redirect:' )
                        return HttpResponseRedirect(reverse('admin:index'))
            else:
                print('not user:'+str(form.errors))
                return render(request, 'ServiceUser/UserManagement/loginuser.html', {'form' :form, 'expired': False})
        else:
            print('not valid form:'+str(form.errors))
            return render(request, 'ServiceUser/UserManagement/loginuser.html', {'form': form , 'expireduser':'dummyuser',
                                                                                 'expired': False})
    else:
        form = Form_connection()
        request.session['redirect']=request.GET.get('next')
        return render(request, 'ServiceUser/UserManagement/loginuser.html', {'form' : form , 'expireduser':'dummyuser',
                                                                             'expired': False})


def verifyuser(request):
    if request.POST:
        # This line is used to check if the Form_connection form has been
        # posted. If mailed, the form will be treated, otherwise it will be
        # displayed to the user.
        form = Form_connection(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            # This line verifies that the username exists and the password
            # is correct.
            if user:
                # In this line, the authenticate function returns None if
                # authentication has failed, otherwise it returns an object that
                # validates the condition.
                login(request, user)
                if not user.is_superuser and not user.is_staff:     #user isnt admin or staff
                    try:
                        # ************* modify here ***************
                        userobj = App_User.objects.get(user_auth=user)
                        # ************* modify here ***************
                        email_state = userobj.email_confirmed
                        if not email_state:
                            logout(request)
                            form.add_error(None,'اعتبار رایانامه شما تایید نشده است! لینک تایید اعتبار  در هنگام ثبت نام اولیه به رایانامه شما ارسال شده است. ')
                            return render(request, 'ServiceUser/UserManagement/loginuser.html',
                                          {'form': form, 'expired': False})
                        else:
                            print(userobj)
                            if user.is_authenticated:
                                print(user.is_authenticated)
                            else:
                                print('not auth')
                            # trial period has expired
                            if 'redirect' in request.session and request.session['redirect'] != '' and request.session['redirect'] != None:
                                print(request.session['redirect'])
                                create_userconfig(username)
                                return redirect(request.session['redirect'])
                            else:
                                print('empty redirect' )
                                #     # if user.is_superuser:
                                #     #     print('go to admin site')
                                #     #     return HttpResponseRedirect(reverse('admin:index'))
                                # # return HttpResponseRedirect(reverse('ExpertSystem:expert_initial'))
                                if (username == 'demouser' or username == 'amozesh'):
                                    print('go to expert initial page')
                                    create_userconfig(username)
                                    return HttpResponseRedirect(reverse('ExpertSystem:expert_initial'))
                                else:
                                    print('go to user dashboard')
                                    create_userconfig(username)
                                    return HttpResponseRedirect(reverse('ServiceUser:user_dashboard2'))  # not admin
                    except App_User.DoesNotExist:
                        print(
                            "هیچ کاربر سرویس با مشخصات وارد شده یافت نگردید:" + "\n" + 'username:' + username)
                        raise Http404(
                            "هیچ کاربر سرویس با مشخصات وارد شده یافت نگردید:" + "\n" + 'username:' + username)
                else:
                    if 'redirect' in request.session and request.session['redirect'] != '' and request.session['redirect'] != None:
                        print(request.session['redirect'])
                        # create_userconfig(username)
                        return redirect(request.session['redirect'])
                    else:
                        print('empty redirect:' )
                        return HttpResponseRedirect(reverse('admin:index'))
            else:
                print('not user:'+str(form.errors))
                return render(request, 'ServiceUser/UserManagement/loginuser.html', {'form' :form, 'expired': False})
        else:
            print('not valid form:'+str(form.errors))
            return render(request, 'ServiceUser/UserManagement/loginuser.html', {'form': form , 'expireduser':'dummyuser',
                                                                                 'expired': False})
    else:
        form = Form_connection()
        request.session['redirect']=request.GET.get('next')
        return render(request, 'ServiceUser/UserManagement/loginuser.html', {'form' : form , 'expireduser':'dummyuser',
                                                                             'expired': False})

class Form_connection(forms.Form):
    username = forms.CharField(label="Login")
    password = forms.CharField(label="Password", widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(Form_connection, self).clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        if not authenticate(username=username, password=password):
            self.add_error(None, "نام کاربری یا گذرواژه اشتباه است")
        return cleaned_data
