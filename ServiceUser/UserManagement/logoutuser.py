from django.shortcuts import render
from django.contrib.auth import logout

def finishconnection(request):
    logout(request)
    return render(request, 'ServiceUser/UserManagement/logout.html')