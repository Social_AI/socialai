from ServiceUser.models import Supervisor, Developer, App_User, Employee, Subscription
from ExpertSystem.models import Expertmodel
from ExpertSystem.inferenceengineclass import inferenceengine
from RuleManager.models import Facts, Rules, Domain
from django import forms
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, redirect,reverse,get_object_or_404
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.password_validation import validate_password
from .messagemanager import save_messages, load_messages
from decorators import valid_subscription_required,staff_user_required, admin_user_required
from .custom_validators import max_user_validator, max_expert_validator
from django.conf import settings
from plans.importer import import_name
from plans.quota import get_user_quota
import jsonpickle, json, datetime
rulefacts = []



# Create your views here.

class Form_supervisor(forms.ModelForm):
    class Meta:
        model = Supervisor
        fields=('user_auth','identifyid','phone','specialisation','last_connection')


class Form_developer(forms.ModelForm):

    class Meta:

        model = Developer
        fields=('user_auth','senior_supervisor','identifyid','phone','last_connection')
        # exclude = ['specialisation',]


class Form_User(forms.ModelForm):
    class Meta:
        # model = App_User
        model = User
        fields=('username','password','last_name','first_name','email')
        widgets={'password':forms.PasswordInput}
    def clean(self):
        cleaned_data = super(Form_User, self).clean()
        password = self.cleaned_data.get('password')
        password_bis = self.cleaned_data.get('password_bis')
        if password and password_bis and password != password_bis:
            self.add_error(None, "گذرواژه ها یکسان نمی باشند." )
        return cleaned_data


class Form_expert(forms.ModelForm):
    class Meta:
       model = Expertmodel
       fields = ('appuser', 'name','accesstype',)

    def clean(self):
        cleaned_data = super (Form_expert, self).clean()
        max_expert_validator(cleaned_data['appuser'].user_auth, add=1)
        return cleaned_data


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'

    # def __init__(self, *args, **kwargs):
    #     print('in init')
    #     super(EmployeeForm, self).__init__(*args, **kwargs)
    #     print('in init af su')
    #     if self.instance.pk:
    #         # print(self.instance)
    #         print('in init 2')
    #         if self.instance.manager:
    #             print('in init 3')
    #             alowedexperts = Expertmodel.objects.filter(appuser__user_auth__username=
    #                                                         self.instance.manager.user_auth.username )
    #             print(alowedexperts)
    #             alowedexperts_field = self.instance.fields['alowedexperts'].widget

    #             alowedexperts_choices = []
    #             if alowedexperts is None:
    #                 alowedexperts_choices.append(('', '---------'))

    #             for expert in alowedexperts:
    #                 alowedexperts_choices.append((expert.id, expert.name))
    #             alowedexperts_field.choices = alowedexperts_choices
    #             self.instance.fields['manger'].widget = forms.ModelChoiceField(queryset=App_User.objects.all(),
    #                                                                           attrs = {'onchange' : "setexperts(this.value);"})
    #     elif len(self.fields['manager'].choices):
    #         manlist = list(self.fields['manager'].choices)
    #         managerid, managername = manlist[0]
    #         alowedexperts = Expertmodel.objects.filter(appuser__user_auth__username=managername )
    #         print(alowedexperts)
    #         alowedexperts_field = self.fields['alowedexperts'].widget
    #         alowedexperts_choices = []
    #         if alowedexperts is None:
    #             alowedexperts_choices.append(('', '---------'))

    #         for expert in alowedexperts:
    #             alowedexperts_choices.append((expert.id, expert.name))
    #         alowedexperts_field.choices = alowedexperts_choices

    def clean(self):
        cleaned_data = super (EmployeeForm, self).clean()
        max_user_validator(cleaned_data['manager'].user_auth, add=1)
        password = cleaned_data.get('password')
        password_bis = cleaned_data.get('password_bis')
        login = cleaned_data.get('login')
        identifyid = cleaned_data.get('identifyid')
        phone = cleaned_data.get('phone')
        if phone and not phone.isnumeric():
             self.add_error('phone',"فقط اعداد مجاز می باشند.")
        if identifyid and not identifyid.isnumeric():
            self.add_error('identifyid', "فقط اعداد مجاز می باشند.")
        if password and password_bis and password != password_bis:
            self.add_error(None, "گذرواژه ها یکسان نمی باشند.")
        if  User.objects.filter(username__iexact=login).exists():
            self.add_error('login', "این نام کاربری از قبل ثبت شده است.")
        return cleaned_data


class Form_Employee(forms.Form):
    fname = forms.CharField(label="نام", max_length=30,required=False)
    lname = forms.CharField(label="نام خانوادگی", max_length=150,required=False)
    login = forms.CharField(label="نام کاربری", max_length=150,required=True)
    email = forms.EmailField(label="رایانامه",max_length=150,required=True)
    password = forms.CharField(label="گذرواژه", widget=forms.PasswordInput,required=True)
    password_bis = forms.CharField(label="تکرارگذرواژه", widget=forms.PasswordInput,required=True)
    identifyid = forms.CharField(label='شماره ملی را وارد کنید',max_length=10,min_length=10,empty_value='1234567890',required=False)
    phone = forms.CharField(label='شماره تلفن همراه خود را وارد کنید',empty_value='09000000000',max_length=11,min_length=11,required=False)
    manager = forms.ModelChoiceField(App_User.objects.all(), required=True,label='نام مدیر ارشد',help_text='نام مدیر ارشد')
    alowedexperts = forms.ModelMultipleChoiceField(Expertmodel.objects.all(),required=True,
                                                   help_text='سامانه های مجاز به دسترسی توسط کاربر',
                                                   label='سامانه های مجاز به دسترسی توسط کاربر')

    def clean(self):
        cleaned_data = super (Form_Employee, self).clean()
        print(cleaned_data)
        password = cleaned_data.get('password')
        password_bis = cleaned_data.get('password_bis')
        login = cleaned_data.get('login')
        identifyid = cleaned_data.get('identifyid')
        phone = cleaned_data.get('phone')
        if phone and not phone.isnumeric():
             self.add_error('phone',"فقط اعداد مجاز می باشند.")
        if identifyid and not identifyid.isnumeric():
            self.add_error('identifyid', "فقط اعداد مجاز می باشند.")
        if password and password_bis and password != password_bis:
            self.add_error(None, "گذرواژه ها یکسان نمی باشند.")
        if  User.objects.filter(username__iexact=login).exists():
            self.add_error('login', "این نام کاربری از قبل ثبت شده است.")
        if not cleaned_data.get('manager'):
            self.add_error('manager',"انتخاب سرپرست الزامی می باشد.")
        else:
            max_user_validator(cleaned_data.get('manager').user_auth, add=1)
        return cleaned_data


class Form_Password(forms.Form):
    oldpass = forms.CharField(label="گذرواژه قدیمی", widget=forms.PasswordInput,
                              error_messages={'required': 'ورود رمز قدیمی الزامی می باشد.'})
    newpass = forms.CharField(label="گذرواژه جدید", widget=forms.PasswordInput,
                              error_messages={'required': 'ورود رمز جدید الزامی می باشد.'})
    newpass_bis = forms.CharField(label="تکرارگذرواژه", widget=forms.PasswordInput,
                              error_messages={'required': 'تکرار رمز جدید الزامی می باشد.'})

    def clean(self):
        cleaned_data = super(Form_Password, self).clean()
        newpass = cleaned_data.get('newpass')
        newpass_bis = cleaned_data.get('newpass_bis')
        if newpass and newpass_bis and newpass != newpass_bis:
            self.add_error(None, "گذرواژه ها یکسان نمی باشند.")
        validate_password(newpass)
        return cleaned_data

class Form_Email(forms.Form):
    newemail = forms.EmailField(label='ایمیل جدید', help_text='example@domain.com',
                                error_messages={'invalid': 'لطفا یک ایمیل معتبر وارد کنید.',
                                                'required':'ورود یک ایمیل معتبر الزامی می باشد.'})

class Form_config(forms.Form):

    no_knowledge_msg = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '2'}),
                                       label='پیام سیستم در صورت عدم وجود حقایق و قوانین مرتبط با موضوع انتخاب شده',
                                       initial='پایگاه دانش فاقد قوانین و دانش مرتبط با موضوع انتخاب شده می باشد! لطفا موضوع را به مدیر سیستم اطلاع دهید.')
    no_question_msg = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '2'}),
                                       label='پیام سیستم در هنگام یافت نشدن پرسش ایجاد شده توسط سیستم',
                                      initial='سوال مورد نظر یافت نشد!')
    no_more_rules_msg = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '5'}),
                                       label="پیام سیستم در مواردی که پاسخی برای وروردی های کاربر وجود ندارد",
                                        initial="""با توجه به اطلاعات وارد شده توسط کاربر، پاسخی که همه شرایط را براورده کند یافت نگردید.چنانچه از صحت اطلاعات وارده اطمینان ندارید با
     برگشت به مراحل قبل و انتخاب پاسخ های صحیح مجددا عملیات را تکرار نمایید.""")
    continue_system_msg = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '2'}),
                                       label='پیام سیستم در هنگام خاتمه جستجوی فعلی ',
                                          initial='*** در صورتی که مایل به ادامه کار سیستم بر اساس اطلاعات وارده هستید گزینه بلی و در غیر اینصورت گزینه خیر را انتخاب کنید. ***')
    no_knowledge_base_msg = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '2'}),
                                       label='پیام سیستم در صورت عدم وجود پایگاه دانش ',
                                            initial='پایگاه دانش پیدا نشد!!')
    session_finished_msg = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '2'}),
                                       label='پیام سیستم در هنگام اتمام جلسه کاری ',
                                           initial='*** اتمام جلسه کاری ***')
    def clean(self):
        cleaned_data = super(Form_config, self).clean()
        return cleaned_data



def configengine(request):
    username = request.user.username
    if request.POST:
        form = Form_config(request.POST)
        if form.is_valid():
            app_user = username
            no_knowledge_msg = form.cleaned_data['no_knowledge_msg']
            no_question_msg= form.cleaned_data['no_question_msg']
            no_more_rules_msg = form.cleaned_data['no_more_rules_msg']
            continue_system_msg = form.cleaned_data['continue_system_msg']
            no_knowledge_base_msg = form.cleaned_data['no_knowledge_base_msg']
            session_finished_msg = form.cleaned_data['session_finished_msg']
            custommsg={'app_user':app_user, 'no_knowledge_msg':no_knowledge_msg, 'no_question_msg':no_question_msg,
                       'no_more_rules_msg':no_more_rules_msg, 'continue_system_msg':continue_system_msg,
                       'no_knowledge_base_msg':no_knowledge_base_msg, 'session_finished_msg':session_finished_msg}
            save_messages(custommsg)
            return render(request, 'ServiceUser/configengine.html', {'form': form, 'status':'ذخیره اطلاعات با موفقیت انجام گردید.'})
        else:
            return render(request, 'ServiceUser/configengine.html', {'form': form, 'status':'اطلاعات ایراد دارد'})
    else:
        userengmsg= load_messages(username)
        if userengmsg:
            form = Form_config(userengmsg)
            print('user in else confeng')
        else:
            form = Form_config()

            print('def')
        return render(request, 'ServiceUser/configengine.html', {'form': form, 'status':''})


@admin_user_required
def create_user(request):
    if request.POST:
        form = Form_User(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password= form.cleaned_data['password']
            lname = form.cleaned_data['last_name']
            fname = form.cleaned_data['first_name']
            password_bis = request.POST.get( 'password_bis')
            if password and password_bis and password != password_bis:
                raise forms.ValidationError("گذرواژه ها یکسان نمی باشند.")
                return render(request, 'ServiceUser/create_user.html', {'form': form})
            else:
                new_user = User.objects.create_user(username=username, password=password)
            # # In this line, we create an instance of the User model with
                new_user.is_active = True
                new_user.is_staff = False
            # # In this line, the is_active attribute defines whether the user
                new_user.last_name = lname
                new_user.first_name = fname
                # # In this line, we define the name of the new user.
                new_user.save()
                return render(request, 'ServiceUser/user_creation_result.html', {'username': username})
        else:
            return render(request, 'ServiceUser/create_user.html', {'form': form})
    else:
        form = Form_User()
        return render(request, 'ServiceUser/create_user.html', {'form': form})


@admin_user_required
def create_supervisor(request):
    if request.POST:
        form = Form_supervisor(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('ServiceUser:supervisorlist'))
        else:
            return render(request, 'ServiceUser/create_supervisor.html', {'form': form})
    else:
        form = Form_supervisor()
        return render(request, 'ServiceUser/create_supervisor.html', {'form': form})

@admin_user_required
def create_developer(request):
    if request.POST:
        form = Form_developer(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('ServiceUser:developerlist'))
        else:
            return render(request, 'ServiceUser/create_developer.html',  {'form': form})
    else:
        form = Form_developer()
        return render(request, 'ServiceUser/create_developer.html', {'form': form})

@login_required
@permission_required('ServiceUser.add_employee')
def create_employee(request):
    username = request.user.username
    if request.POST:
        form = Form_Employee(request.POST)
        if form.is_valid():
            print('valid')
            # data for auth_user --a real user in system
            fname = form.cleaned_data['fname']
            lname = form.cleaned_data['lname']
            email = form.cleaned_data['email']
            login = form.cleaned_data['login']
            password = form.cleaned_data['password']
            new_user = User.objects.create_user(username = login, password=password)
            # data for auth_user --a real user in system
            # in future we make user active when validate phone number
            new_user.is_active = True
            # in future we make user active when validate phone number
            new_user.is_staff = False
            new_user.first_name=fname
            new_user.last_name=lname
            new_user.email = email
            content_type = ContentType.objects.get_for_model(Employee)
            pub_perm = Permission.objects.get(content_type=content_type, codename='can_use_public_expert')
            rest_perm = Permission.objects.get(content_type=content_type, codename='can_use_restricted_expert')
            content_type = ContentType.objects.get_for_model(App_User)
            # listexp_perm = Permission.objects.get(content_type=content_type, codename='can_use_public_expert')
            new_user.user_permissions.set([pub_perm,rest_perm])
            new_user.save()
            # data for appuser --a user in ServiceUser
            identifyid = form.cleaned_data['identifyid']
            phone = form.cleaned_data['phone']
            manager = form.cleaned_data['manager']
            alowedexperts = form.cleaned_data['alowedexperts']
            create_date = datetime.datetime.now()
            new_employee = Employee.objects.create(user_auth=new_user,identifyid=identifyid,phone=phone,
                                last_connection=create_date,manager=manager)
            new_employee.alowedexperts.set(alowedexperts)
            new_employee.save()
            return HttpResponseRedirect(reverse('ServiceUser:user_staffs'))
        else:
            print('allow:')
            print(form.data)

            data = {'manager': App_User.objects.filter(user_auth__username=username),
                    'alowedexperts':Expertmodel.objects.filter(appuser__user_auth__username=username), 'form':form}
            return render(request, 'ServiceUser/create_employee.html',  data)
    else:

        data = {'manager': App_User.objects.filter(user_auth__username=username),
                'alowedexperts':Expertmodel.objects.filter(appuser__user_auth__username=username), 'form':Form_Employee()}
        return render(request, 'ServiceUser/create_employee.html', data)

@login_required
@permission_required('ServiceUser.change_employee')
def edit_employee_by_index(request, staff_id):
    astaff = get_object_or_404(Employee, Q(pk=staff_id) & Q(manager__user_auth__username=request.user.username))
    if request.method == 'GET':

        staffdata= {'fname':astaff.user_auth.last_name,'lname':astaff.user_auth.first_name,
            'login':astaff.user_auth.username, 'identifyid':astaff.identifyid,'user_auth' :request.user.pk,'last_connection':datetime.date(1960, 8, 1),
            'phone':astaff.phone,'manager':astaff.manager,'alowedexperts':astaff.alowedexperts}
        form = EmployeeForm(instance=astaff)
        print(staffdata['alowedexperts'])
        # form.alowedexperts.set(astaff.alowedexperts)
        print('curr:'+str(astaff))
        return render(request, 'ServiceUser/edit_employee.html',{'form':form, 'astaff':astaff, 'staffdata':staffdata})
    else:

        form = EmployeeForm(request.POST,instance=astaff)
        # form.last_connection = datetime.date(1960, 8, 1)
        # form.save()
        print(str(form))
        if form.is_valid():
            print(form.cleaned_data)
            form.save()
        #     # print('super edited.')
            return HttpResponseRedirect(reverse('ServiceUser:user_staffs'))
        else:
            print(form.cleaned_data)
            return render(request, 'ServiceUser/edit_employee.html',{'form':form, 'astaff':astaff})


@admin_user_required
def edit_supervisor_by_index(request,sup_id):
    if request.method == 'GET':
        asup = get_object_or_404(Supervisor, user_auth=sup_id)
        data= {'user_auth':asup.user_auth, 'identifyid':asup.identifyid, 'phone':asup.phone,
               'last_connection':asup.last_connection, 'specialisation':asup.specialisation}
        form = Form_supervisor(data)
        auths = User.objects.all()
        return render(request, 'ServiceUser/edit_supervisor.html',{'form':form, 'asup':asup, 'auths':auths})
    else:
        asup = get_object_or_404(Supervisor, user_auth=sup_id)
        form = Form_supervisor(request.POST,instance=asup)
        # print(str(request.POST))
        if form.is_valid():
            form.save()
            # print('super edited.')
            return HttpResponseRedirect(reverse('ServiceUser:supervisorlist'))
        else:
            return HttpResponse(form.as_table())

@admin_user_required
def edit_developers_by_index(request,dev_id):
    if request.method == 'GET':
        adev = get_object_or_404(Developer, user_auth__id=dev_id)
        data= {'user_auth':adev.user_auth, 'identifyid':adev.identifyid, 'phone':adev.phone,
               'last_connection':adev.last_connection, 'senior_supervisor':adev.senior_supervisor}
        form = Form_developer(data)
        supers = Supervisor.objects.all()
        return render(request, 'ServiceUser/edit_developer.html',{'form':form, 'adev':adev, 'supers':supers})
    else:
        adev = get_object_or_404(Supervisor, user_auth=dev_id)
        form = Form_supervisor(request.POST,instance=adev)
        # print(str(request.POST))
        if form.is_valid():
            form.save()
            # print('super edited.')
            return HttpResponseRedirect(reverse('ServiceUser:developerlist'))
        else:
            return HttpResponse(form.as_table())

@login_required
@ permission_required('ExpertSystem.add_expertmodel')
def create_expertmodel(request):
    username = request.user.username
    curr_authuser = request.user
    if curr_authuser.is_superuser or curr_authuser.is_staff:
        # return HttpResponseRedirect(reverse('signin'))
        data = {'msg': username}
        return render(request, 'ServiceUser/UserManagement/loginuser.html', data)
    curruser = App_User.objects.get(user_auth__username=username)
    if len(request.POST):
        form = Form_expert(request.POST)
        print(request.POST)
        if form.is_valid() and form.cleaned_data['appuser'].user_auth.username==username:
            form.save()
            request.session['currexpert'] = form.cleaned_data['name']
            tp = curruser.total_apps
            curruser.total_apps = tp+1
            curruser.save()

            return HttpResponseRedirect(reverse('ServiceUser:user_expertmodel'))
        else:
            print(type (form.cleaned_data['appuser']))
            data = {'form': form, 'user': curruser}
            return render(request, 'ServiceUser/new_expert_wizard.html',data)
    else:

        userquata = get_user_quota(curr_authuser)
        print('uq:'+str(userquata))
        userusedquata = {}
        validators = getattr(settings, 'PLANS_VALIDATORS', {})
        for quota in validators:
            validator = import_name(validators[quota])
            quata_validator = validator()
            userusedquata[quata_validator.code] = quata_validator.get_queryset(curr_authuser).count()
        print(userusedquata)
        auth_totalapps = userusedquata['Max_Assist']
        totalapps = curruser.total_apps
        maxapp = userquata['Max_Assist']
        print('auth_totalapps:')
        print(auth_totalapps)
        print('max:')
        print(maxapp)
        if totalapps < maxapp :
            print('max app not lost')
            formdata = {'appuser': curruser, 'name':'سیستم خبره', 'accesstype':'PUBLIC'}
            form = Form_expert(initial=formdata)
            # print(form)
            data = {'form': form, 'user': request.user}
            return render(request, 'ServiceUser/new_expert_wizard.html', data)
        else:
            print('max app lost')
            return HttpResponseRedirect(reverse('ServiceUser:accountlimit'))

@login_required
@ permission_required('ExpertSystem.change_expertmodel')
def userexpertedit(request, exp_id):
   username = request.user.username
   if App_User.objects.filter(user_auth__username=username).exists():
       if len(request.POST):
           form = Form_expert(request.POST)
           if form.is_valid():
               curr = Expertmodel.objects.get(Q(id=exp_id)&Q(appuser__user_auth__username=username))
               curr.appuser = form.cleaned_data['appuser']
               curr.name = form.cleaned_data['name']
               curr.accesstype = form.cleaned_data['accesstype']
               curr.save()
               curapuser = App_User.objects.get(user_auth__username=username)
               tp = curapuser.total_apps
               curapuser.total_apps = tp+1
               curapuser.save()
               return HttpResponseRedirect(reverse('ServiceUser:user_expertmodel'))
           else:
               return render(request, 'ServiceUser/userexpert_edit.html',
                             {'form': form, 'exp_id': exp_id, 'owner': form.cleaned_data['appuser']})
       else:
           curr = Expertmodel.objects.get(Q(id=exp_id)&Q(appuser__user_auth__username=username))
           data={'name':curr.name, 'appuser':curr.appuser, 'accesstype':curr.accesstype}
           form = Form_expert(data)
           return render(request, 'ServiceUser/userexpert_edit.html', {'form':form, 'exp_id':exp_id, 'owner':curr.appuser})
   else:
       return HttpResponseRedirect(reverse(('RuleManager:loginuser')))

@login_required
# @ permission_required('ServiceUser.can_view_expert')
def userexpertstart(request, exp_id):
    username = request.user.username
    if len(request.POST) > 0:

        # form = Contract_Expert(request.POST)
        error = False
        factlist = []
        if 'inittype'in request.POST:
            inittype = request.POST.get('inittype', '')
            if inittype == 'priorfact':
                if 'factlist-subject' in request.POST or 'factlist-detail[]' in request.POST:
                    subject = request.POST.get('factlist-subject', '')
                    if subject != '':
                        factlist.append( subject)
                    for fd in request.POST.getlist('factlist-detail[]', None):
                        factlist.append( fd)
                    print(factlist)
                if not len(factlist):
                    error = True
        else:
            error = True
        if 'datalist' in request.POST:
            datalist = request.POST.get('datalist', '')
        else:
            error = True
            # ctypes.windll.user32.MessageBoxW(0, "Your text", "Your title", 0)
        if not error:
            instance = inferenceengine(datalist, inittype, factlist)
            request.session['instance'] = jsonpickle.encode(instance)
            if request.user.is_authenticated:
                print('auth')
                user=request.user.get_username()
            else:
                print('not auth')
                user='not_signed_in'

            data = {'question': instance.currentquestion, 'question_type': instance.question_type
                , 'whythisquestion': instance.whythis, 'howthisanswer': instance.howthis,
                    'step': instance.currentstatus,'curruser':user, 'exp_id':exp_id}
            # print('3')
            # return render(request, 'ExpertSystem/expert_contract.html',data )
            print('data to view in initengine:'+str(data))
            if request.user.is_authenticated:
                return render(request, 'ExpertSystem/expert_contract_byajax.html', data)
            else:
                request.session['redirect'] = 'ExpertSystem:expert_contract_byajax'
                request.session['data'] = jsonpickle.encode(data)
                print('datencd:'+request.session['data'])
                print('ins:'+request.session['instance'])
                return redirect( reverse('signin'))
            # return render(request, 'ExpertSystem/expert_contract.html', {'instance': instance})
        else:
            if request.user.is_authenticated:
                # print('auth')
                user=request.user.get_username()
            else:
                # print('not auth')
                user='not_signed_in'
            # data = {'facts': Facts.objects.filter(domain__ownerexpert_id=exp_id).filter(fact_effect='COND').order_by(
            #     'pk').reverse(),
            #         'databases': Domain.objects.filter(ownerexpert__appuser__user_auth__username=username), 'curruser': user,
            #         'exp_id': exp_id}
            # return render(request, 'ExpertSystem/user_expertengine.html', data)
            newdata = {'subjectfacts': Facts.objects.filter(Q(domain__ownerexpert_id=exp_id)|Q(domain__ownerexpert_id=1)).
                filter(fact_effect='SUBJ').order_by('pk').reverse(),
                    'detailfacts': Facts.objects.filter(Q(domain__ownerexpert_id=exp_id)|Q(domain__ownerexpert_id=1)).
                        filter(fact_effect='COND').exclude(fact__icontains='موضوع').order_by('pk').reverse(),
                    'databases': Domain.objects.filter(Q(ownerexpert__appuser__user_auth__username=username)|Q(domain__ownerexpert_id=1)),
                    'curruser': user,
                    'exp_id': exp_id}
            return render(request, 'ExpertSystem/newexpertsystem.html', newdata)
    else:
        if request.user.is_authenticated and Expertmodel.objects.filter(Q(appuser__user_auth__username=username)
                                                                        & Q(id=exp_id)| Q(id=1)):
            user=request.user.get_username()
        else:
            user='not_signed_in'
        data = {'subjectfacts': Facts.objects.filter(Q(domain__ownerexpert_id=exp_id)|Q(id=1)).filter(fact_effect='SUBJ').
           order_by('pk').reverse(),
                'detailfacts': Facts.objects.filter(Q(domain__ownerexpert_id=exp_id)|Q(id=1)).filter(fact_effect='COND').
                 exclude(fact__icontains='موضوع').order_by('pk').reverse(),
                'databases': Domain.objects.filter(Q(ownerexpert__appuser__user_auth__username=username)|Q(id=1)),'curruser':user,
                'exp_id':exp_id}
        # print(user)
        return render(request, 'ExpertSystem/newexpertsystem.html', data)
        # return render(request, 'ExpertSystem/test.html', data)


@login_required
def userexpertstartclassic(request, exp_id):
    username =request.user.username
    if len(request.POST) > 0:

        # form = Contract_Expert(request.POST)
        error = False
        factlist = []
        if 'inittype'in request.POST:
            inittype = request.POST.get('inittype', '')
            if inittype == 'priorfact':
                if 'factlist-subject' in request.POST or 'factlist-detail[]' in request.POST:
                    subject = request.POST.get('factlist-subject', '')
                    if subject != '':
                        factlist.append( subject)
                    for fd in request.POST.getlist('factlist-detail[]', None):
                        factlist.append( fd)
                    print(factlist)
                if not len(factlist):
                    error = True
        else:
            error = True
        if 'datalist' in request.POST:
            datalist = request.POST.get('datalist', '')
        else:
            error = True
            # ctypes.windll.user32.MessageBoxW(0, "Your text", "Your title", 0)
        if not error:
            instance = inferenceengine(datalist, inittype, factlist)
            request.session['instance'] = jsonpickle.encode(instance)
            if request.user.is_authenticated:
                print('auth')
                user=request.user.get_username()
            else:
                print('not auth')
                user='not_signed_in'

            data = {'question': instance.currentquestion, 'question_type': instance.question_type
                , 'whythisquestion': instance.whythis, 'howthisanswer': instance.howthis,
                    'step': instance.currentstatus,'curruser':user, 'exp_id':exp_id}
            # print('3')
            # return render(request, 'ExpertSystem/expert_contract.html',data )
            print('data to view in initengine:'+str(data))
            if request.user.is_authenticated:
                return render(request, 'ExpertSystem/expert_contract_byajax.html', data)
            else:
                request.session['redirect'] = 'ExpertSystem:expert_contract_byajax'
                request.session['data'] = jsonpickle.encode(data)
                print('datencd:'+request.session['data'])
                print('ins:'+request.session['instance'])
                return redirect( reverse('RuleManager:loginuser'))
            # return render(request, 'ExpertSystem/expert_contract.html', {'instance': instance})
        else:
            if request.user.is_authenticated:
                # print('auth')
                user=request.user.get_username()
            else:
                # print('not auth')
                user='not_signed_in'
            # data = {'facts': Facts.objects.filter(domain__ownerexpert_id=exp_id).filter(fact_effect='COND').order_by(
            #     'pk').reverse(),
            #         'databases': Domain.objects.filter(ownerexpert__appuser__user_auth__username=username), 'curruser': user,
            #         'exp_id': exp_id}
            # return render(request, 'ExpertSystem/user_expertengine.html', data)
            newdata = {'subjectfacts': Facts.objects.filter(Q(domain__ownerexpert_id=exp_id)|Q(domain__ownerexpert_id=1)).
                filter(fact_effect='SUBJ').order_by('pk').reverse(),
                    'detailfacts': Facts.objects.filter(Q(domain__ownerexpert_id=exp_id)|Q(domain__ownerexpert_id=1)).
                        filter(fact_effect='COND').exclude(fact__icontains='موضوع').order_by('pk').reverse(),
                    'databases': Domain.objects.filter(Q(ownerexpert__appuser__user_auth__username=username)|Q(domain__ownerexpert_id=1)),
                    'curruser': user,
                    'exp_id': exp_id}
            return render(request, 'ExpertSystem/classic_expertsystem.html', newdata)
    else:
        if request.user.is_authenticated and Expertmodel.objects.filter(Q(appuser__user_auth__username=username)
                                                                        & Q(id=exp_id)| Q(id=1)):
            user=request.user.get_username()
        else:
            user='not_signed_in'
        data = {'subjectfacts': Facts.objects.filter(Q(domain__ownerexpert_id=exp_id)|Q(id=1)).filter(fact_effect='SUBJ').
           order_by('pk').reverse(),
                'detailfacts': Facts.objects.filter(Q(domain__ownerexpert_id=exp_id)|Q(id=1)).filter(fact_effect='COND').
                 exclude(fact__icontains='موضوع').order_by('pk').reverse(),
                'databases': Domain.objects.filter(Q(ownerexpert__appuser__user_auth__username=username)|Q(id=1)),'curruser':user,
                'exp_id':exp_id}
        # print(user)
        return render(request, 'ExpertSystem/classic_expertsystem.html', data)
        # return render(request, 'ExpertSystem/test.html', data)

@login_required
def upgradeaccount(request):
    username = request.user.username
    if request.user.is_authenticated and not request.user.is_superuser:
        curruser = App_User.objects.get(user_auth__username=username)
        if len(request.POST):
            upmode = request.POST.get('mode', '')
            if upmode == 'subscription':
                newsubscription = request.POST.get('userchoice', '')
                if newsubscription =='custom':
                    tu = request.POST.get('totalusers', '')
                    te = request.POST.get('totalexperts', '')
                    asub = Subscription.objects.create(creation_mode=Subscription._USER_MODE,
                        subscription_type=Subscription._CUSTOM,
                                        app_limit=Subscription._APP_CUSTOM,support_period=Subscription._SUP_FULL,
                                        user_range=Subscription._TOTAL_USER_BASIC,subscription_rate=Subscription._COST_CUSTOM,
                                        custom_app_limit=te,custom_user_limit=tu)

                    curruser.user_subscription = asub
                    curruser.ismanager = True
                    curruser.save(update_fields=['user_subscription', 'ismanager'])
                    return JsonResponse("success", safe=False)
                else:
                    curruser.user_subscription = Subscription.objects.get(subscription_type=newsubscription)
                    if newsubscription == Subscription._TRIAL:
                        curruser.ismanager = False
                    else:
                        curruser.ismanager = True
                    curruser.save(update_fields=['user_subscription', 'ismanager'])
                    print("success")
                    return JsonResponse("success", safe=False)
            elif upmode == 'password':
                form = Form_Password(request.POST)
                if form.is_valid():
                    print(form.cleaned_data)
                    # print(form)
                    oldpass = form.cleaned_data['oldpass']
                    newpass = form.cleaned_data['newpass']
                    if curruser.user_auth.check_password(oldpass):
                        curruser.user_auth.set_password(newpass)
                        curruser.user_auth.save()
                        return HttpResponseRedirect(reverse('ServiceUser:user_dashboard'))
                    else:
                        print('not old pass')
                        form.add_error('oldpass','گذرواژه قدیمی را بطور صحیح وارد کنید')
                        useraccount = curruser.user_subscription
                        allaccounttype = Subscription.objects.filter(creation_mode=Subscription._SYS_MODE)
                        data = {'allaccounttype': allaccounttype, 'useraccount': useraccount, 'passform': form}
                        return render(request, 'ServiceUser/useraccount.html', data)
                else:
                    useraccount = curruser.user_subscription
                    allaccounttype = Subscription.objects.filter(creation_mode=Subscription._SYS_MODE)
                    data = {'allaccounttype': allaccounttype, 'useraccount': useraccount, 'passform': form, 'mode':'password'}
                    print('pass')
                    return render(request, 'ServiceUser/useraccount.html',data)
            elif upmode == 'email':
                form = Form_Email(request.POST)
                if form.is_valid():
                    print(form.cleaned_data)
                    newmail = form.cleaned_data['newemail']
                    curruser.user_auth.email = newmail
                    curruser.user_auth.save()
                    return HttpResponseRedirect(reverse('ServiceUser:user_dashboard'))
                else:
                    useraccount = curruser.user_subscription
                    allaccounttype = Subscription.objects.filter(creation_mode=Subscription._SYS_MODE)
                    data = {'allaccounttype': allaccounttype, 'useraccount': useraccount, 'mailform': form, 'mode':'email'}
                    print('email')
                    return render(request, 'ServiceUser/useraccount.html', data)
    else:
        print(request)
        print(request.user)
        print(request.user.is_authenticated)
        print(request.user.is_superuser)


def extractfacts( conds):
    print('conds in extf:'+conds)
    splited = conds.partition(' AND ')
    if ' AND ' not in splited:
        if ' OR ' not in splited[0]:
            print('here1:'+splited[0])
            # splited = splited[0].partition('NOT ')
            if 'NOT ' not in splited[0]:
                print('here added to rulefacts 2:' + splited[0])
                # rulefacts.append(splited[0])
                rulefacts.append(splited[0])
                return splited[0]
            else:
                print('here we can added to rulefacts 3:' + splited[2])
                # rulefacts.append(splited[2])
                print('***but we  added NOT to rulefacts 3***:' + splited[0])
                rulefacts.append(splited[0])
                return splited[0]
            # print('here tempres 1:' + splited[0])
        else:
            splited = splited[0].partition(' OR ')
            print('here4:' + splited[0])
            rulefacts.append( splited[0])
            print('here tempres 5:' + splited[2])
            rulefacts.append(extractfacts( splited[2]))

    else:
        print('here6:' + splited[0])
        rulefacts.append( splited[0])
        print('here tempres 7:' + splited[2])
        extractfacts( splited[2])
        return rulefacts

def update_detailfacts(request, exp_id):
    username = request.user.username
    if len(request.POST):
        domain = request.POST.get('domainid', '')
        if domain == 'null' or domain =='':
            domain = 1
        domain = int(domain)
        subject = request.POST.get('subject_fact', '')
        print(subject)
        print(domain)
        details_related_to_subject = []
        currusername = request.user.get_username
        if currusername == 'demouser' or currusername == 'amozesh' or domain ==1:
            factfilter = Q(domain__desc__icontains='پیمانکاران') \
                     | Q(ownerexpert__appuser__user_auth__username='demouser') \
                     | Q(ownerexpert__appuser__user_auth__username='amozesh') | Q(ownerexpert_id=1)
        else:
            factfilter = Q(ownerexpert__appuser__user_auth=username) & Q(domain_id=domain) & Q(ownerexpert_id=exp_id)
        if subject == 'all_detail' :

            alldetailfacts = Facts.objects.filter(factfilter). \
                filter(fact_effect='COND').exclude(fact_effect='SUBJ').order_by('pk').reverse()
            for value in list(alldetailfacts.values('fact')):
                print(type (value))
                print(value)
                details_related_to_subject.append(value['fact'])
                details_related_to_subject.append('NOT ' + value['fact'])
            print('final:'+str(details_related_to_subject))
            print(alldetailfacts)
            data = json.dumps(details_related_to_subject, ensure_ascii=False)
            return HttpResponse(data, content_type="text/json-comment-filtered")
        else:
            for rule in Rules.objects.filter(factfilter & Q(condition__icontains=subject)):
                rulefacts.clear()
                print(rule.condition)
                allfacts = extractfacts( rule.condition)
                if not isinstance(allfacts, list):
                    allfacts = [allfacts]
                print('all f:'+str(allfacts))
                details = remained_facts(allfacts, subject)
                print('remdet:'+str(details))
                for detail in details:
                    if detail not  in details_related_to_subject:
                        details_related_to_subject.append(detail)
            print('final:'+str(details_related_to_subject))
            data = json.dumps(details_related_to_subject,ensure_ascii=False)
            # print('data in updat:')
            # print(data)
            return HttpResponse(data, content_type="text/json-comment-filtered")
    else:
        return HttpResponse('not valid request')


def remained_facts( bigger, lesser):
    if not isinstance(lesser, list):
        lesser = [lesser]
    print('in _remainfact:--' + 'bigger:' + str(bigger))
    print('in _remainfact:--' + 'lesser:' + str(lesser) )
    bigger = set(bigger)
    lesser = set(lesser)
    if bigger != lesser:
        # print('in _remainfact:--' + 'remained:' + str(list(bigger-lesser)) )
        return list(bigger-lesser)
    elif bigger == lesser:
        # print('in _remainfact:--' + 'remained:' + str(list(bigger-lesser)) )
        return list(bigger-lesser)
