from django.db import models
from ServiceUser.models import App_User, Employee
# from RuleManager.models import Rules, Facts
# Create your models here.
class Comments(models.Model):

    name = models.CharField( max_length=255, blank=False )
    email=models.EmailField(max_length=255, blank=False )
    subject = models.TextField(max_length=3000, blank=False )

    def __str__(self):
        return (self.subject[:100] + '...') if len(self.subject) > 100 else self.subject

class Expertmodel(models.Model):
    _PUB = 'PUBLIC'
    _USER = 'RESTRICTED'
    _ACCESS = [(_PUB, 'دسترسی عمومی'),(_USER, 'دسترسی فقط برای کاربران')]
    name = models.CharField(max_length=200, verbose_name='نام سیستم خبره')
    appuser = models.ForeignKey('ServiceUser.App_User', on_delete=models.DO_NOTHING, related_name='user', verbose_name='مالک سیستم خبره')
    accesstype = models.CharField(choices=_ACCESS, default=_PUB,max_length=10,  verbose_name='نحوه دسترسی به سیستم خبره')
    # authorizedusers = models.ManyToManyField(Employee,related_name='allowedusers_fiels',
    #                                          limit_choices_to=Employee.objects.filter(manager=appuser))



    def get_related_facts(self):
        fact_set =self.factowner_field.all()
        if fact_set.count():
            return fact_set
        else:
            return None

    def get_related_rules(self):
        rule_set =self.ruleowner_field.all()
        if rule_set.count():
            return rule_set
        else:
            return None

    def get_related_domains(self):
        domain_set = self.domainowner_field.all()
        if domain_set.count():
            return domain_set
        else:
            return None

    def __str__(self):
        return self.name

    def getowneruser(self):
        return self.appuser.user_auth.username