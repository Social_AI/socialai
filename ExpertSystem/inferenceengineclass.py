""""
 این کلاس  موتور استنتاج را پیاده سازی می کند.
 پس از ایجاد یک نمونه از این کلاس با استفاده از currentquestion در هر مرحله
 بر اساس قوانین موجود در پایگاه قوانین یک سوال مطرخ می شود و سپس در ادامه قوانین دنبال می شوند تا جایی که
 امکان یافتن قانون جدیدی بر اساس اطلاعات موجود در workingmemory وجود نداشته باشد.
 سیستم سپس بر اساس فرایند ارزیابی ، قانون(سوال) جدیدی از کاربر می پرسد و پاسخ کاربر
 با تابع evalanswer ارزیابی و بر اساس ان قانون جدیدی انتخاب(سوال جدیدی ایجاد )و در currentquestion  قرار می گیرد.
 روند کلی استفاده از کلاس:
 1- ایجاد یک نمونه از کلاس
 2- بدست اوردن مقدار currentquestion  و نمایش به کاربر
 3-ارسال پاسخ کاربر به تابع evalanswe
 4- تکرا مراحل 2 و 3
"""

from RuleManager.models import Rules , Facts
from Contract_Assistant.settings import BASE_DIR
from itertools import combinations
from django.db.models import Q
from django.http import Http404
import os , csv ,datetime, random, sys
from hazm import *
from Contract_Assistant.utility import to_persian
modelpath = os.path.join(BASE_DIR, 'static/ExpertSystem/resources')
normalizer = Normalizer()
tagger = POSTagger(model=os.path.join(BASE_DIR,'static/ExpertSystem/resources', 'postagger.model'))

# import from hazm

class inferenceengine(object):
    _workingmemory = []
    _foundfacts = []
    _questions = []
    _answers = []
    currentquestion = ''
    currentstatus = ''
    _curr_rule =None
    _curr_fact = None
    _YESNO = 'Y/N'
    _EXPRESSING = 'EXP'
    question_type =''
    whythis = ''
    howthis = ''
    _fortesting = []
    _sessiondata=[]
    _tempdata = {}
    _innerstatus = []
    _knowledgebasename=''
    _knowledgeid = 1
    _enginemode = ''    #valid values: priorfact, first, random
    _firstfact = []
    _firstfact2 = []
    _excluderules = []
    logicblocks = []
    _currfact_value = True
    _operand_type = ''
    _ceriteria_value = 0
    global _logfile

    _ST_PROCCESSING = 1
    _ST_WAITING_FOR_REPLY = 0
    _INITIAL_MODE_FIRST = "first"
    _INITIAL_MODE_RANDOM = "random"
    _INITIAL_MODE_FACTS = "priorfact"
    _NO_KNOWLEDGE_MSG= 'پایگاه دانش فاقد قوانین و دانش مرتبط با موضوع انتخاب شده می باشد! لطفا موضوع را به مدیر سیستم اطلاع دهید.'
    _NO_QUESTION_MSG = 'سوال مورد نظر یافت نشد!'
    _NO_MORE_RULES_MSG ="""با توجه به اطلاعات وارد شده توسط کاربر، پاسخی که همه شرایط را براورده کند یافت نگردید.چنانچه از صحت اطلاعات وارده اطمینان ندارید با
     برگشت به مراحل قبل و انتخاب پاسخ های صحیح مجددا عملیات را تکرار نمایید."""
    _CONTINUE_SYSTEM_MSG = '.***آیا ادامه می دهید؟'
    _NO_KNOWLEDGE_BASE_MSG = 'پایگاه دانش پیدا نشد!!'
    _SESSION_FINISHED_MSG = '*** اتمام جلسه کاری ***'
                            # ' و برای ادامه جستجو از لیست بالا پاسخ "بلی" را انتخاب و دکمه مرحله بعد را فشار دهید.'

    def __init__(self,knowledgebase = 1, initialmode =_INITIAL_MODE_FIRST, priorfact = []):
        self._fortesting.append('in __init__:--' + self.currentquestion)
        self._fortesting.append('in __init__:--prior=' + str(priorfact))
        self._enginemode = initialmode
        self._knowledgeid = knowledgebase
        self._foundfacts = []
        self._workingmemory = []
        self._excluderules = []
        self.whythis = ''
        self.howthis = ''
        self.currentstatus = 'beginsession'
        # ********************* for test only ******************
        basepath = os.path.join(os.getcwd(), 'Contract_Assistant/ExpertSystem/ModelsData')
        # basepath = os.path.join(os.getcwd(), 'ModelsData')
        logdata = '--- knowledgebase='+str(knowledgebase)+'\n--- initialmode='\
                  +initialmode+'\n--- priorfact='+str(priorfact)
        self._logfile = open(os.path.join(basepath, 'log.txt'), 'a+', 1, 'utf-8')
        log_size = os.path.getsize(os.path.join(basepath, 'log.txt'))
        # if log_size/1e+6 > 2:
            # self._logfile.truncate()
        self._logfile.write('***********************************************\n')
        self._logfile.write('Run log in ' + str(datetime.datetime.now()) + '\n')
        self._logfile.write('***Initializing engine start***\n')
        self._logfile.write(logdata)
        self._logfile.write('\n')
        self._logfile.write('***Initializing engine end***\n')
        self._logfile.write('***Runtime parameters:***\n')
        self._logfile.write('in init: ' + 'curr wm=' + str(self._workingmemory) + '\n')
        self._logfile.write('in init: ' + 'curr ff=' + str(self._foundfacts) + '\n')
        self._logfile.write('***********************************************\n')
        # ********************* for test only ******************
        self._knowledgebasename = knowledgebase
        #********** modify here ***************
        # result = self._load_data_fromfile(knowledgebase)
        # if result == 'nofact':
        #     self.question = 'حقایق اولیه یافت نشد' + '\n'+ 'لطفا پس از ثبت حقایق اولیه، برنامه را مجددا راه اندازی نمایید'
        #     return
        # elif result == 'norule':
        #     self.question = 'قوانین یافت نشد' + '\n' + 'لطفا پس از ثبت قوانین، برنامه را مجددا راه اندازی نمایید'
        #     return
            # ************* modify here ***************
            # if knowledgebase != 'Default':
        #     self._load_data_fromfile(knowledgebase)
        # ************* modify here ***************
        if not Facts.objects.filter(domain_id=self._knowledgeid).count():
            self.currentquestion = self._NO_KNOWLEDGE_BASE_MSG
            self.question_type = self._NO_KNOWLEDGE_BASE_MSG
            print(self._NO_KNOWLEDGE_BASE_MSG)
            return
        # ************* modify here ***************
        # ************* modify here ***************
        if Rules.objects.filter(domain_id=self._knowledgeid).count():
        # ************* modify here ***************
            self._registerstatus()
            print('*** In init status registered ***')
            self._logfile.write('*** In init status registered ***\n')
            if initialmode == self._INITIAL_MODE_FIRST:
                # ************* modify here ***************
                self._curr_rule = Rules.objects.filter(domain_id=self._knowledgeid).first()
                # ************* modify here ***************
                self._tempdata['rul_id'] = self._curr_rule.id
                quest = self._curr_rule.condition
                # if self._curr_rule.rule_type == self._YESNO:
                #     self.question_type = self._YESNO
                # else:
                #     self.question_type = self._EXPRESSING
                if quest.strip():
                    self._logfile.write('in init: ' + 'extract fact from current condition=' + quest+'\n')
                    self._logfile.write('***in init: goto _extractfact\n')
                    self._extractfact(quest)
                    self._logfile.write('***in init: return from _extractfact\n')
                    for fact in self._foundfacts:
                        self._fortesting.append('in init: '+'curr fact='+fact)
                        self._logfile.write('in init: '+'curr fact to make question='+fact+'\n')
                        if 'NOT ' in fact:
                            purefact = fact.split('NOT ')[1]
                            # ************* modify here ***************
                            self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=purefact).fact_type
                            # ************* modify here ***************
                        else:
                            # ************* modify here ***************
                            self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=fact).fact_type
                            # ************* modify here ***************

                        self._logfile.write('***in init: goto _makequestion\n')
                        self._makequestion(fact)
                        self._logfile.write('***in init: return from _makequestion\n')
                        return
                else:
                    quest = self._NO_QUESTION_MSG
                    # self._logfile.write('***in init: goto _makequestion\n')
                    # # self._makequestion(quest)
                    # self._logfile.write('***in init: return from _makequestion\n')
                    self.currentquestion = quest
                    self.question_type = 'Finished'
                    return
            elif initialmode == self._INITIAL_MODE_RANDOM:
                self._foundfacts = []
                # ************* modify here ***************
                ind = random.randint(1, Rules.objects.filter(domain_id=self._knowledgeid).count())
                self._curr_rule = Rules.objects.filter(domain_id=self._knowledgeid).all()[ind-1]
                # ************* modify here ***************
                self._tempdata['rul_id'] = self._curr_rule.id
                quest = self._curr_rule.condition
                # if self._curr_rule.rule_type == self._YESNO:
                #     self.question_type = self._YESNO
                # else:
                #     self.question_type = self._EXPRESSING
                if quest.strip():
                    self._logfile.write('***in init: goto _extractfact\n')
                    self._extractfact(quest)
                    self._logfile.write('***in init: return from _extractfact\n')
                    for fact in self._foundfacts:
                        if 'NOT ' in fact:
                            purefact = fact.split('NOT ')[1]
                            # ************* modify here ***************
                            self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=purefact).fact_type
                            # ************* modify here ***************
                        else:
                            self._logfile.write('in init: '+'curr fact to make question='+fact+'\n')
                            # ************* modify here ***************
                            self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=fact).fact_type
                            # ************* modify here ***************
                        self._fortesting.append('in init: '+'curr fact='+fact)

                        self._logfile.write('***in init: goto _makequestion\n')
                        self._makequestion(fact)
                        self._logfile.write('***in init: return from _makequestion\n')
                        return
                else:
                    quest = self._NO_QUESTION_MSG
                    self._logfile.write('***in init: goto _makequestion\n')
                    self._makequestion(quest)
                    self._logfile.write('***in init: return from _makequestion\n')
            else:       # _INITIAL_MODE_FACTS
                self._foundfacts = []
                databasematched = False
                facttocheck = []
                _NOTincluded = False
                _detailincluded = False
                for pf in priorfact:
                    self._workingmemory.append(pf)
                    self._logfile.write('***in init: firstfact is:'+str(self._firstfact)+ '\n')
                    # ************* modify here  to check fact type in database if a fact is subject fact***************
                    purefact = pf
                    if 'NOT ' in pf:
                        _NOTincluded = True
                        purefact = pf[4:len(pf)]
                    if Facts.objects.get(fact__iexact=purefact).fact_effect != Facts._SUBJECT:
                    # if 'موضوع' not in pf:
                        _detailincluded = True
                    # ************* modify here  to check fact type in database if a fact is subject fact***************

                    self._fortesting.append('in init: ' + 'fact added to memory=' + pf)
                    self._logfile.write('in init: ' + 'fact added to memory=' + pf + '\n')
                self._firstfact = list.copy(priorfact)
                print('pf:')
                print(self._firstfact)
                # ************* modify here ***************
                if _NOTincluded:
                    StartRules = Rules.objects.filter(domain_id=self._knowledgeid).\
                        filter(self._qcondition(self._workingmemory))
                else:
                    StartRules = Rules.objects.filter(domain_id=self._knowledgeid).\
                    filter(self._qcondition(self._workingmemory)).\
                        exclude(self._qorcondition(self._negativefact(self._workingmemory)))
                if len(StartRules):
                    # ************* modify here ***************
                    databasematched = True
                    facttocheck = self._workingmemory
                if not databasematched:
                    if _detailincluded:
                        self.currentquestion = self._NO_MORE_RULES_MSG
                        return
                    else:
                        self.currentquestion = self._NO_KNOWLEDGE_MSG
                        return
                else:
                    # ************* modify here ***************
                    matchedrule = Rules.objects.filter(domain_id=self._knowledgeid).filter(self._qcondition(facttocheck))
                    # ************* modify here ***************
                    if len(matchedrule):
                        for rule in matchedrule:
                            self._fortesting.append('in init: goto _extractfact')
                            self._logfile.write('in init: goto _extractfact' + '\n')
                            self._extractfact( rule.condition)
                            self._fortesting.append('in init: return from _extractfact')
                            self._logfile.write('in init: return from _extractfact' + '\n')
                            self._curr_rule = rule
                            print('in init-prior fact-currule:'+str(rule))
                            # ************* modify here ***************
                            self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=self._foundfacts[0]).fact_type
                            self._tempdata['rul_id'] = self._curr_rule.id
                            self._evaluaterulelogic(rule)

                            # self._choosenextrule(pf)
                            # if self.howthis != '':
                            return
                    else:
                        self.currentquestion = self._NO_MORE_RULES_MSG
                        return
        else:
            self.currentquestion = self._NO_KNOWLEDGE_BASE_MSG
            self.question_type = self._NO_KNOWLEDGE_BASE_MSG
            return

    def _load_data_fromfile(self, knowledgebase):
        # knowledgebase: folder name that Fact data and Rule data are stored in 2 seperate file
        # with file name formated like these:
        # for Facts in format "knowledgebase_facts"
        # for Rules in format "knowledgebase_rules"
        #in first we retrive all facts from file and store them in database and then
        # retrive all rules from file and store them in database
        if knowledgebase == 'Default' and Rules.objects.filter(domain_id=self._knowledgeid).count() == 0 :
            return 'norule'
        if knowledgebase == 'Default' and Facts.objects.filter(domain_id=self._knowledgeid).count() ==0:
            return  'nofacts'
        basepath = os.path.join(os.getcwd(), 'Contract_Assistant/ExpertSystem/ModelsData',knowledgebase)
        defaultpath = os.path.join(os.getcwd(), 'Contract_Assistant/ExpertSystem/ModelsData')
        if knowledgebase == 'Default':
            basepath = defaultpath
            if not os.path.exists(os.path.join(defaultpath, 'Default_facts.csv')):
                return
        # if we choose use new data to load in system then save primary data to csv files
        else:
            # store all primary facts to a csv file by name:Default_facts.csv
            with open(os.path.join(defaultpath, 'Default_facts.csv'),'w', 1, 'utf-8') as csvfile:
                fieldnames = ['fact', 'fact_type']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                for fact in Facts.objects.filter(domain_id=self._knowledgeid).all():
                    writer.writerow({'fact': fact.fact, 'fact_type': fact.fact_type})
            # store all primary rules to a csv file by name:Default_rules.csv
            with open(os.path.join(defaultpath, 'Default_rules.csv'),'w', 1, 'utf-8') as csvfile:
                fieldnames = ['condition', 'consequence', 'pub_date' ]
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                for rule in Rules.objects.filter(domain_id=self._knowledgeid).all():
                    writer.writerow({'condition': rule.condition, 'consequence': rule.consequence,'pub_date': rule.pub_date})

        # in first we collect facts data from file
        with open(os.path.join(basepath, knowledgebase+'_facts.csv')) as csvfile:
            reader = csv.DictReader(csvfile)
            if reader.line_num > 1:
                Facts.objects.filter(domain_id=self._knowledgeid).all().delete()
            else:
                return 'nofact'
            for row in reader:
                afact = Facts(fact = row['fact'],fact_type=row['fact_type'])
                afact.save()
        # #now we collect rules data from file
        with open(os.path.join(basepath, knowledgebase + '_rules.csv')) as csvfile:
            reader = csv.DictReader(csvfile)
            if reader.line_num > 1:
                Rules.objects.filter(domain_id=self._knowledgeid).all().delete()
            else:
                return 'norule'
            for row in reader:
                condition = row['condition']
                consequencedata = row['consequence']
                tmpfact = Facts.objects.filter(domain_id=self._knowledgeid).get(consequence_field=consequencedata)
                pub_data = row['pub_date']
                arule = Rules(condition=condition,consequence=tmpfact,pub_date=pub_data)
                arule.save()

    def _firerule(self, rule):
        print('priorfact in evalanswer:')
        print(self._firstfact)
        self._logfile.write('in _firerule:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _firerule:--_firstfact:' + str(self._firstfact))
        self._fortesting.append('in __firerule_currentquestion:--' + self.currentquestion)
        self._logfile.write('in __firerule_:--currentquestion:' + self.currentquestion+'\n')
        cons = rule.consequence.fact
        if cons not in self._workingmemory:
            self._curr_rule = rule
            self._tempdata['rul_id'] = self._curr_rule.id
            self._workingmemory.append(cons)
            self._logfile.write('***in _firerule: firstfact is:'+str(self._firstfact)+ '\n')
            self._tempdata['workingmemory']=self._workingmemory
            self._fortesting.append('in _firerule:--' + 'fact added to wm:'+cons.__str__())
            self._logfile.write('in _firerule:--' + 'fact added to wm:'+cons.__str__() + '\n')
        self._logfile.write('in _firerule:--now go to _howthisanswer:' + '\n')
        self._howthisanswer()
        self._logfile.write('in _firerule:--return from _howthisanswer:' + '\n')
        if self._enginemode == self._INITIAL_MODE_FACTS:
            self._logfile.write('in _firerule:--now go to _choosenextrule:' + '\n')
            self._choosenextrule(cons)
            self._logfile.write('in _firerule:--return from _choosenextrule:' + '\n')
            if self.question_type == 'Finished':
                self.currentquestion = cons
                self._logfile.write('in _firerule:--we found answer:' + cons + '\n')
                self._tempdata['question'] = self.currentquestion
                self._questions.append(self.currentquestion)
                self._tempdata['question_type'] = self.question_type
                self._sessiondata.append(self._tempdata)
                print('*** In _firerule status registered ***')
                self._logfile.write('*** In _firerule status registered ***\n')
                self._registerstatus()
        else:
            self.currentquestion =  cons + self._CONTINUE_SYSTEM_MSG
            self._tempdata['question'] = self.currentquestion
            self._questions.append(self.currentquestion)
            self._tempdata['question_type'] = self.question_type
            self._sessiondata.append(self._tempdata)
            print('*** In _firerule status registered ***')
            self._logfile.write('*** In _firerule status registered ***\n')
            self._logfile.write('in _firerule:--we found answer:' + cons + '\n')
        self._logfile.write('end _firerule:--_firstfact:' + str(self._firstfact) + '\n')

    def _evaluaterule(self, rule):
        print('priorfact in _evaluaterule:')
        print(self._firstfact)
        self._logfile.write('in _evaluaterule:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _evaluaterule:--_firstfact:' + str(self._firstfact))
        self._fortesting.append('in __evaluaterule__currquestion:' + self.currentquestion)
        self._logfile.write('in __evaluaterule__currquestion:' + self.currentquestion + '\n')
        self._fortesting.append('in _evaluaterule:--' + 'start')
        self._logfile.write('in _evaluaterule:--' + 'start' + '\n')
        self._foundfacts = []
        cond = rule.condition
        self._logfile.write('***in _evaluaterule: goto _extractfact\n')
        self._extractfact(cond)
        self._logfile.write('***in _evaluaterule: return from _extractfact\n')
        if len(self._foundfacts) > 1:
            # self._fortesting.append('in _evaluaterule:--' + 'fact sdd to wm:' + self._foundfacts[0])
            # self._workingmemory.append(self._foundfacts[0])
            rulfact = self._foundfacts
            for fact in rulfact:
                self._fortesting.append('in _evaluaterule:--' + 'in for-curr fact:'+fact)
                self._logfile.write('in _evaluaterule:--' + 'in for-curr fact:'+fact + '\n')
                if fact not in self._workingmemory :
                    if self._negativefact(fact) in self._workingmemory:
                        self._logfile.write('***in _evaluaterule-selected rule: curfact is:' + str(fact)
                                            + ' but '+self._negativefact(fact)+' is in wm\n')
                        self._logfile.write('***in _evaluaterule-now try get new rule' + '\n')
                        self._choosenextrule('null')
                    else:
                        self._fortesting.append('in _evaluaterule:--' + fact+' not in _workingmemory --rulfact:'+str(rulfact))
                        self._logfile.write('in _evaluaterule:--' + fact+' not in _workingmemory --memory is:'+str(rulfact) + '\n')
                        self._fortesting.append('in _evaluaterule:--now makequestion('+fact+')')
                        self._logfile.write('in _evaluaterule:--now makequestion('+fact+')--all facts: ' + str(rulfact) + '\n')
                        # self._foundfacts = []
                        # self._choosenextrule('null')
                        self._tempdata['workingmemory'] = self._workingmemory
                        # self._innerstatus.append(self._tempdata.copy())
                        # self._tempdata = {}
                        print('fact in evalrule:'+fact)
                        if 'NOT ' in fact:
                            purefact = fact.split('NOT ')[1]
                            # ************* modify here ***************
                            self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=purefact).fact_type
                        else:
                            self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=fact).fact_type
                            # ************* modify here ***************
                        self._logfile.write('***in _evaluaterule: goto _makequestion:'+fact+'\n')
                        self._makequestion(fact)
                        self._logfile.write('***in _evaluaterule: return from _makequestion:'+fact+'\n')
                        return
            self._fortesting.append('in _evaluaterule:--' + '_workingmemory:' + str(self._workingmemory))
            self._logfile.write('in _evaluaterule:--' + '_workingmemory:' + str(self._workingmemory) + '\n')
            self._fortesting.append('in _evaluaterule:--' + 'now wlii fire rule:' + rule.__str__())
            self._logfile.write('in _evaluaterule:--' + 'now wlii fire rule:' + rule.__str__() + '\n')
            self._logfile.write('***in _evaluaterule: goto _firerule1\n')
            self._firerule(rule)
            self._logfile.write('***in _evaluaterule: return _firerule1\n')
        else:
            self._fortesting.append('in _evaluaterule:--' + 'end.rule:'+rule.__str__() )
            self._logfile.write('in _evaluaterule:--' + 'end.rule:'+rule.__str__() + '\n')
            self._logfile.write('***in _evaluaterule: goto _firerule2\n')
            self._firerule(rule)
            self._logfile.write('***in _evaluaterule: return _firerule2\n')
        self._logfile.write('end _evaluaterule:--_firstfact:' + str(self._firstfact2) + '\n')

    def _evaluaterulelogic(self, rule):
        print('priorfact in _evaluaterulelogic:')
        print(self._firstfact)
        self._logfile.write('in _evaluaterulelogic:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _evaluaterulelogic:--_firstfact:' + str(self._firstfact))
        self._fortesting.append('in _evaluaterulelogic_currquestion:' + self.currentquestion)
        self._logfile.write('in __evaluaterulelogic__currquestion:' + self.currentquestion + '\n')
        self._fortesting.append('in _evaluaterulelogic:--' + 'start')
        self._logfile.write('in __evaluaterulelogic:--' + 'start' + '\n')
        cond = rule.condition
        self._logfile.write('***in __evaluaterulelogic: goto _extractlogicblocks\n')
        self.logicblocks = []
        self._extractlogicblocks(cond)
        self._logfile.write('***in __evaluaterulelogic: return from _extractlogicblocks\n')
        for lb in self.logicblocks:
            if self.question_type == 'Finished':
                break
            self._foundfacts = []
            self._logfile.write('***in _evaluaterulelogic: goto _extractfact\n')
            self._extractfact(lb)
            self._logfile.write('***in _evaluaterulelogic: return from _extractfact\n')
            if len(self._foundfacts) > 1:
                blockfact = self._foundfacts
                for fact in blockfact:
                    self._fortesting.append('in _evaluaterulelogic:--' + 'in for-curr fact:'+fact)
                    self._logfile.write('in _evaluaterulelogic:--' + 'in for-curr fact:'+fact + '\n')
                    if fact not in self._workingmemory :
                        if self._negativefact(fact) in self._workingmemory:
                            self._logfile.write('***in _evaluaterulelogic-selected rule: curfact is:' + str(fact)
                                            + ' but '+self._negativefact(fact)+' is in wm\n')
                            self._logfile.write('***in _evaluaterulelogic-now try get new rule' + '\n')
                            self._choosenextrule('null')
                        else:
                            self._fortesting.append('in _evaluaterulelogic:--' + fact+' not in _workingmemory --rulfact:'+str(blockfact))
                            self._logfile.write('in _evaluaterulelogic:--' + fact+' not in _workingmemory --found facts is:'+str(blockfact) + '\n')
                            self._fortesting.append('in _evaluaterulelogic:--now makequestion('+fact+')')
                            self._logfile.write('in _evaluaterulelogic:--now makequestion('+fact+')--all found facts: ' + str(blockfact) + '\n')
                        # self._foundfacts = []
                        # self._choosenextrule('null')
                            self._tempdata['workingmemory'] = self._workingmemory
                        # self._innerstatus.append(self._tempdata.copy())
                        # self._tempdata = {}
                            print('fact in evalrule:'+fact)
                            if 'NOT ' in fact:
                                purefact = fact.split('NOT ')[1]
                            # ************* modify here ***************
                                self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=purefact).fact_type
                            else:
                                self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=fact).fact_type
                            # ************* modify here ***************
                            self._logfile.write('***in __evaluaterulelogic: goto _makequestion:'+fact+'\n')
                            self._makequestion(fact)
                            self._logfile.write('***in __evaluaterulelogic: return from _makequestion:'+fact+'\n')
                            return
                self._fortesting.append('in _evaluaterulelogic:--' + '_workingmemory:' + str(self._workingmemory))
                self._logfile.write('in _evaluaterulelogic:--' + '_workingmemory:' + str(self._workingmemory) + '\n')
                self._fortesting.append('in _evaluaterulelogic:--' + 'now wlii fire rule:' + rule.__str__())
                self._logfile.write('in _evaluaterulelogic:--' + 'now wlii fire rule:' + rule.__str__() + '\n')
                self._logfile.write('***in _evaluaterulelogic: goto _firerule1\n')
                self._firerule(rule)
                self._logfile.write('***in _evaluaterulelogic: return _firerule1\n')
            else:
                self._fortesting.append('in _evaluaterulelogic:--' + 'end.rule:'+rule.__str__() )
                self._logfile.write('in _evaluaterulelogic:--' + 'end.rule:'+rule.__str__() + '\n')
                self._logfile.write('***in __evaluaterulelogic: goto _firerule2\n')
                self._firerule(rule)
                self._logfile.write('***in _evaluaterulelogic: return _firerule2\n')
        self._logfile.write('end _evaluaterulelogic:--_firstfact:' + str(self._firstfact2) + '\n')

    def _extractlogicblocks(self, conds):
        print('priorfact in _extractlogicblocks:')
        print(self._firstfact)
        self._logfile.write('in _extractlogicblocks:_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _extractlogicblocks:_firstfact:' + str(self._firstfact))
        print('conds in extractlogicblocks:' + conds)
        splited = conds.partition(' OR ')
        if ' OR ' not in splited:
            self.logicblocks.append(splited[0])
        else:
            print('left arg:' + splited[0])
            self.logicblocks.append(splited[0])
            print('right arg:' + splited[2])
            self._extractlogicblocks(splited[2])
            return self.logicblocks
        self._logfile.write('end _extractlogicblocks:_firstfact:' + str(self._firstfact) + '\n')

    def _extractfact(self, cond):
        self._logfile.write('in _extractfact:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _extractfact:--_firstfact:' + str(self._firstfact))
        self._fortesting.append('in _extractfact:--currentquestion:' + self.currentquestion)
        self._logfile.write('in _extractfact:--curr condition:' + cond + '\n')
        self._logfile.write('in _extractfact:--curr question:' + self.currentquestion+ '\n')
        # self._foundfacts = []
        self._fortesting.append('in _extractfact --start--cond:'+cond.__str__())
        self._logfile.write('in _extractfact --start--cond:'+cond.__str__() + '\n')
        splited = cond.partition(' AND ')
        if ' AND ' not in splited:
            self._fortesting.append('in _extractfact -- AND isnt in ')
            self._logfile.write('in _extractfact -- AND isnt in ' + '\n')
            if ' OR ' not in splited[0]:
                self._fortesting.append('in _extractfact -- OR isnt in ')
                self._logfile.write('in _extractfact -- OR isnt in ' + '\n')
                self._foundfacts.append(splited[0])
                self._fortesting.append('in _extractfact --fact added in OR:' + splited[0])
                self._logfile.write('in _extractfact --fact added in OR:' + splited[0] + '\n')
            else:
                splited = splited[0].partition(' OR ')
                self._foundfacts.append(splited[0])
                self._fortesting.append('in _extractfact --fact added in else OR:' + splited[0])
                self._logfile.write('in _extractfact --fact added in else OR:' + splited[0] + '\n')
                self._extractfact(splited[2])
        else:
            self._foundfacts.append(splited[0])
            self._fortesting.append('in _extractfact --fact added in else  AND:' + splited[0])
            self._logfile.write('in _extractfact --fact added in else  AND:' + splited[0]+ '\n')
            self._extractfact(splited[2])
        self._tempdata['foundfacts'] = self._foundfacts
        self._logfile.write('end _extractfact:--_firstfact:' + str(self._firstfact) + '\n')
        return  self._foundfacts

    def _makequestion(self, fact):
        self._logfile.write('in _makequestion:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _makequestion:--_firstfact:' + str(self._firstfact))
        self._fortesting.append('in _makequestion:--currentquestion:' + self.currentquestion)
        self._logfile.write('in _makequestion:--currquestion:' + self.currentquestion + '\n')
        purefact = fact
        if 'NOT ' in fact:
            purefact = fact.partition('NOT ')[2]
        try:
                # ************* modify here ***************
            obj = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=purefact)
            self._curr_fact = obj
                # ************* modify here ***************
            self.question_type = obj.fact_type
        except Facts.DoesNotExist:
            print(
                    "هیچ واقعیتی با مشخصات زیر یافت نگردید:" + "\n" + 'fact:' + fact + '\n' + 'currquest:' + self.currentquestion)
            raise Http404(
                    "هیچ واقعیتی با مشخصات زیر یافت نگردید:" + "\n" + 'fact:' + fact + '\n' + 'currquest:' + self.currentquestion)
        print(purefact, file=sys.stderr)
        print(self.question_type, file=sys.stderr)
        # modelpath = os.path.join(BASE_DIR, 'Contract_Assistant/static/ExpertSystem/resources')
        # normalizer = Normalizer()
        print('fac', file=sys.stderr)
        purefact = normalizer.normalize(purefact)
        print('normalized', file=sys.stderr)
        print(purefact, file=sys.stderr)
        # print(modelpath)
        # tagger = POSTagger(model=os.path.join(BASE_DIR,'static/ExpertSystem/resources', 'postagger.model'))
        print('tgd', file=sys.stderr)
        taggedfact = tagger.tag(word_tokenize(purefact))
        print('tagged:', file=sys.stderr)
        print(taggedfact, file=sys.stderr)
        # chunker = Chunker(model=os.path.join(modelpath, 'chunker.model'))
        # chunker = RuleBasedChunker()
        # parser = DependencyParser(tagger = tagger, lemmatizer=Lemmatizer())
        # chunked_fact = tree2list(chunker.parse(taggedfact))
        # parsed_fact = parser.parse(word_tokenize(purefact))
        # print('chunked_fact')
        # print(chunked_fact)
        if self.question_type == Facts._YESNO:
            if 'NOT ' in fact:
                #****** old method ******
                # if 'است' in purefact:
                    # mainfact = purefact.split('است')[0]
                    # self.currentquestion = 'آیا ' + mainfact + 'نیست ؟'

                # elif 'می باشد' in purefact:
                    # mainfact = purefact.split('می باشد')[0]
                    # self.currentquestion = 'آیا ' + mainfact + 'نمی باشد ؟'
                #****** old method ******
                #////////////////////////
                #****** new method ******
                print('*********new method*******', file=sys.stderr)
                # purefact = fact.split('NOT ')[1]
                last = taggedfact[len(taggedfact)-1]
                print(last, file=sys.stderr)
                print(last[0], file=sys.stderr)
                print(last[1], file=sys.stderr)
                print('pf:'+purefact, file=sys.stderr)
                if last[1] == 'V':
                    Verb = last[0].replace('_', ' ')
                    print('Verb:'+Verb, file=sys.stderr)
                    if Verb == 'است':
                        N_Verb = 'نیست'
                        print('N_Verb: '+N_Verb, file=sys.stderr)
                        mainfact = purefact.partition(Verb)[0]
                        print('mainfact: '+mainfact, file=sys.stderr)
                        self.currentquestion = 'آیا ' + mainfact + N_Verb+ '؟'
                    elif Verb == 'باشد':
                        priorlast = taggedfact[len(taggedfact)-2]
                        Verb = priorlast[0]
                        N_Verb = 'ن'+Verb
                        print('N_Verb: '+N_Verb, file=sys.stderr)
                        N_fact = purefact.replace(Verb, N_Verb)
                        print('N_fact: '+N_fact, file=sys.stderr)
                        self.currentquestion = 'آیا ' + N_fact+ '؟'
                    else:
                        # T_Verb= Verb.replace('u200c', ' ' )
                        N_Verb = 'ن'+Verb
                        print('N_Verb: '+N_Verb, file=sys.stderr)
                        splitedpuref = purefact.partition(Verb)
                        mainfact = splitedpuref[0]
                        print('spf0:'+str(splitedpuref[0]), file=sys.stderr)
                        print('spf1:'+str(splitedpuref[1]), file=sys.stderr)
                        print('spf2:'+str(splitedpuref[2]), file=sys.stderr)
                        print('mainfact: '+mainfact, file=sys.stderr)
                        self.currentquestion = 'آیا ' + mainfact + N_Verb+ '؟'
                #****** new method ******
            else:
                self.currentquestion = 'آیا ' + str(fact) + '؟'

        else:
            chunker = Chunker(model=os.path.join(modelpath, 'chunker.model'))
            # chunker = RuleBasedChunker()
            # parser = DependencyParser(tagger = tagger, lemmatizer=Lemmatizer())
            chunked_fact = tree2list(chunker.parse(taggedfact))
            # parsed_fact = (parser.parse(word_tokenize(purefact)))
            print(chunked_fact, file=sys.stderr)
            # parsed_fact.tree().pprint()
            self.currentquestion = ''
            ind =0
            totalcunks = len(chunked_fact)
            for chunk in chunked_fact:
                if chunk[1] == 'NP':
                    if chunked_fact[ind-1][1] =='PP' and chunked_fact[ind+1][1] =='VP':
                        self.currentquestion += 'چقدر '
                        self._calc_ceriteria_value(chunk[0])
                        print('chunk is '+chunk[0]+' but replaced with چقدر')
                    else:
                        self.currentquestion += chunk[0]
                        print(type(chunk[0]))
                elif  chunk[1] == 'PP' and (chunked_fact[ind-1][1] =='NP' or chunked_fact[ind-1][1] =='VP'):
                    self.currentquestion += chunk[0]
                elif chunk[1] == 'AJe' or chunk[1] == 'ADVP' or chunk[1] == 'NUM':
                    self.currentquestion += 'چقدر '
                    self._calc_ceriteria_value(chunk[0])
                    print('chunk is '+chunk[0]+' but replaced with چقدر')
                elif chunk[1] =='ADJP':
                    self._define_operand_type(chunk[0])
                elif chunk[1] == 'VP':
                    print('ind:'+str(ind))
                    print('tot:'+str(totalcunks))
                    if ind == totalcunks-1:
                        self.currentquestion += chunk[0]+'؟'
                    else:
                        self.currentquestion += chunk[0]
                ind += 1
                # self.currentquestion = chunked_fact


        self._questions.append(self.currentquestion)


        if self.currentstatus == 'beginsession':
            self.currentstatus = 'initial'
        else:
            self.currentstatus = 'in progress'
        print('*** In _makequestion status registered ***')
        self._logfile.write('*** In _makequestion status registered ***\n')
        self._registerstatus()
        self._fortesting.append('in _makequestion==>Current question=' + self.currentquestion)
        self._logfile.write('in _makequestion==>Current question=' + self.currentquestion + '\n')
        self._logfile.write('in _makequestion==> go to _whythisquestion\n')
        self._whythisquestion()
        self._logfile.write('in _makequestion==> return from _whythisquestion\n')
        self._logfile.write('whythis:' + self.whythis + '\n')
        self._logfile.write('in _makequestion==> waiting for user action\n')
        self._logfile.write('end _makequestion:--_firstfact:' + str(self._firstfact) + '\n')

    def _makequestion1(self,fact):
        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in evalanswer:--_firstfact:' + str(self._firstfact))
        self._fortesting.append('in _makequestion:--currentquestion:' + self.currentquestion)
        self._logfile.write('in _makequestion:--currquestion:' + self.currentquestion + '\n')
        purefact = fact
        if 'NOT ' in fact:
            purefact = fact.split('NOT ')[1]
            if 'است' in purefact:
                mainfact = purefact.split('است')[0]
                self.currentquestion = 'آیا ' + mainfact + 'نیست ؟'
            else:
                mainfact = purefact.split('می باشد')[0]
                self.currentquestion = 'آیا ' + mainfact + 'نمی باشد ؟'
        else:
            self.currentquestion ='آیا '+str(fact)+'؟'
        # with open(os.path.join(basepath, 'log.txt'), 'w+', 1, 'utf-8') as logfile:
        #     logfile.write(self.currentquestion+'****'+str(self._curr_rule)+'fact:'+fact)
        # self._tempdata['question'] = self.currentquestion
        self._questions.append(self.currentquestion)
        try:
            # ************* modify here ***************
            obj = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=purefact)
            # ************* modify here ***************
            self.question_type = obj.fact_type
        except Facts.DoesNotExist:
            print("هیچ واقعیتی با مشخصات زیر یافت نگردید:"+"\n"+'fact:'+fact+'\n'+'currquest:'+self.currentquestion)
            raise Http404("هیچ واقعیتی با مشخصات زیر یافت نگردید:"+"\n"+'fact:'+fact+'\n'+'currquest:'+self.currentquestion)
        # self.question_type = Facts.objects.filter(domain_id=self._knowledgeid).get(fact=fact).fact_type
        if self.currentstatus == 'beginsession':
            self.currentstatus = 'initial'
        else:
            self.currentstatus = 'in progress'
        print('*** In _makequestion status registered ***')
        self._logfile.write('*** In _makequestion status registered ***\n')
        self._registerstatus()
        self._fortesting.append('in _makequestion==>Current question='+self.currentquestion)
        self._logfile.write('in _makequestion==>Current question='+self.currentquestion + '\n')
        self._logfile.write('in _makequestion==> go to _whythisquestion\n')
        self._whythisquestion()
        self._logfile.write('in _makequestion==> return from _whythisquestion\n')
        self._logfile.write('whythis:' + self.whythis + '\n')
        self._logfile.write('in _makequestion==> waiting for user action\n')
        self._logfile.write('end _makequestion:--_firstfact:' + str(self._firstfact) + '\n')

    def evalanswer(self,ans):
        print('priorfact in evalanswer:')
        print(self._firstfact)
        basepath = os.path.join(os.getcwd(), 'Contract_Assistant/ExpertSystem/ModelsData')
        self._logfile = open(os.path.join(basepath, 'log.txt'), 'a+', 1, 'utf-8')
        self._logfile.write('in evalanswer==> **************action triggered by user***************\n')
        self._fortesting.append('in evalanswer:--_firstfact:' + str(self._firstfact))
        self._fortesting.append('in evalanswer:--currentquestion:' + self.currentquestion)
        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
        self._logfile.write('in evalanswer-start:--currquestion' + self.currentquestion + '\n')
        self._logfile.write('in evalanswer:--working memory is:' + str(self._workingmemory) + '\n')
        self._logfile.write('in evalanswer:--found fact is:' + str(self._foundfacts) + '\n')
        if self.question_type == self._YESNO :
            self._logfile.write('in evalanswer:--question_type == self._YESNO' + '\n')
            if ans == 'بلی':
                self._currfact_value = True
                if self._CONTINUE_SYSTEM_MSG in self.currentquestion :
                    self._workingmemory = []
                    self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    self._foundfacts = []
                    self.whythis = ''
                    self.howthis = ''
                    self.currentstatus = 'beginsession'
                    self._innerstatus = []
                    self._tempdata = {}
                    print('*** In evalanswer status registered ***')
                    self._logfile.write('*** In evalanswer status registered ***\n')
                    self._registerstatus()
                    self._logfile.write('*****************************************************************\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***          SYSTEM RESTARTED--NEW SESSION WILL BEGIN         ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('*****************************************************************\n')
                    self._logfile.write('***in evalanswer: goto choosenextrule(null)\n')
                    self._choosenextrule('null')
                    self._logfile.write('***in _makequestion: return from choosenextrule(null)\n')
                    # self._logfile.close()
                    return
                elif 'نیست' in self.currentquestion:
                    self._answers.append(self.currentquestion[4:len(self.currentquestion)-1])
                    self._tempdata['answer'] = self.currentquestion[4:len(self.currentquestion)-1]
                    mainfact = self.currentquestion.split('نیست')[0]
                    mainfact=mainfact+'است'
                    mainfact = mainfact[4:len(mainfact)]
                    factofans = 'NOT '+mainfact
                    print('here1')
                elif 'نمی باشد' in self.currentquestion:
                    self._answers.append(self.currentquestion[4:len(self.currentquestion) - 1])
                    self._tempdata['answer'] = self.currentquestion[4:len(self.currentquestion) - 1]
                    mainfact = self.currentquestion.split('نمی باشد')[0]
                    mainfact=mainfact+'می باشد'
                    mainfact = mainfact[4:len(mainfact)]
                    factofans = 'NOT '+mainfact
                    print('here2')
                else:
                    factofans = self.currentquestion[4:len(self.currentquestion)-1]
                    print('here3')
                print('excludelist before:')
                print(self._excluderules)
                for r in Rules.objects.filter(condition__icontains=factofans):
                    if r.pk in self._excluderules:
                        print('remove item from exclude list:'+str(r.pk))
                        self._excluderules.remove(r.pk)
                for r in Rules.objects.filter(condition__icontains=self._negativefact(factofans)):
                    if r.pk not in self._excluderules:
                        print('add item to exclude list:'+str(r.pk))
                        self._excluderules.append(r.pk)
                print('excludelist after:')
                print(self._excluderules)
                self._answers.append(factofans)
                self._tempdata['answer'] = factofans
                if factofans not in self._workingmemory:
                    self._workingmemory.append(factofans)
                    self._logfile.write('in evalanswer:--fact added to memory:' + factofans + '\n')
                    self._logfile.write('in evalanswer:--memory is:' + str(self._workingmemory) + '\n')
                    self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                # matchedrules = Rules.objects.filter(domain_id=self._knowledgeid).filter(condition__icontains=factofans)
                matchedrules = self._curr_rule
                # if True :  #len(matchedrules) :
                    # for rule in matchedrules:
                    #     self._curr_rule = rule
                self._tempdata['rul_id'] = matchedrules.id
                self._fortesting.append('in evalanswer--Yes:--evaluaterule now'  )
                self._logfile.write('in evalanswer--Yes:--evaluate rule now:'+str(self._curr_rule) + '\n')
                self._logfile.write('***in evalanswer: goto _evaluaterulelogic\n')
                self._evaluaterulelogic(matchedrules)
                self._logfile.write('***in evalanswer: return from _evaluaterulelogic\n')
            elif ans == 'خیر':
                self._currfact_value = False
                print('curq in NO:' + self.currentquestion)
                if self._CONTINUE_SYSTEM_MSG in self.currentquestion:
                    self.currentquestion = self._SESSION_FINISHED_MSG
                    self.question_type = 'Finished'
                    self.whythis = ''
                    self.howthis = ''
                    print('*** In evalanswer status registered ***')
                    self._logfile.write('*** In evalanswer status registered ***\n')
                    self._registerstatus()
                    self._logfile.close()
                    return
                elif 'نیست' in self.currentquestion:
                    self._answers.append(self.currentquestion[4:len(self.currentquestion) - 1])
                    self._tempdata['answer'] = self.currentquestion[4:len(self.currentquestion) - 1]
                    mainfact = self.currentquestion.split('نیست')[0]
                    mainfact=mainfact+'است'
                    mainfact = mainfact[4:len(mainfact)]
                    print('NOT '+mainfact)
                    for r in Rules.objects.filter(condition__icontains='NOT '+mainfact):
                        if r.pk not in self._excluderules:
                            print('add to exclude list1:' + r.__str__())
                            print('add to exclude list1:' + str(r.pk))
                            self._excluderules.append(r.pk)
                    print(self._excluderules)
                    if mainfact not in self._workingmemory:
                        self._workingmemory.append(mainfact)
                        self._logfile.write('in evalanswer:--fact added to memory:' + mainfact + '\n')
                        self._logfile.write('in evalanswer:--memory is:' + str(self._workingmemory) + '\n')
                        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    self._answers.append(mainfact)
                    self._tempdata['answer'] = mainfact
                    print('here4')
                elif 'نمی باشد' in self.currentquestion:
                    self._answers.append(self.currentquestion[4:len(self.currentquestion) - 1])
                    self._tempdata['answer'] = self.currentquestion[4:len(self.currentquestion) - 1]
                    mainfact = self.currentquestion.split('نمی باشد')[0]
                    mainfact=mainfact+'می باشد'
                    mainfact = mainfact[4:len(mainfact)]
                    print('NOT ' + mainfact)
                    for r in Rules.objects.filter(condition__icontains='NOT '+mainfact):
                        if r.pk not in self._excluderules:
                            print('add to exclude list2:' + r.__str__())
                            print('add to exclude list2:' + str(r.pk))
                            self._excluderules.append(r.pk)

                    if mainfact not in self._workingmemory:
                        self._workingmemory.append(mainfact)
                        self._logfile.write('in evalanswer:--fact added to memory:' + mainfact + '\n')
                        self._logfile.write('in evalanswer:--memory is:' + str(self._workingmemory) + '\n')
                        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    self._answers.append(mainfact)
                    self._tempdata['answer'] = mainfact
                    print('here5')
                else:
                    factofans = self.currentquestion[4:len(self.currentquestion) - 1]
                    print(factofans)
                    notfact = 'NOT '+factofans
                    if notfact not in self._workingmemory:
                        self._workingmemory.append(notfact)
                        self._logfile.write('in evalanswer:--fact added to memory:' + notfact + '\n')
                        self._logfile.write('in evalanswer:--workingmemory is:' + str(self._workingmemory) + '\n')
                        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    print('here6')
                    for r in Rules.objects.filter(condition__icontains=factofans):
                        # self._foundfacts = []
                        # rulefact = self._extractfact(r.condition)
                        mustexclude = True
                        # print(rulefact)
                        # ************ control here ************
                        # for f in rulefact:
                        #     if f in self._workingmemory:
                        #         mustexclude = False
                        #         print(f+ ' in memory.so not exlcude this rule:')
                        #         print(r)
                        #         break
                        # ************ control here ************
                        if r.pk not in self._excluderules and mustexclude:
                            print('add to exclude list3:' + r.__str__())
                            print('add to exclude list3:' + str(r.pk))
                            self._excluderules.append(r.pk)
                if self._enginemode != 'priorfact':
                    self._fortesting.append('in evalanswer:--No--' + 'choosenextrule(null)')
                    self._logfile.write('in evalanswer:--No--' + 'choosenextrule(null)' + '\n')
                    self._logfile.write('***in evalanswer: goto _choosenextrule(null)\n')
                    self._choosenextrule('null')
                    self._logfile.write('***in evalanswer: return from _choosenextrule(null)\n')
                    # self._choosenextrule('null')
                else:
                    if self._curr_rule.pk not in self._excluderules:
                        print('add to exclude list4:' + str(self._curr_rule))
                        print('add to exclude list4:' + str(self._curr_rule.pk))
                        self._excluderules.append(self._curr_rule.pk)
                    print('excluded rules:')
                    print(list(self._excluderules))
                    # ************* modify here ***************

                    print('in evalanswer:--No--' + 'try to found new rule contain current working memory')
                    tempquery = Rules.objects.filter(domain_id=self._knowledgeid).exclude(
                        pk__in=self._excluderules ).filter(self._qcondition(self._workingmemory))
                    # ************* modify here ***************
                    if(len(tempquery)):
                        self._curr_rule = tempquery[0]
                        # self.whythis = ''
                        print('in evalanswer:--No--' + 'found new rule--goto _evaluaterulelogic for a new rule contain current working memory')
                        self._logfile.write('in evalanswer:--No--' + 'goto _evaluaterule for a new rule contain current working memory' + '\n')
                        self._evaluaterulelogic(self._curr_rule)
                        self._logfile.write('***in evalanswer--No--: return from _evaluaterule\n')
                    else:
                        print('in evalanswer:--No--' + 'try to found new rule contain prior facts')
                        print('pf:')
                        print(self._firstfact)
                        newquery = Rules.objects.filter(domain_id=self._knowledgeid).exclude(
                        pk__in=self._excluderules ).filter( self._qcondition(self._firstfact))
                        if(len(newquery)):
                            print('in evalanswer:--No--' + 'found new rule--goto _evaluaterulelogic for a new rule contain prior facts')
                            self._curr_rule = newquery[0]
                            self.whythis = ''
                            self.howthis = ''
                            self._workingmemory = list.copy(self._firstfact)
                            self._foundfacts = []
                            self._logfile.write('in evalanswer:--No--' + 'goto _evaluaterulelogic for a new rule contain prior facts' + '\n')
                            self._evaluaterulelogic(self._curr_rule)
                            self._logfile.write('***in evalanswer--No--: return from _evaluaterulelogic\n')
                        else:
                            ans = 'TestNoRule'
                            print('***in evalanswer--No--: no matching rule found in working memory and prior facts---- go to "TestNoRule" condition')
                            print('pf:')
                            print(self._firstfact)
                            print(self._excluderules)
                            self._logfile.write('***in evalanswer--No--: no matching rule found in working memory and prior facts---- go to "TestNoRule" condition'+'\n')
                            self.evalanswer(ans)
            elif ans == 'TestFinish':
                if 'نیست' in self.currentquestion:
                    self._answers.append(self.currentquestion[4:len(self.currentquestion)-1])
                    self._tempdata['answer'] = self.currentquestion[4:len(self.currentquestion)-1]
                    mainfact = self.currentquestion.split('نیست')[0]
                    mainfact=mainfact+'است'
                    mainfact = mainfact[4:len(mainfact)]
                    factofans = 'NOT '+mainfact
                elif 'نمی باشد' in self.currentquestion:
                    self._answers.append(self.currentquestion[4:len(self.currentquestion) - 1])
                    self._tempdata['answer'] = self.currentquestion[4:len(self.currentquestion) - 1]
                    mainfact = self.currentquestion.split('نمی باشد')[0]
                    mainfact=mainfact+'می باشد'
                    mainfact = mainfact[4:len(mainfact)]
                    factofans = 'NOT '+mainfact
                else:
                    factofans = self.currentquestion[4:len(self.currentquestion)-1]

                self._tempdata['question'] = factofans
                matchedrules = Rules.objects.filter(domain_id=self._knowledgeid).filter(condition__icontains=factofans)
                if not len(matchedrules):
                    self._curr_rule = None
                    self._tempdata['rul_id'] = 0
                    self.question_type = 'Finished'
                    self.currentquestion = self._NO_MORE_RULES_MSG
                    self._tempdata['question'] = self.currentquestion

                else:
                    self._logfile.write('in evalanswer:--NOT FINISHED YET' + '\n')
            elif ans == 'TestNoRule':
                self.question_type = 'Finished'
                self._curr_rule = None
                self._tempdata['rul_id'] = 0
                lastresultinpersian = self._persianate(self._workingmemory[len(self._workingmemory)-1])
                # self.currentquestion = 'بر اساس اطلاعات وارده پاسخی وجود ندارد.'+'<br>'+ \
                #                        'آخرین نتیجه:'+lastresultinpersian+ self._CONTINUE_SYSTEM_MSG +'<br>'+\
                #                        'می توانید با برگشت به مراحل قبل و تغییر انتخاب ها مجددا تلاش نمایید.'
                self.currentquestion =self._NO_MORE_RULES_MSG+'<br>'
                if len(self._workingmemory):
                    self.currentquestion = self.currentquestion +'<strong>اطلاعاتی که کاربر تایید نموده است:'+'<br>'
                    for wm in self._workingmemory:
                        self.currentquestion+='* '+self._persianate(wm)+'<br>'
                self.currentquestion = self.currentquestion+ '<strong>آخرین نتیجه:'+lastresultinpersian+'</strong>'
                self._logfile.write('in evalanswer:--now go to _howthisanswer:' + '\n')
                self._howthisanswer()
                self._logfile.write('in evalanswer:--return from _howthisanswer:' + '\n')

                self._logfile.write('in evalanswer# in TestNoRule:--currentquestion:' +self.currentquestion+ '\n')
                self._tempdata['question'] = self.currentquestion
                print('*** in evalanswer# in TestNoRule status registered ***')
                self._logfile.write('*** in evalanswer# in TestNoRule status registered ***\n')
                self._registerstatus()

        else:       #*** question_type == self._EXP ***
            self._logfile.write('in evalanswer:--question_type == self._EXP' + '\n')
            print('here in EXP:')
            modelpath = os.path.join(os.getcwd(), 'Contract_Assistant/static/ExpertSystem/resources')
            factofans = self._curr_fact.fact
            if ans == 'TestFinish':
                self._logfile.write('in evalanswer:--_EXP:here in TestFinish' + '\n')
                factofans = self._curr_fact.fact
                    # ************* modify here ***************
                matchedrules = Rules.objects.filter(domain_id=self._knowledgeid).filter(condition__icontains=factofans)
                # ************* modify here ***************
                if not len(matchedrules):
                    self._curr_rule = None
                    self._tempdata['rul_id'] = 0
                    self.question_type = 'Finished'
                    self.currentquestion = factofans
                    self._curr_fact = None
                    self._operand_type = ''
                    self._ceriteria_value = 0
                    self._logfile.write('in evalanswer# in TestFinish:--currentquestion:' + self.currentquestion + '\n')
                    self._tempdata['question'] = self.currentquestion
                    return
                else:
                    self._logfile.write('in evalanswer:--NOT FINISHED YET' + '\n')
            elif ans == 'TestNoRule':
                self._logfile.write('in evalanswer:--_EXP:here in TestNoRule' + '\n')
                self.question_type = 'Finished'
                self._curr_rule = None
                self._tempdata['rul_id'] = 0
                self._curr_fact = None
                self._operand_type = ''
                self._ceriteria_value = 0
                lastresultinpersian = self._persianate(self._workingmemory[len(self._workingmemory)-1])
                # self.currentquestion = 'بر اساس اطلاعات وارده پاسخی وجود ندارد.'+'<br>'+ \
                #                        'آخرین نتیجه:'+lastresultinpersian+ self._CONTINUE_SYSTEM_MSG +'<br>'+\
                #                        'می توانید با برگشت به مراحل قبل و تغییر انتخاب ها مجددا تلاش نمایید.'
                self.currentquestion =self._NO_MORE_RULES_MSG+'<br>'
                if len(self._workingmemory):
                    self.currentquestion = self.currentquestion +'<strong>اطلاعاتی که کاربر تایید نموده است:'+'<br>'
                    for wm in self._workingmemory:
                        self.currentquestion +='* '+self._persianate(wm)+'<br>'
                    self.currentquestion+='</strong><br>'
                self.currentquestion = self.currentquestion+ '<strong>آخرین نتیجه:'+lastresultinpersian+'</strong>'
                self._logfile.write('in evalanswer:--now go to _howthisanswer:' + '\n')
                self._howthisanswer()
                self._logfile.write('in evalanswer:--return from _howthisanswer:' + '\n')

                self._logfile.write('in evalanswer# in TestNoRule:--currentquestion:' +self.currentquestion+ '\n')
                self._tempdata['question'] = self.currentquestion
                print('*** in evalanswer# in TestNoRule status registered ***')
                self._logfile.write('*** in evalanswer# in TestNoRule status registered ***\n')
                self._registerstatus()
            elif self._operand_type == '>=' or  self._operand_type == '>' :
                print('here in >=')
                self._logfile.write('in evalanswer:--_EXP:here in >=' + '\n')
                if self._CONTINUE_SYSTEM_MSG in self.currentquestion :
                    self._workingmemory = []
                    self._foundfacts = []
                    self.whythis = ''
                    self.howthis = ''
                    self.currentstatus = 'beginsession'
                    self._innerstatus = []
                    self._tempdata = {}
                    print('*** In evalanswer status registered ***')
                    self._logfile.write('*** In evalanswer status registered ***\n')
                    self._registerstatus()
                    self._logfile.write('*****************************************************************\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***          SYSTEM RESTARTED--NEW SESSION WILL BEGIN         ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('*****************************************************************\n')
                    self._logfile.write('***in evalanswer: goto choosenextrule(null)\n')
                    self._choosenextrule('null')
                    self._logfile.write('***in evalanswer: return from choosenextrule(null)\n')
                    # self._logfile.close()
                    return
                elif int(ans) >= self._ceriteria_value:    #*** fact is confirmed ****#
                    self._currfact_value = True
                    print('here in >= fact is confirmed: '+ans)
                    print('excludelist before:')
                    print(self._excluderules)
                    for r in Rules.objects.filter(condition__icontains=factofans):
                        if r.pk in self._excluderules:
                            print('remove item from exclude list:'+str(r.pk))
                            self._excluderules.remove(r.pk)
                    for r in Rules.objects.filter(condition__icontains=self._negativefact(factofans)):
                        if r.pk not in self._excluderules:
                            print('add item to exclude list:'+str(r.pk))
                            self._excluderules.append(r.pk)
                    print('excludelist after:')
                    print(self._excluderules)
                    self._answers.append(factofans)
                    self._tempdata['answer'] = factofans
                    if factofans not in self._workingmemory:
                        self._workingmemory.append(factofans)
                        self._logfile.write('in evalanswer:--fact added to memory:' + factofans + '\n')
                        self._logfile.write('in evalanswer:--memory is:' + str(self._workingmemory) + '\n')
                        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    # matchedrules = Rules.objects.filter(domain_id=self._knowledgeid).filter(condition__icontains=factofans)
                    matchedrules = self._curr_rule
                    # if True :  #len(matchedrules) :
                        # for rule in matchedrules:
                        #     self._curr_rule = rule
                    self._tempdata['rul_id'] = matchedrules.id
                    self._fortesting.append('in evalanswer--Yes:--evaluaterule now'  )
                    self._logfile.write('in evalanswer--Yes:--evaluate rule now:'+str(self._curr_rule) + '\n')
                    self._logfile.write('***in evalanswer: goto _evaluaterulelogic\n')
                    self._evaluaterulelogic(matchedrules)
                    self._logfile.write('***in evalanswer: return from _evaluaterulelogic\n')
                else:    #**** fact not confirmed ****#
                    self._currfact_value = False
                    print('here in >= fact not confirmed: '+ans)
                    print('curq in NO:' + self.currentquestion)
                    self._logfile.write('***in evalanswer:--_EXP:here in >=fact not confirmed' + '\n')
                    if self._CONTINUE_SYSTEM_MSG in self.currentquestion:
                        self.currentquestion = self._SESSION_FINISHED_MSG
                        self.question_type = 'Finished'
                        self.whythis = ''
                        self.howthis = ''
                        print('*** In evalanswer status registered ***')
                        self._logfile.write('*** In evalanswer status registered ***\n')
                        self._registerstatus()
                        self._logfile.close()
                        return
                    notfact = 'NOT '+factofans
                    self._answers.append(self._persianate(self._negativefact(factofans)))
                    if notfact not in self._workingmemory:
                        self._logfile.write('in evalanswer:--_EXP_BEFORE fact added to memory:' + str(self._firstfact) + '\n')
                        self._workingmemory.append(notfact)
                        self._logfile.write('in evalanswer:--_EXP_AFTER fact added to memory:' + str(self._firstfact) + '\n')
                        self._logfile.write('in evalanswer:--_EXP_fact added to memory:' + notfact + '\n')
                        self._logfile.write('in evalanswer:--_EXP_workingmemory is:' + str(self._workingmemory) + '\n')
                    # print('here6')
                    for r in Rules.objects.filter(condition__icontains=factofans):
                        # self._foundfacts = []
                        # rulefact = self._extractfact(r.condition)
                        mustexclude = True
                        # print(rulefact)
                        # ********* control here **********
                        # for f in rulefact:
                        #     if f in self._workingmemory:
                        #         mustexclude = False
                        #         print(f+ ' in memory.so not exlcude this rule:')
                        #         print(r)
                        #         break
                        if r.pk not in self._excluderules and mustexclude:
                            print('add to exclude list3:' + r.__str__())
                            print('add to exclude list3:' + str(r.pk))
                            self._excluderules.append(r.pk)
                        # ********* control here **********
                    if self._enginemode != 'priorfact':
                        self._fortesting.append('in evalanswer:--No--_EXP' + 'choosenextrule(null)')
                        self._logfile.write('in evalanswer:--No--_EXP' + 'choosenextrule(null)' + '\n')
                        self._logfile.write('***in evalanswer:--No--_EXP: goto _choosenextrule(null)\n')
                        self._choosenextrule('null')
                        self._logfile.write('***in evalanswer:--No--_EXP: return from _choosenextrule(null)\n')
                        # self._choosenextrule('null')
                    else:
                        if self._curr_rule.pk not in self._excluderules:
                            # print('add to exclude list4:' + str(self._curr_rule))
                            # print('add to exclude list4:' + str(self._curr_rule.pk))
                            self._excluderules.append(self._curr_rule.pk)
                        # print('excluded rules:')
                        print(list(self._excluderules))
                        # ************* modify here ***************
                        print('in evalanswer:--No--' + 'try to found new rule contain current working memory')
                        tempquery = Rules.objects.filter(domain_id=self._knowledgeid).exclude(
                            pk__in=self._excluderules ).filter(self._qcondition(self._workingmemory))
                        # ************* modify here ***************
                        if(len(tempquery)):
                            self._curr_rule = tempquery[0]
                            # self.whythis = ''
                            print('in evalanswer:--No--' + 'found new rule--goto _evaluaterulelogic for a new rule contain current working memory')
                            self._logfile.write('in evalanswer:--No--' + 'goto _evaluaterule for a new rule contain current working memory' + '\n')
                            self._evaluaterulelogic(self._curr_rule)
                            self._logfile.write('***in evalanswer:--No--_EXP: return from _evaluaterulelogic\n')
                        else:
                            print('in evalanswer:--No--' + 'try to found new rule contain prior facts')
                            newquery = Rules.objects.filter(domain_id=self._knowledgeid).exclude(
                            pk__in=self._excluderules ).filter( self._qcondition(self._firstfact))
                            if(len(newquery)):
                                print('in evalanswer:--No--' + 'found new rule--goto _evaluaterulelogic for a new rule contain prior facts')
                                self._curr_rule = newquery[0]
                                self.whythis = ''
                                self.howthis = ''
                                self._workingmemory = list.copy(self._firstfact)
                                self._foundfacts = []
                                self._logfile.write('in evalanswer:--No--' + 'goto _evaluaterulelogic for a new rule contain prior facts' + '\n')
                                self._evaluaterulelogic(self._curr_rule)
                                self._logfile.write('***in evalanswer--No--: return from _evaluaterulelogic\n')
                            else:
                                ans = 'TestNoRule'
                                print('pf:')
                                print(self._firstfact)
                                self._logfile.write('***in evalanswer--No--_EXP: no matching rule found---- go to "TestNoRule" condition\n')
                                self.evalanswer(ans)
            elif self._operand_type == '<=' or  self._operand_type =='<' :
                self._logfile.write('in evalanswer:--_EXP:here in <=' + '\n')
                print('here in <=')
                if self._CONTINUE_SYSTEM_MSG in self.currentquestion :
                    self._workingmemory = []
                    self._foundfacts = []
                    self.whythis = ''
                    self.howthis = ''
                    self.currentstatus = 'beginsession'
                    self._innerstatus = []
                    self._tempdata = {}
                    print('*** In evalanswer status registered ***')
                    self._logfile.write('*** In evalanswer status registered ***\n')
                    self._registerstatus()
                    self._logfile.write('*****************************************************************\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***          SYSTEM RESTARTED--NEW SESSION WILL BEGIN         ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('*****************************************************************\n')
                    self._logfile.write('***in evalanswer: goto choosenextrule(null)\n')
                    self._choosenextrule('null')
                    self._logfile.write('***in _makequestion: return from choosenextrule(null)\n')
                    # self._logfile.close()
                    return
                elif int(ans) <= self._ceriteria_value:    #*** fact is confirmed ****#
                    self._currfact_value = True
                    print('here in <= fact is confirmed: '+ans)
                    print('excludelist before:')
                    print(self._excluderules)
                    for r in Rules.objects.filter(condition__icontains=factofans):
                        if r.pk in self._excluderules:
                            print('remove item from exclude list:'+str(r.pk))
                            self._excluderules.remove(r.pk)
                    for r in Rules.objects.filter(condition__icontains=self._negativefact(factofans)):
                        if r.pk not in self._excluderules:
                            print('add item to exclude list:'+str(r.pk))
                            self._excluderules.append(r.pk)
                    print('excludelist after:')
                    print(self._excluderules)
                    self._answers.append(factofans)
                    self._tempdata['answer'] = factofans
                    if factofans not in self._workingmemory:
                        self._workingmemory.append(factofans)
                        self._logfile.write('in evalanswer:--fact added to memory:' + factofans + '\n')
                        self._logfile.write('in evalanswer:--memory is:' + str(self._workingmemory) + '\n')
                        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    # matchedrules = Rules.objects.filter(domain_id=self._knowledgeid).filter(condition__icontains=factofans)
                    matchedrules = self._curr_rule
                    # if True :  #len(matchedrules) :
                        # for rule in matchedrules:
                        #     self._curr_rule = rule
                    self._tempdata['rul_id'] = matchedrules.id
                    self._fortesting.append('in evalanswer--Yes:--evaluaterule now'  )
                    self._logfile.write('in evalanswer--Yes:--evaluate rule now:'+str(self._curr_rule) + '\n')
                    self._logfile.write('***in evalanswer: goto _evaluaterulelogic\n')
                    self._evaluaterulelogic(matchedrules)
                    self._logfile.write('***in evalanswer: return from _evaluaterulelogic\n')
                else:    #**** fact not confirmed ****#
                    self._currfact_value = False
                    print('here in <= fact not confirmed: '+ans)
                    print('curq in NO:' + self.currentquestion)
                    if self._CONTINUE_SYSTEM_MSG in self.currentquestion:
                        self.currentquestion = self._SESSION_FINISHED_MSG
                        self.question_type = 'Finished'
                        self.whythis = ''
                        self.howthis = ''
                        print('*** In evalanswer status registered ***')
                        self._logfile.write('*** In evalanswer status registered ***\n')
                        self._registerstatus()
                        self._logfile.close()
                        return
                    notfact = 'NOT '+factofans
                    self._answers.append(self._persianate(self._negativefact(factofans)))
                    if notfact not in self._workingmemory:
                        self._workingmemory.append(notfact)
                        self._logfile.write('in evalanswer:--fact added to memory:' + notfact + '\n')
                        self._logfile.write('in evalanswer:--workingmemory is:' + str(self._workingmemory) + '\n')
                        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    # print('here6')
                    for r in Rules.objects.filter(condition__icontains=factofans):
                        # self._foundfacts = []
                        # rulefact = self._extractfact(r.condition)
                        mustexclude = True
                        # print(rulefact)
                        # ********* control here **********
                        # for f in rulefact:
                        #     if f in self._workingmemory:
                        #         mustexclude = False
                        #         # print(f+ ' in memory.so not exlcude this rule:')
                        #         # print(r)
                        #         break
                        # ********* control here **********
                        if r.pk not in self._excluderules and mustexclude:
                            # print('add to exclude list3:' + r.__str__())
                            # print('add to exclude list3:' + str(r.pk))
                            self._excluderules.append(r.pk)
                    if self._enginemode != 'priorfact':
                        self._fortesting.append('in evalanswer:--No--' + 'choosenextrule(null)')
                        self._logfile.write('in evalanswer:--No--' + 'choosenextrule(null)' + '\n')
                        self._logfile.write('***in evalanswer: goto _choosenextrule(null)\n')
                        self._choosenextrule('null')
                        self._logfile.write('***in evalanswer: return from _choosenextrule(null)\n')
                        # self._choosenextrule('null')
                    else:
                        if self._curr_rule.pk not in self._excluderules:
                            # print('add to exclude list4:' + str(self._curr_rule))
                            # print('add to exclude list4:' + str(self._curr_rule.pk))
                            self._excluderules.append(self._curr_rule.pk)
                        # print('excluded rules:')
                        print(list(self._excluderules))
                        # ************* modify here ***************
                        print('in evalanswer:--No--' + 'try to found new rule contain current working memory')
                        tempquery = Rules.objects.filter(domain_id=self._knowledgeid).exclude(
                            pk__in=self._excluderules ).filter(self._qcondition(self._workingmemory))
                        # ************* modify here ***************
                        if(len(tempquery)):
                            self._curr_rule = tempquery[0]
                            # self.whythis = ''
                            print('in evalanswer:--No--' + 'found new rule--goto _evaluaterulelogic for a new rule contain current working memory')
                            self._logfile.write('in evalanswer:--No--' + 'goto _evaluaterulelogic for a new rule contain current working memory' + '\n')
                            self._evaluaterulelogic(self._curr_rule)
                            self._logfile.write('***in evalanswer--No--: return from _evaluaterulelogic\n')
                        else:
                            print('in evalanswer:--No--' + 'try to found new rule contain prior facts')
                            newquery = Rules.objects.filter(domain_id=self._knowledgeid).exclude(
                            pk__in=self._excluderules ).filter( self._qcondition(self._firstfact))
                            if(len(newquery)):
                                print('in evalanswer:--No--' + 'found new rule--goto _evaluaterulelogic for a new rule contain prior facts')
                                self._curr_rule = newquery[0]
                                self.whythis = ''
                                self.howthis = ''
                                self._workingmemory = list.copy(self._firstfact)
                                self._foundfacts = []
                                self._logfile.write('in evalanswer:--No--' + 'goto _evaluaterulelogic for a new rule contain prior facts' + '\n')
                                self._evaluaterulelogic(self._curr_rule)
                                self._logfile.write('***in evalanswer--No--: return from _evaluaterulelogic\n')
                            else:
                                ans = 'TestNoRule'
                                print('pf:')
                                print(self._firstfact)
                                self._logfile.write('***in evalanswer--No--: no matching rule found---- go to "TestNoRule" condition\n')
                                self.evalanswer(ans)
            elif self._operand_type == '=='  :
                self._logfile.write('in evalanswer:--_EXP:here in ==' + '\n')
                print('here in ==')
                if self._CONTINUE_SYSTEM_MSG in self.currentquestion :
                    self._workingmemory = []
                    self._foundfacts = []
                    self.whythis = ''
                    self.howthis = ''
                    self.currentstatus = 'beginsession'
                    self._innerstatus = []
                    self._tempdata = {}
                    print('*** In evalanswer status registered ***')
                    self._logfile.write('*** In evalanswer status registered ***\n')
                    self._registerstatus()
                    self._logfile.write('*****************************************************************\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***          SYSTEM RESTARTED--NEW SESSION WILL BEGIN         ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('***                                                           ***\n')
                    self._logfile.write('*****************************************************************\n')
                    self._logfile.write('***in evalanswer: goto choosenextrule(null)\n')
                    self._choosenextrule('null')
                    self._logfile.write('***in evalanswer: return from choosenextrule(null)\n')
                    # self._logfile.close()
                    return
                elif int(ans) == self._ceriteria_value:    #*** fact is confirmed ****#
                    self._currfact_value = True
                    print('here in == fact is confirmed: '+ans)
                    print('excludelist before:')
                    print(self._excluderules)
                    for r in Rules.objects.filter(condition__icontains=factofans):
                        if r.pk in self._excluderules:
                            print('remove item from exclude list:'+str(r.pk))
                            self._excluderules.remove(r.pk)
                    for r in Rules.objects.filter(condition__icontains=self._negativefact( factofans)):
                        if r.pk not in self._excluderules:
                            print('add item to exclude list:'+str(r.pk))
                            self._excluderules.append(r.pk)
                    print('excludelist after:')
                    print(self._excluderules)
                    self._answers.append(factofans)
                    self._tempdata['answer'] = factofans
                    if factofans not in self._workingmemory:
                        self._workingmemory.append(factofans)
                        self._logfile.write('in evalanswer:--fact added to memory:' + factofans + '\n')
                        self._logfile.write('in evalanswer:--memory is:' + str(self._workingmemory) + '\n')
                        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    # matchedrules = Rules.objects.filter(domain_id=self._knowledgeid).filter(condition__icontains=factofans)
                    matchedrules = self._curr_rule
                    # if True :  #len(matchedrules) :
                        # for rule in matchedrules:
                        #     self._curr_rule = rule
                    self._tempdata['rul_id'] = matchedrules.id
                    self._fortesting.append('in evalanswer--Yes:--evaluaterule now'  )
                    self._logfile.write('in evalanswer--Yes:--evaluate rule now:'+str(self._curr_rule) + '\n')
                    self._logfile.write('***in evalanswer: goto _evaluaterulelogic\n')
                    self._evaluaterulelogic(matchedrules)
                    self._logfile.write('***in evalanswer: return from _evaluaterulelogic\n')
                else:    #**** fact not confirmed ****#
                    self._currfact_value = False
                    print('here in == fact not confirmed: '+ans)
                    print('curq in NO:' + self.currentquestion)
                    if self._CONTINUE_SYSTEM_MSG in self.currentquestion:
                        self.currentquestion = self._SESSION_FINISHED_MSG
                        self.question_type = 'Finished'
                        self.whythis = ''
                        self.howthis = ''
                        print('*** In evalanswer status registered ***')
                        self._logfile.write('*** In evalanswer status registered ***\n')
                        self._registerstatus()
                        self._logfile.close()
                        return
                    notfact = 'NOT '+factofans
                    self._answers.append(self._persianate(self._negativefact(factofans)))
                    if notfact not in self._workingmemory:
                        self._workingmemory.append(notfact)
                        self._logfile.write('in evalanswer:--fact added to memory:' + notfact + '\n')
                        self._logfile.write('in evalanswer:--workingmemory is:' + str(self._workingmemory) + '\n')
                        self._logfile.write('in evalanswer:--_firstfact:' + str(self._firstfact) + '\n')
                    # print('here6')
                    for r in Rules.objects.filter(condition__icontains=factofans):
                        # self._foundfacts = []
                        # rulefact = self._extractfact(r.condition)
                        mustexclude = True
                        # print(rulefact)
                        # ********* control here **********
                        # for f in rulefact:
                        #     if f in self._workingmemory:
                        #         mustexclude = False
                        #         # print(f+ ' in memory.so not exlcude this rule:')
                        #         # print(r)
                        #         break
                        # ********* control here **********
                        if r.pk not in self._excluderules and mustexclude:
                            # print('add to exclude list3:' + r.__str__())
                            # print('add to exclude list3:' + str(r.pk))
                            self._excluderules.append(r.pk)
                    if self._enginemode != 'priorfact':
                        self._fortesting.append('in evalanswer:--No--' + 'choosenextrule(null)')
                        self._logfile.write('in evalanswer:--No--' + 'choosenextrule(null)' + '\n')
                        self._logfile.write('***in evalanswer: goto _choosenextrule(null)\n')
                        self._choosenextrule('null')
                        self._logfile.write('***in evalanswer: return from _choosenextrule(null)\n')
                        # self._choosenextrule('null')
                    else:
                        if self._curr_rule.pk not in self._excluderules:
                            # print('add to exclude list4:' + str(self._curr_rule))
                            # print('add to exclude list4:' + str(self._curr_rule.pk))
                            self._excluderules.append(self._curr_rule.pk)
                        # print('excluded rules:')
                        print(list(self._excluderules))
                        # ************* modify here ***************
                        print('in evalanswer:--No--' + 'try to found new rule contain current working memory')
                        tempquery = Rules.objects.filter(domain_id=self._knowledgeid).exclude(
                            pk__in=self._excluderules ).filter(self._qcondition(self._workingmemory))
                        # ************* modify here ***************
                        if(len(tempquery)):
                            self._curr_rule = tempquery[0]
                            # self.whythis = ''
                            print('in evalanswer:--No--' + 'found new rule--goto _evaluaterulelogic for a new rule contain current working memory')
                            self._logfile.write('in evalanswer:--No--' + 'goto _evaluaterulelogic for a new rule contain current working memory' + '\n')
                            self._evaluaterulelogic(self._curr_rule)
                            self._logfile.write('***in evalanswer--No--: return from _evaluaterulelogic\n')
                        else:
                            print('in evalanswer:--No--' + 'try to found new rule contain prior facts')
                            newquery = Rules.objects.filter(domain_id=self._knowledgeid).exclude(
                            pk__in=self._excluderules ).filter( self._qcondition(self._firstfact))
                            if(len(newquery)):
                                print('in evalanswer:--No--' + 'found new rule--goto _evaluaterulelogic for a new rule contain prior facts')
                                self._curr_rule = newquery[0]
                                self.whythis = ''
                                self.howthis = ''
                                self._workingmemory = list.copy(self._firstfact)
                                self._foundfacts = []
                                self._logfile.write('in evalanswer:--No--' + 'goto _evaluaterulelogic for a new rule contain prior facts' + '\n')
                                self._evaluaterulelogic(self._curr_rule)
                                self._logfile.write('***in evalanswer--No--: return from _evaluaterulelogic\n')
                            else:
                                ans = 'TestNoRule'
                                print('pf:')
                                print(self._firstfact)
                                self._logfile.write('***in evalanswer--No--: no matching rule found---- go to "TestNoRule" condition\n')
                                self.evalanswer(ans)
        self._logfile.write('end evalanswer:--_firstfact:' + str(self._firstfact) + '\n')

    def _persianate(self, conds):
        self._logfile.write('in _persianate:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _persianate:--_firstfact:' + str(self._firstfact))
        print('in _persianate:--_conds:' +str(conds))
        # conds is just a  fact
        if ('AND' not in conds) and ('OR' not in conds):
            if 'NOT ' in conds:
                partitioned = conds.partition('NOT ')
                purefact = partitioned[2]
                print('pf:'+purefact)
                print('pt:'+str(partitioned))
                taggedfact = tagger.tag(word_tokenize(purefact))
                print('taggedfact:')
                print(taggedfact)
                last = taggedfact[len(taggedfact)-1]
                print('last"')
                print(last)
                print(last[0])
                print(last[1])
                if last[1] == 'V':
                    Verb = last[0].replace('_', ' ')
                    if Verb == 'است':
                        N_Verb = 'نیست'
                        print('N_Verb: '+N_Verb)
                        mainfact = purefact.partition(Verb)[0]
                        print('mainfact: '+mainfact)
                        return mainfact + N_Verb
                    elif Verb == 'باشد':
                        priorlast = taggedfact[len(taggedfact)-2]
                        Verb = priorlast[0]
                        N_Verb = 'ن'+Verb
                        print('N_Verb: '+N_Verb)
                        N_fact = purefact.replace(Verb, N_Verb)
                        return N_fact
                    else:
                        N_Verb = 'ن'+Verb
                        print('N_Verb: '+N_Verb)
                        mainfact = purefact.partition(Verb)[0]
                        print('mainfact: '+mainfact)
                        return mainfact + N_Verb

                else:
                    return conds
            else:
                return conds
        else:
            tempres = ''
            splited = conds.partition(' AND ')
            if ' AND ' not in splited:
                if ' OR ' not in splited[0]:
                    tempres += to_persian(splited[0])
                    print('tempres:'+tempres)
                    return tempres
                else:
                    splited = splited[0].partition(' OR ')
                    tempres += to_persian(splited[0])
                    tempres += ' یا '
                    tempres += to_persian(splited[2])
                    print('tempres:'+tempres)
                    return tempres
            else:
                tempres += to_persian(splited[0])
                tempres += ' و '
                tempres += to_persian(splited[2])
                print('tempres:'+tempres)
                return  tempres
        self._logfile.write('end _persianate:--_firstfact:' + str(self._firstfact) + '\n')

    def _choosenextrule(self , cons):
        print('priorfact in _choosenextrule:')
        print(self._firstfact)
        self._logfile.write('in _choosenextrule:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _choosenextrule:--_firstfact:' + str(self._firstfact))
        self._fortesting.append('in _choosenextrule:--currentquestion:' + self.currentquestion)
        self._logfile.write('in _choosenextrule:--currquestion' + self.currentquestion + '\n')
        if len(self._fortesting) > 200:
            self._fortesting=[]
        if cons != 'null':
            # ************* modify here ***************
            matchedrules = Rules.objects.filter(domain_id=self._knowledgeid).filter(self._qcondition(cons))
            # ************* modify here ***************
            excludelist = []
            if not isinstance(cons, list):
                cons = [cons]

            for item in cons:
                excludelist.append(self._negativefact(item))
            if len(excludelist) and len(matchedrules):
                self._logfile.write('in _choosenextrule:--excludelist is: ' + str(excludelist) + '\n')
                matchedrules = matchedrules.exclude(self._qorcondition(excludelist))
        else:
            matchedrules=[]
        if len(matchedrules):
            rule = matchedrules[0]     #first matching rule selected
            # for rule in matchedrules:
            self._curr_rule = rule
            self._tempdata['rul_id'] =self._curr_rule.id
            self._fortesting.append('in _choosenextrule:--cons='+str(cons) + '--cur Rule='+rule.__str__() )
            self._logfile.write('in _choosenextrule:--cons='+str(cons) + '--cur Rule='+rule.__str__() + '\n')
            self._logfile.write('***in _choosenextrule: goto _evaluaterulelogic\n')
            self._evaluaterulelogic(rule)
            self._logfile.write('***in _choosenextrule: return from _evaluaterulelogic\n')
        else:
              # here we choose new rule and make new question to answer by user
            #choosenewrule
            #makequestion
            factlist = []
            self._fortesting.append('in _choosenextrule:--' + 'Rule null')
            self._logfile.write('in _choosenextrule:--' + 'Rule  is null' + '\n')
            if len(self._workingmemory) > 1:
                self._logfile.write('in _choosenextrule:--' + 'wm is :' + str(self._workingmemory) + '\n')
                wmrange = range(1, len(self._workingmemory))
                # here we store all combinations(2 item, 3 item , ...) of found facts in working memorry as below:
                # combinations(range(4), 3) –> (0,1,2), (0,1,3), (0,2,3), (1,2,3)
                # each item of list is a tuple
                for counter in wmrange:
                    factlist.append(list(combinations(self._workingmemory, counter+1)))
                # reverse combination list
                factlist.reverse()
                self._fortesting.append('in _choosenextrule:--' + 'factlist:'+str(factlist))
                self._logfile.write('in _choosenextrule:--' + 'factlist:'+str(factlist) + '\n')
                # for  all combinations(2 item, 3 item , ...)
                for x in range(0, factlist.__len__()):
                    # for each tuple of combinations
                    for y in range(0, factlist[x].__len__()):
                        # filter rules contain current facts:qcondition(factlist[x][y])
                        orderedrules = Rules.objects.filter(domain_id=self._knowledgeid).filter(self._qcondition(factlist[x][y]))  # <** correct here **
                        self._logfile.write(
                            'in _choosenextrule:-- num of rules is: ' + str(orderedrules.count()) + '\n')
                        # make temporary negative list facts of current combinations
                        excludelist = []
                        for item in factlist[x][y]:
                            excludelist.append(self._negativefact(item))
                        #here exlude rules contain any negative fact of current combinations
                        if len(excludelist):
                            self._logfile.write('in _choosenextrule:-- in for arule excludelist is: ' + str(excludelist) + '\n')
                            orderedrules = orderedrules.exclude(self._qorcondition(excludelist))
                        self._fortesting.append('in _choosenextrule:--' + 'in for x and y:' + str(factlist[x][y]))
                        self._logfile.write('in _choosenextrule:--' + 'in for x='+str(x) + ' and y='+str(y)+' fact is:\n'
                                            + str(factlist[x][y]) + '\n')
                        if len(orderedrules):
                            self._logfile.write(
                                'in _choosenextrule:-- num of rules after exclude is: ' + str(orderedrules.count()) + '\n')
                            rulecount = 0
                            for arule in orderedrules:
                                self._curr_rule = arule
                                self._foundfacts = []
                                self._extractfact(self._curr_rule.condition)
                                rulfacts = self._foundfacts
                                for fact in rulfacts:
                                    if self._negativefact(fact) in self._workingmemory:
                                        self._logfile.write('in _choosenextrule:--negative fact found' +str(fact)+ '\n')
                                        if rulecount < orderedrules.count()-1:
                                            self._curr_rule = orderedrules[rulecount+1]
                                            self._logfile.write(
                                                'in _choosenextrule:--after negativefact we get next ordered rule:'
                                                + str(self._curr_rule) + '\n')
                                            break
                                            # ************* modify here ***************
                                        elif Rules.objects.filter(domain_id=self._knowledgeid).filter(pk__gt=self._curr_rule.pk).exists():
                                            self._curr_rule = Rules.objects.filter(domain_id=self._knowledgeid).filter(pk__gt=self._curr_rule.pk)[0]
                                            # ************* modify here ***************
                                            self._logfile.write(
                                                'in _choosenextrule:--after negativefact we get next  rule in table:'
                                                + str(self._curr_rule) + '\n')
                                            # ************** new change*********** control to modify********
                                            self._workingmemory = []
                                            self.whythis = ''
                                            # ************** new change*********** control to modify********
                                        else:
                                            self.question_type = 'Finished'
                                            self.currentquestion = self._NO_MORE_RULES_MSG
                                            self._logfile.write(
                                                'in _choosenextrule:--after negativefact we finished' + '\n')
                                            self._logfile.write(
                                                'in _choosenextrule:--currentquestion:' + self.currentquestion + '\n')
                                            self._tempdata['question'] = self.currentquestion
                                            self._tempdata['workingmemory'] = self._workingmemory
                                            self._tempdata['answer'] = ''
                                            # self._innerstatus.append(self._tempdata.copy())
                                            # self._tempdata = {}
                                            print('inner in chosenext1:'+str(self._innerstatus))
                                            return
                                self._fortesting.append(
                                    'in _choosenextrule:--' + 'in for arule-no negative:' + self._curr_rule.condition)
                                self._logfile.write(
                                    'in _choosenextrule:--' + 'in for arule--currcondition-no negative:' + self._curr_rule.condition + '\n')
                                self._evaluaterule(self._curr_rule)
                                rulecount += 1
                                # self._evaluaterule(arule)
                                return
                self._fortesting.append('in _choosenextrule:--' + 'curr rule:' + self._curr_rule.condition)
                self._logfile.write('in _choosenextrule:--' + 'curr rule cond:' + self._curr_rule.condition + '\n')
                self._fortesting.append('in _choosenextrule:--' + 'in end and no rule found' )
                self._logfile.write('in _choosenextrule:--' + 'in end and no rule found' + '\n')
                # self.evalanswer('TestNoRule')
                # ************* modify here ***************
            if Rules.objects.filter(domain_id=self._knowledgeid).count() == 1:
                # ************* modify here ***************
                self.question_type = 'Finished'
                self._tempdata['rul_id'] = Rules.objects.filter(domain_id=self._knowledgeid).first().id
                # self.currentquestion = cons
                self.currentquestion = self._NO_MORE_RULES_MSG
                self._logfile.write('in _choosenextrule:--in Rules.objects.filter(domain_id=self._knowledgeid).count() == 1-currentquestion:' + self.currentquestion + '\n')
                self._tempdata['question'] = self.currentquestion
                self._tempdata['workingmemory'] = self._workingmemory
                self._tempdata['answer'] = ''
                # self._innerstatus.append(self._tempdata.copy())
                # self._tempdata = {}
                print('inner in chosenext2:' + str(self._innerstatus))
                # ************* modify here ***************
            elif Rules.objects.filter(domain_id=self._knowledgeid).filter(pk__gt=self._curr_rule.pk).exists() and self._enginemode != self._INITIAL_MODE_FACTS:
                # ************* modify here ***************
                # test
                if self._CONTINUE_SYSTEM_MSG not in self.currentquestion and cons != 'null':
                    self.currentquestion = cons+'. *** '+self._CONTINUE_SYSTEM_MSG
                    self._logfile.write('in _choosenextrule:--currentquestion:'+self.currentquestion + '\n')
                    self._logfile.write('in _choosenextrule:--now go to _howthisanswer:'  + '\n')
                    self._howthisanswer()
                    self._logfile.write('in _choosenextrule:--return from _howthisanswer:'  + '\n')

                    self._fortesting.append('we found answer:--'+self.currentquestion)
                    self._tempdata['question'] = self.currentquestion
                    self._tempdata['rul_id'] = self._curr_rule.id
                    self._tempdata['workingmemory'] = self._workingmemory
                    self._tempdata['answer'] = ''
                    # self._innerstatus.append(self._tempdata.copy())
                    # self._tempdata = {}
                    print('inner in chosenext3:' + str(self._innerstatus))
                    self._foundfacts=[]
                    return
                findnext = True
                self._curr_rule = Rules.objects.filter(domain_id=self._knowledgeid).filter(pk__gt=self._curr_rule.pk)[0]
                ind = 0
                while findnext:
                    # ************* modify here ***************
                    # self._curr_rule = Rules.objects.filter(domain_id=self._knowledgeid).filter(pk__gt=self._curr_rule.pk)[0]
                    # ************* modify here ***************
                    self._foundfacts=[]
                    self._logfile.write('***in _choosenextrule: goto _extractfact\n')
                    self._extractfact(self._curr_rule.condition)
                    self._logfile.write('***in _choosenextrule: return from _extractfact\n')
                    facts = self._foundfacts
                    for fact in facts:
                        if self._negativefact(fact) in self._workingmemory:
                            # ************* modify here ***************
                            if Rules.objects.filter(domain_id=self._knowledgeid).filter(pk__gt=self._curr_rule.pk).exists():
                                # ************* modify here ***************
                                ind = ind+1
                                self._curr_rule = \
                                Rules.objects.filter(domain_id=self._knowledgeid).filter(pk__gt=self._curr_rule.pk)[ind]
                                #****** new change******** control to modify******************
                                self._workingmemory = []
                                self.whythis = ''
                                # ****** new change******** control to modify******************
                                findnext = True
                                break
                            else:
                                self._fortesting.append('no no no')
                                self.question_type = 'Finished'
                                self.currentquestion = self._NO_MORE_RULES_MSG
                                self._logfile.write('in _choosenextrule:--currentquestion:' + self.currentquestion + '\n')
                                self._tempdata['question'] = self.currentquestion
                                self._tempdata['workingmemory'] = self._workingmemory
                                self._tempdata['answer'] = ''
                                # self._innerstatus.append(self._tempdata.copy())
                                # self._tempdata = {}
                                print('inner in chosenext4:' + str(self._innerstatus))
                                return
                        else:
                            findnext = False
                self._tempdata['rul_id'] = self._curr_rule.id
                self._fortesting.append('in _choosenextrule:--' + 'get new rule:' + self._curr_rule.condition)
                self._logfile.write('in _choosenextrule:--' + 'get new rule-cond is:' + self._curr_rule.condition + '\n')
                quest = self._curr_rule.condition
                if quest.strip():
                    self._evaluaterulelogic(self._curr_rule)
                    return
                else:
                    self.currentquestion = self._NO_QUESTION_MSG
                    self._logfile.write('in _choosenextrule:--currentquestion:' + self.currentquestion + '\n')
                    self._tempdata['question'] = self.currentquestion
                    self._tempdata['workingmemory'] = self._workingmemory
                    self._tempdata['answer'] = ''
                    # self._innerstatus.append(self._tempdata.copy())
                    # self._tempdata = {}
                    print('inner in chosenext5:' + str(self._innerstatus))
                    self._logfile.write('***in _choosenextrule: goto evalanswer\n')
                    self.evalanswer('TestFinish')
                    self._logfile.write('***in _choosenextrule: return from evalanswer\n')
            elif self._CONTINUE_SYSTEM_MSG not in self.currentquestion and cons != 'null' and self._enginemode != self._INITIAL_MODE_FACTS:
                  # self.question_type = 'Finished'
                  # if not cons.isalpha():
                  #     self.currentquestion = 'پاسخ سیستم:'+cons.fact+'. *** '+self._CONTINUE_SYSTEM_MSG
                  #     self._tempdata['question'] = self.currentquestion
                  # else:
                  self.currentquestion =  cons + '. *** ' + self._CONTINUE_SYSTEM_MSG
                  self._logfile.write('in _choosenextrule:--currentquestion:' + self.currentquestion + '\n')
                  self._logfile.write('in _choosenextrule:--now go to _howthisanswer:' + '\n')
                  self._howthisanswer()
                  self._logfile.write('in _choosenextrule:--return from _howthisanswer:' + '\n')
                  self._tempdata['question'] = self.currentquestion
                  self._tempdata['rul_id'] = self._curr_rule.id
                  self._tempdata['workingmemory'] = self._workingmemory
                  self._tempdata['answer'] = ''
                  # self._innerstatus.append(self._tempdata.copy())
                  # self._tempdata = {}
                  print('inner in chosenext6:' + str(self._innerstatus))
                  self._foundfacts = []
                  return
            else:
                self._fortesting.append('no no no:'+self._NO_MORE_RULES_MSG )
                self._logfile.write('no no no:'+self._NO_MORE_RULES_MSG + '\n')
                self.question_type = 'Finished'
                self.currentquestion = self._NO_MORE_RULES_MSG
                self._logfile.write('in _choosenextrule:--currentquestion:' + self.currentquestion + '\n')
                self._tempdata['question'] = self.currentquestion
                self._tempdata['workingmemory'] = self._workingmemory
                self._tempdata['answer'] = ''
        self._logfile.write('end _choosenextrule:--_firstfact:' + str(self._firstfact) + '\n')

    def _whythisquestion(self):
        print('priorfact in _whythisquestion:')
        print(self._firstfact)
        self._logfile.write('in _whythisquestion:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _whythisquestion:--_firstfact:' + str(self._firstfact))
        if self._curr_rule == None:
            return
        print(self._curr_rule)
        astr = ''
        remaincond = self._curr_rule.condition
        print('remaincond')
        print(remaincond)
        remaincond = self._persianate(remaincond)
        print('remaincond')
        print(remaincond)
        if self.whythis == '':
        # if len(self._workingmemory) == 0 :
            astr += ' اگر '
        else:
            astr = 'من می دانم' +' '
            print('curr condition:'+remaincond)
            for f in self._workingmemory:
                f = self._persianate(f)
                print('cur working memory item to add to whythis:'+f)
                astr += self._persianate(f)+' و '
                # remaincond = remaincond.replace(f+' AND ','')
                remaincond = remaincond.replace(f + ' و ', '')
                print('remaincond after removing:' + remaincond)
                remaincond = remaincond.replace( ' و '+f, '')
                print('remaincond after removing:' + remaincond)
                remaincond = remaincond.replace(f , '')
                print('remaincond after removing:'+remaincond)
                # remaincond = remaincond.replace(f+' OR ','')
                remaincond = remaincond.replace(f + ' یا ', '')
                print('remaincond after removing:' + remaincond)
                remaincond = remaincond.replace( ' یا '+f, '')
                print('remaincond after removing:' + remaincond)
                remaincond = remaincond.replace(f, '')
                print('remaincond after removing:' + remaincond)
            astr += 'بنابراین اگر '
        # astr += self._curr_rule.condition+' '
        # remaincond = self._extractfact(remaincond)
        # for f in remaincond:
        astr += self._persianate(remaincond)+' '
        astr += 'آنگاه  '
        astr += str(self._curr_rule.consequence)
        # astr = astr.replace('AND', 'و')
        self.whythis = astr
        self._fortesting.append('whythis:'+astr)
        # self.whythis = str(self.whythis)
        self._logfile.write('end _whythisquestion:--_firstfact:' + str(self._firstfact) + '\n')

    def _howthisanswer(self):
        print('priorfact in _howthisanswer:')
        print(self._firstfact)
        self._logfile.write('in _howthisanswer:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _howthisanswer:--_firstfact:' + str(self._firstfact))
        print(self._sessiondata)
        if self._curr_rule == None :
            return
        astr = 'چون '
        astr += self._persianate(self._curr_rule.condition) + ' '
        astr += 'پس  '
        astr += str(self._persianate(self._curr_rule.consequence.fact))
        if self.howthis == '':
            self.howthis = astr
        else:
            self.howthis += '<br>'+ astr
        self.howthis = self.howthis.replace('AND', 'و')
        print('now wt is:' + self.howthis)
        self._fortesting.append('howthis:' + self.howthis)
        self._logfile.write('in _howthisanswer==> ' + 'howthis:' + self.howthis + '\n')
        self._logfile.write('end _howthisanswer:--_firstfact:' + str(self._firstfact) + '\n')

    def _qcondition(self , facts):
        self._logfile.write('in _qcondition:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _qcondition:--_firstfact:' + str(self._firstfact))
        if not isinstance(facts, list):
            facts = [facts]
        output = Q('')
        for fact in facts:
            if output == Q(''):
                output = Q(condition__icontains=fact)

            else:
                output = output & Q(condition__icontains=fact)
        self._fortesting.append('in _qcondition:--' + 'output:' + output.__str__())
        self._logfile.write('in _qcondition:--' + 'output:' + output.__str__() + '\n')
        self._logfile.write('end _qcondition:--_firstfact:' + str(self._firstfact) + '\n')
        return output

    def _qorcondition(self, facts):
        self._logfile.write('in _qorcondition:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _qorcondition:--_firstfact:' + str(self._firstfact))
        if not isinstance(facts, list):
            facts = [facts]
        output = Q('')
        for fact in facts:
            if output == Q(''):
                output = Q(condition__icontains=fact)
            else:
                output = output | Q(condition__icontains=fact)
        self._fortesting.append('in _qorcondition:--' + 'output:' + output.__str__())
        self._logfile.write('in _qorcondition:--' + 'output:' + output.__str__() + '\n')
        self._logfile.write('end _qorcondition:--_firstfact:' + str(self._firstfact) + '\n')
        return output

    def _allstr(self,str):
        output = ''
        for item in str:
            if output == '':
                output = item.__str__()
            else:
                output = output+' AND '+item.__str__()
        self._fortesting.append('in _allstr:--' + 'output:' + output)
        self._logfile.write('in _allstr:--' + 'output:' + output + '\n')
        return output

    def _remainfact(self, bigger, lesser):
        self._logfile.write('in _remainfact:--_firstfact:' + str(self._firstfact) + '\n')
        self._logfile.write('in _remainfact:--' + 'lesser:' + str(lesser) + '\n')
        bigger = set(bigger)
        lesser = set(lesser)
        if bigger != lesser:
            self._logfile.write('in _remainfact:--' + 'remained:' + str(list(bigger-lesser)) + '\n')
            return list(bigger-lesser)
        else:
            self._logfile.write('in _remainfact:--' + 'remained:' + str(bigger) + '\n')
            return bigger

    def _negativefact(self, fact):
        self._logfile.write('in _negativefact:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in _negativefact:--_firstfact:' + str(self._firstfact))
        if  isinstance(fact, str):
            if 'NOT ' in fact:
                return fact.split('NOT ')[1]
            else:
                return 'NOT '+fact
        elif isinstance(fact, list):
            output = []
            for f in fact:
                if 'NOT ' in f:
                    output.append(f.split('NOT ')[1])
                else:
                    output.append('NOT '+f)
            return output

    def _registerstatus(self):
        print('priorfact in _registerstatus:')
        print(self._firstfact)
        self._logfile.write('in _registerstatus:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in evalanswer:--_firstfact:' + str(self._firstfact))
        if self._curr_rule:
            self._tempdata['rul_id'] = self._curr_rule.id
        else:
            self._tempdata['rul_id'] = 0
        self._tempdata['question'] = self.currentquestion
        self._tempdata['curr_fact'] = self._curr_fact
        self._tempdata['operandtype'] = self._operand_type
        self._tempdata['ceriteria_value'] = self._ceriteria_value
        self._tempdata['question_type'] = self.question_type
        self._tempdata['workingmemory'] = self._workingmemory
        self._tempdata['foundfacts'] = self._foundfacts
        self._tempdata['status'] = self.currentstatus
        self._tempdata['exclude_rules'] = self._excluderules
        # print('***'+self._tempdata['status'])
        if len(self._answers):
            self._tempdata['answer'] = self._answers[len(self._answers)-1]
        else:
            self._tempdata['answer'] = ''
        self._innerstatus.append(self._tempdata.copy())
        print('status registered')
        self._logfile.write('end _registerstatus:--_firstfact:' + str(self._firstfact) + '\n')

    def stepback(self):
        print('priorfact in stepback:')
        print(self._firstfact)
        basepath = os.path.join(os.getcwd(), 'Contract_Assistant/ExpertSystem/ModelsData')
        self._logfile = open(os.path.join(basepath, 'log.txt'), 'a+', 1, 'utf-8')
        self._logfile.write('in stepback:--_firstfact:' + str(self._firstfact) + '\n')
        self._fortesting.append('in stepback:--_firstfact:' + str(self._firstfact))
        innerlen = len(self._innerstatus)
        if  innerlen == 1:
            laststatus = self._innerstatus[0]
            # self._excluderules = []
        elif innerlen > 1 :
            print('len > 1')
            laststatus = self._innerstatus[len(self._innerstatus) - 2]
        else:
            print('no more step to rewind')
            return
        # print('inner in stepback:::'+str(self._innerstatus))
        print('innerlen:' + str(len(self._innerstatus)))
        print('inner in stepback:::')
        for x in self._innerstatus:
            print(str(x))
        print('inner in stepback:::')

        print('last:'+str(laststatus))
        if self._curr_rule != None:
            if laststatus['rul_id'] == self._curr_rule.id:
                self.howthis = ''
                print('how this cleared in stepback')
        if laststatus['rul_id'] != 0:
            # ************* modify here ***************
            self._curr_rule = Rules.objects.filter(domain_id=self._knowledgeid).get(pk=laststatus['rul_id'])
            # ************* modify here ***************
        else:
            self._curr_rule = None
        print('curruleid::' + str(laststatus['rul_id']))

        self.question_type = laststatus['question_type']
        print('question_type ='+self.question_type )

        self._curr_fact = laststatus['curr_fact']
        print('curr_fact:' + str(laststatus['curr_fact']))

        self._operand_type = laststatus['operandtype']
        print('operandtype:' + str(laststatus['operandtype']))

        self._ceriteria_value = laststatus['ceriteria_value']
        print('ceriteria_value::' + str(laststatus['ceriteria_value']))

        self.currentquestion = laststatus['question']
        print('currentquestion =' + self.currentquestion)

        self._workingmemory = laststatus['workingmemory']
        print('workingmemory =' + str(self._workingmemory))

        self._foundfacts = laststatus['foundfacts']
        print('_foundfacts =' + str(self._foundfacts))

        self.currentstatus = laststatus['status']
        print('currentstatus =' + str(self.currentstatus))

        self._excluderules = laststatus['exclude_rules']
        print('exclude_rules =' + str(self._excluderules))

        # basepath = os.path.join(os.getcwd(), 'Contract_Assistant/ExpertSystem/ModelsData')
        # self._logfile = open(os.path.join(basepath, 'log.txt'), 'a+', 1, 'utf-8')
        self._whythisquestion()
        print('howthis in stepback'+'\n')
        # self._howthisanswer()
        print(self.howthis)
        # if innerlen > 1:
        self._innerstatus.pop(innerlen-1)
        self._logfile.write('end stepback:--_firstfact:' + str(self._firstfact) + '\n')

    def _updatefacts(self, fact):
        if fact in self._foundfacts:
            self._foundfacts.remove(fact)
        print('ff:'+str(self._foundfacts))

    def _purefactfromquestion(self):
        if self._CONTINUE_SYSTEM_MSG in self.currentquestion:
            print(self.currentquestion[0:len(self.currentquestion) - len(self._CONTINUE_SYSTEM_MSG)])
            return self.currentquestion[0:len(self.currentquestion) - len(self._CONTINUE_SYSTEM_MSG)]
        elif 'نیست' in self.currentquestion:
            self._answers.remove(self.currentquestion[4:len(self.currentquestion) - 1])
            mainfact = self.currentquestion.split('نیست')[0]
            mainfact = mainfact + 'است'
            mainfact = mainfact[4:len(mainfact)]
            return 'NOT ' + mainfact
        elif 'نمی باشد' in self.currentquestion:
            self._answers.append(self.currentquestion[4:len(self.currentquestion) - 1])
            mainfact = self.currentquestion.split('نمی باشد')[0]
            mainfact = mainfact + 'می باشد'
            mainfact = mainfact[4:len(mainfact)]
            return 'NOT ' + mainfact
        else:
            print(self.currentquestion[4:len(self.currentquestion) - 1])
            return self.currentquestion[4:len(self.currentquestion) - 1]

    def _calc_ceriteria_value(self,token):
        val =''
        for item in token:
            if item.isdigit():
                val+= item
        self._ceriteria_value = int(val)
        print(self._ceriteria_value)

    def _define_operand_type(self,opr):
        normalizer = Normalizer()
        opr = normalizer.normalize(opr)
        if 'بیشتر یا مساوی ' in opr or 'بزرگتر یا مساوی' in opr:
            self._operand_type = '>='
        elif 'بیشتر' in opr or 'بزرگتر' in opr:
            self._operand_type = '>'
        elif 'کمتر یا مساوی' in opr or 'کوچکتر یا مساوی' in opr:
            self._operand_type = '<='
        elif 'کمتر' in opr or 'کوچکتر' in opr:
            self._operand_type = '<'
        elif 'معادل' in opr or 'برابر' in opr or 'مساوی' in opr:
            self._operand_type = '=='
        else:
            self._operand_type = '=='
        print(self._operand_type)


