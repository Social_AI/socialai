from django.apps import AppConfig


class ExpertSystemConfig(AppConfig):
    name = 'ExpertSystem'
