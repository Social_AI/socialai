from django.http import HttpResponse
from django.shortcuts import render, redirect,reverse
from RuleManager.models import Facts, Domain, Rules
from ExpertSystem.models import Comments
from django import forms
from .inferenceengineclass import inferenceengine
import jsonpickle
from django.contrib.auth.decorators import login_required
from django.db.models import Q
import json
from Contract_Assistant.utility import to_persian
# from hazm import *

# modelpath = os.path.join(os.getcwd(), 'Contract_Assistant/static/ExpertSystem/resources')
# normalizer = Normalizer()
# tagger = POSTagger(model=os.path.join(modelpath, 'postagger.model'))

global localexpert
rulefacts = []


class Form_contacts(forms.ModelForm):
    # Here we create a class that inherits from ModelForm.
    class Meta:
        model = Comments
        # We define the model that should be based on the form.
           # exclude = ('date_created', 'last_connexion',)
        fields = ('name', 'email','subject', )



# logger = logging.getLogger(__name__)

def getdatabasename(curruser):
    if curruser != 'amozesh' and curruser != 'demouser' and curruser != 'not_signed_in':
        domainfilter = Q(ownerexpert__appuser__user_auth__username=curruser)|Q(ownerexpert_id=1)
    else:
        domainfilter = Q(ownerexpert__appuser__user_auth__username=curruser)\
                   | Q(ownerexpert__appuser__user_auth__username='amozesh')\
                    | Q(ownerexpert__appuser__user_auth__username='demouser')|Q(ownerexpert_id=1)
    datalist = Domain.objects.filter(domainfilter)

    return datalist


# @login_required
def initengine(request):
    print('*************************************************** IN INITENGINE--START *************************************************** ')
    if len(request.POST) > 0:
        # print('in post')
        error = False
        factlist = []
        if 'inittype'in request.POST:
            inittype = request.POST.get('inittype', '')
            # print(inittype)
            if inittype == 'priorfact':
                if 'factlist-subject' in request.POST or 'factlist-detail[]' in request.POST:
                    subject = request.POST.get('factlist-subject', '')
                    if subject != '':
                        factlist.append( subject)
                    for fd in request.POST.getlist('factlist-detail[]', None):
                        factlist.append( fd)
                    print(factlist)
                if not len(factlist):
                    error = True
        else:
            error = True
            # print('initype not set')
            # ctypes.windll.user32.MessageBoxW(0, "Your text", "Your title", 0)
        if 'datalist' in request.POST:
            datalist = request.POST.get('datalist', '')
        else:
            error = True
        if not error:
            # print('not error')
            instance = inferenceengine(datalist, inittype, factlist)
            request.session['instance'] = jsonpickle.encode(instance)
            if request.user.is_authenticated:
                print('auth')
                exp_id = Domain.objects.get(id=datalist).ownerexpert_id
                print(exp_id)
                persiandetails = []
                for det in request.POST.getlist('factlist-detail[]', None):
                    persiandetails.append(to_persian(det))
                data = {'question': instance.currentquestion, 'question_type': instance.question_type
                    , 'whythisquestion': instance.whythis, 'howthisanswer': instance.howthis,
                        'step': instance.currentstatus, 'curruser': request.user.get_username(), 'exp_id':exp_id,
                        'exp_name':Domain.objects.get(id=datalist),'inittypedec':inittypedesc(inittype),
                        'subject':request.POST.get('factlist-subject', ''),
                        'detail':persiandetails}
                print('data to view in initengine:' + str(data))
                return render(request, 'ExpertSystem/expert_contract_byajax.html', data)
            else:
                # print('not auth')
                user='not_signed_in'
                request.session['redirect'] = 'ExpertSystem:expert_contract_byajax'
                # request.session['data'] = jsonpickle.encode(data)
                # print('datencd:'+request.session['data'])
                # print('ins:'+request.session['instance'])
                return redirect( reverse('signin'))
        else:
            # print('error')
            if request.user.is_authenticated:
                # print('auth')
                user=request.user
            else:
                # print('not auth')
                user='not_signed_in'
            # data = {'facts': Facts.objects.filter(fact_effect='COND').order_by('pk').reverse(),
            #         'databases': getdatabasename(user),'user':user}
            if user != 'amozesh' and user != 'demouser' and user != 'not_signed_in':
                factfilter = Q(ownerexpert__appuser__user_auth__username=user)
            else:
                factfilter = Q(domain__desc__icontains='پیمانکاران') \
                             | Q(ownerexpert__appuser__user_auth__username='demouser') | Q(
                    ownerexpert__appuser__user_auth__username='amozesh')

            print(user)
            newdata = {'subjectfacts': Facts.objects.filter(factfilter).filter(
                fact_effect='SUBJ').order_by('pk').reverse(),
                       'detailfacts': Facts.objects.filter(factfilter).filter(
                           fact_effect='COND').exclude(fact_effect='SUBJ').order_by('pk').reverse(),
                       'databases': getdatabasename(user), 'user': user}
            return render(request, 'ExpertSystem/newexpertsystem.html', newdata)

            # return render(request, 'ExpertSystem/expertsystem.html', data)
            # return render(request, 'ExpertSystem/test.html', data)
            # ctypes.windll.user32.MessageBoxW(0, "Your text", "Your title", 0)
    else:
        if request.user.is_authenticated:
            # user=request.user.get_username()
            user=request.user
        else:
            user='not_signed_in'
        if user!= 'amozesh' and user!= 'demouser' and user!= 'not_signed_in':
            factfilter = Q(ownerexpert__appuser__user_auth__username=user)|Q(ownerexpert_id=1)
        else:
            factfilter =  Q(domain__desc__icontains='پیمانکاران')\
                     | Q(ownerexpert__appuser__user_auth__username='demouser')\
                          | Q(ownerexpert__appuser__user_auth__username='amozesh')|Q(ownerexpert_id=1)
        # data = {'facts': Facts.objects.filter(factfilter).filter(
        #     fact_effect='COND').order_by('pk').reverse(),
        #         'databases': getdatabasename(user), 'user': user}
        # return render(request, 'ExpertSystem/expertsystem.html', data)
        newdata = {'subjectfacts': Facts.objects.filter(factfilter).filter(
            fact_effect='SUBJ').order_by('pk').reverse(),
                   'detailfacts': Facts.objects.filter(factfilter).filter(
                       fact_effect='COND').exclude(fact_effect='SUBJ').order_by('pk').reverse(),
                'databases': getdatabasename(user), 'user': user}
        return render(request, 'ExpertSystem/newexpertsystem.html', newdata)


def initengine_classic(request):
    print('*************************************************** IN CLASSIC INITENGINE--START *************************************************** ')
    if len(request.POST) > 0:
        # print('in post')
        error = False
        factlist = []
        if 'inittype'in request.POST:
            inittype = request.POST.get('inittype', '')
            print(inittype)
            if inittype == 'priorfact':
                if 'factlist-subject' in request.POST or 'factlist-detail[]' in request.POST:
                    subject = request.POST.get('factlist-subject', '')
                    if subject != '':
                        factlist.append( subject)
                    for fd in request.POST.getlist('factlist-detail[]', None):
                        factlist.append( fd)
                    print(factlist)
                if not len(factlist):
                    error = True
        else:
            error = True
            # print('initype not set')
            # ctypes.windll.user32.MessageBoxW(0, "Your text", "Your title", 0)
        if 'datalist' in request.POST:
            datalist = request.POST.get('datalist', '')
        else:
            error = True
        if not error:
            # print('not error')
            instance = inferenceengine(datalist, inittype, factlist)
            request.session['instance'] = jsonpickle.encode(instance)
            if request.user.is_authenticated:
                print('auth')
                exp_id = Domain.objects.get(id=datalist).ownerexpert_id
                print(exp_id)
                persiandetails = []
                for det in request.POST.getlist('factlist-detail[]', None):
                    persiandetails.append(to_persian(det))
                data = {'question': instance.currentquestion, 'question_type': instance.question_type
                    , 'whythisquestion': instance.whythis, 'howthisanswer': instance.howthis,
                        'step': instance.currentstatus, 'curruser': request.user.get_username(), 'exp_id':exp_id,
                        'exp_name':Domain.objects.get(id=datalist),'inittypedec':inittypedesc(inittype),
                        'subject':request.POST.get('factlist-subject', ''),
                        'detail':persiandetails}
                print('data to view in initengine:' + str(data))
                return render(request, 'ExpertSystem/expert_contract_byajax.html', data)
            else:
                # print('not auth')
                user='not_signed_in'
                request.session['redirect'] = 'ExpertSystem:expert_contract_byajax'
                # request.session['data'] = jsonpickle.encode(data)
                # print('datencd:'+request.session['data'])
                # print('ins:'+request.session['instance'])
                return redirect( reverse('signin'))
        else:
            # print('error')
            if request.user.is_authenticated:
                # print('auth')
                user=request.user
            else:
                # print('not auth')
                user='not_signed_in'
            # data = {'facts': Facts.objects.filter(fact_effect='COND').order_by('pk').reverse(),
            #         'databases': getdatabasename(user),'user':user}
            if user != 'amozesh' and user != 'demouser' and user != 'not_signed_in':
                factfilter = Q(ownerexpert__appuser__user_auth__username=user)|Q(ownerexpert_id=1)
            else:
                factfilter = Q(domain__desc__icontains='پیمانکاران') \
                             | Q(ownerexpert__appuser__user_auth__username='demouser') | Q(
                    ownerexpert__appuser__user_auth__username='amozesh')|Q(ownerexpert_id=1)

            print(user)
            newdata = {'subjectfacts': Facts.objects.filter(factfilter).filter(
                fact_effect='SUBJ').order_by('pk').reverse(),
                       'detailfacts': Facts.objects.filter(factfilter).filter(
                           fact_effect='COND').exclude(fact_effect='SUBJ').order_by('pk').reverse(),
                       'databases': getdatabasename(user), 'user': user}
            return render(request, 'ExpertSystem/classic_expertsystem.html', newdata)

            # return render(request, 'ExpertSystem/classic_expertsystem.html', data)
            # return render(request, 'ExpertSystem/test.html', data)
            # ctypes.windll.user32.MessageBoxW(0, "Your text", "Your title", 0)
    else:
        if request.user.is_authenticated:
            user=request.user.get_username()
        else:
            user='not_signed_in'
        if user!= 'amozesh' and user!= 'demouser' and user!= 'not_signed_in':
            factfilter = Q(ownerexpert__appuser__user_auth__username=user)|Q(ownerexpert_id=1)
        else:
            factfilter =  Q(domain__desc__icontains='پیمانکاران')\
                          | Q(ownerexpert__appuser__user_auth__username='demouser')\
                          | Q(ownerexpert__appuser__user_auth__username='amozesh')|Q(ownerexpert_id=1)
        newdata = {'subjectfacts': Facts.objects.filter(factfilter).filter(
                        fact_effect='SUBJ').order_by('pk').reverse(),
                   'detailfacts': Facts.objects.filter(factfilter).filter(
                       fact_effect='COND').exclude(fact_effect='SUBJ').order_by('pk').reverse(),
                   'databases': getdatabasename(user), 'user': request.user}
        return render(request, 'ExpertSystem/classic_expertsystem.html', newdata)


def inittypedesc(inival):
    if inival == 'priorfact':
        return 'انتخاب اطلاعات از پایگاه حقایق اولیه'
    elif inival =='first':
        return 'از اولین قانون پایگاه داده'
    else:
        return 'انتخاب بصورت تصادفی'


@login_required
def Creat_Expert(request ):
    if len(request.POST) > 0:
        # print('in posr- ce')
        if 'answer'in request.POST:
            # print('in posr- ce--answer')
            answer = request.POST.get('answer', '')
            if 'instance' in request.session:
                expertinstance = jsonpickle.decode(request.session['instance'])
            else:
                expertinstance = inferenceengine()
            expertinstance.evalanswer(answer)
            request.session['instance'] = jsonpickle.encode(expertinstance)
            data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': expertinstance.whythis, 'howthisanswer': expertinstance.howthis, 'step': expertinstance.currentstatus}
            # print('1')
            return render(request, 'ExpertSystem/expert_contract.html', data)
            # return HttpResponse('with answer')
        elif 'ynselect'in request.POST:
            # print('in posr- ce--yn')
            answer = request.POST.get('ynselect','')
            if 'instance' in request.session:
                expertinstance = jsonpickle.decode(request.session['instance'])
            else:
                expertinstance = inferenceengine()
            expertinstance.evalanswer(answer)
            request.session['instance'] = jsonpickle.encode(expertinstance)
            data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': expertinstance.whythis, 'howthisanswer': expertinstance.howthis, 'step': expertinstance.currentstatus}
            if expertinstance.whythis == '' :
                data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': 'اطلاعاتی برای نمایش وجود ندارد', 'howthisanswer': expertinstance.howthis}
            if expertinstance.howthis == '':
                data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': expertinstance.whythis, 'howthisanswer': 'اطلاعاتی برای نمایش وجود ندارد', 'step': expertinstance.currentstatus}
            # print('2')
            return render(request, 'ExpertSystem/expert_contract.html', data)
    else:
        if 'instance' in request.session:
            # expertinstance = request.session['instance']
            expertinstance = jsonpickle.decode(request.session['instance'])
            logger.warning('returend from instance')
            print('GET INSTANCE')
        else:
            expertinstance = inferenceengine()
            logger.warning('returend from instance')
            print('GET')
            # data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
            #     , 'whythisquestion': expertinstance.whythis, 'howthisanswer': expertinstance.howthis}
        if request.user.is_authenticated:
            print('auth')
            user=request.user.get_username()
        else:
            print('not auth')
            user='not_signed_in'
        data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': 'ایراد در پردازش اطلاعات',
                    'howthisanswer': 'ایراد در پردازش اطلاعات', 'step': expertinstance.currentstatus,'user':user}
        return render(request, 'ExpertSystem/expert_contract.html',data)

def contact(request):
    if len(request.POST) > 0:
        form = Form_contacts(request.POST)
        if form.is_valid():
            form.save(commit=True)
            data={'msg':'ثبت پیام با موفقیت انجام گردید','state':'success'}
            return render(request, 'ExpertSystem/contact.html',data)
        else:
            form = Form_contacts()
            data={'msg':'اطلاعات بصورت صحیح وارد نشده است. لطفا مجددا تلاش نمایید','state':'failur'}
            return render(request, 'ExpertSystem/contact.html',data)
    else:
        form = Form_contacts()
        if request.user.is_authenticated:
            print('auth')
            user=request.user
        else:
            print('not auth')
            user='not_signed_in'
        data={'msg':'پیام خود را ارسال نمایید','state':'info','user':user}
        return render(request, 'ExpertSystem/contact.html',data)

def extractfacts( conds):
    print('conds in extf:'+conds)
    splited = conds.partition(' AND ')
    if ' AND ' not in splited:
        if ' OR ' not in splited[0]:
            print('here1:'+splited[0])
            # splited = splited[0].partition('NOT ')
            if 'NOT ' not in splited[0]:
                print('here added to rulefacts 2:' + splited[0])
                # rulefacts.append(splited[0])
                rulefacts.append(splited[0])
                return splited[0]
            else:
                print('here we can added to rulefacts 3:' + splited[2])
                # rulefacts.append(splited[2])
                print('***but we  added NOT to rulefacts 3***:' + splited[0])
                rulefacts.append(splited[0])
                return splited[0]
            # print('here tempres 1:' + splited[0])
        else:
            splited = splited[0].partition(' OR ')
            print('here4:' + splited[0])
            rulefacts.append( splited[0])
            print('here tempres 5:' + splited[2])
            rulefacts.append(extractfacts( splited[2]))

    else:
        print('here6:' + splited[0])
        rulefacts.append( splited[0])
        print('here tempres 7:' + splited[2])
        extractfacts( splited[2])
        return rulefacts

def update_detailfacts(request):
    print('*************** IN update_detailfacts *********************')
    if len(request.POST):
        domain = request.POST.get('domainid', '')
        if domain == 'null' or domain =='':
            domain = 1
        # print('domain:'+domain, file=sys.stderr)
        # domain = 1
        domain = int(domain)
        subject = request.POST.get('subject_fact', '')
        #print('subject:'+subject, file=sys.stderr)

        details_related_to_subject = []
        currusername = request.user.get_username
        if currusername == 'demouser' or currusername == 'amozesh' or domain ==1:
            factfilter = Q(domain__desc__icontains='پیمانکاران') \
                     | Q(ownerexpert__appuser__user_auth__username='demouser') \
                     | Q(ownerexpert__appuser__user_auth__username='amozesh') | Q(ownerexpert_id=1)
        else:
            factfilter = Q(ownerexpert__appuser__user_auth=request.user) & Q(domain_id=domain)
        if subject == 'all_detail' :
            #print('all_detail', file=sys.stderr)
            alldetailfacts = Facts.objects.filter(factfilter).\
                           filter(fact_effect='COND').exclude(fact_effect='SUBJ').order_by('pk').reverse()
            print(alldetailfacts.values('fact'))
            print(type(alldetailfacts.values('fact')))
            for value in list(alldetailfacts.values('fact')):
                #print(type (value), file=sys.stderr)
                #print(value, file=sys.stderr)
                item = {'fact':value['fact'], 'persian_fact':value['fact']}
                details_related_to_subject.append(item)
                #print('item: '+str(item)+' added.', file=sys.stderr)
                persian_item = {'fact':'NOT '+value['fact'], 'persian_fact':to_persian('NOT '+value['fact'])}
                details_related_to_subject.append(persian_item)
                #print('item: '+str(persian_item)+' added.', file=sys.stderr)
                #print('after adding details_related_to_subject now is:', file=sys.stderr)
                #print(details_related_to_subject, file=sys.stderr)
            #print('final:'+str(details_related_to_subject), file=sys.stderr)
            #print(alldetailfacts, file=sys.stderr)
            data = json.dumps(details_related_to_subject, ensure_ascii=False)
            return HttpResponse(data, content_type="text/json-comment-filtered")
        else:
            #print('related_detail', file=sys.stderr)
            for  rule in  Rules.objects.filter(factfilter & Q(condition__icontains=subject)):
                rulefacts.clear()
                #print(rule.condition, file=sys.stderr)
                allfacts = extractfacts( rule.condition)
                if not isinstance(allfacts, list):
                    allfacts = [allfacts]
                #print('all f:'+str(allfacts), file=sys.stderr)
                details = remained_facts(allfacts, subject)
                #print('remdet:'+str(details), file=sys.stderr)

                for detail in details:
                    duplicated = False
                    # persian_detail = to_persian(detail)
                    # if persian_detail not in details_related_to_subject:
                        # details_related_to_subject.append(persian_detail)
                    persian_item = {'fact':detail, 'persian_fact':to_persian(detail)}
                    #print('item: '+str(detail), file=sys.stderr)
                    #print(details_related_to_subject, file=sys.stderr)
                    #print('persian_item:', file=sys.stderr)
                    #print(persian_item, file=sys.stderr)
                    #print('type of persian_item:'+str(type(persian_item)), file=sys.stderr)
                    for listitem in details_related_to_subject:
                        if detail == listitem['fact']:
                            duplicated = True
                            #print('duplicated in: '+str(listitem), file=sys.stderr)
                            break
                    if not duplicated:
                        # details_related_to_subject.append(detail)
                        #print('detail added to list', file=sys.stderr)
                        details_related_to_subject.append(persian_item)
                    else:
                        # print('duplicate', file=sys.stderr)
                        pass
                #print('*** HERE RELATED ITEMS***:'+str(details_related_to_subject), file=sys.stderr)
            # print('final:'+str(details_related_to_subject), file=sys.stderr)
            data = json.dumps(details_related_to_subject,ensure_ascii=False)
            # print('data in updat:')
            # print(data)
            return HttpResponse(data, content_type="text/json-comment-filtered")
    else:
        return HttpResponse('not valid request')


def remained_facts( bigger, lesser):
    if not isinstance(lesser, list):
        lesser = [lesser]
    print('in _remainfact:--' + 'bigger:' + str(bigger))
    print('in _remainfact:--' + 'lesser:' + str(lesser) )
    bigger = set(bigger)
    lesser = set(lesser)
    if bigger != lesser:
        print('in _remainfact:--' + 'remained:' + str(list(bigger-lesser)) )
        return list(bigger-lesser)
    elif bigger == lesser:
        print('in _remainfact:--' + 'remained:' + str(list(bigger-lesser)) )
        return list(bigger-lesser)

# def to_persian( conds):
#     # conds is just a  fact
#     if ('AND' not in conds) and ('OR' not in conds):
#         if 'NOT ' in conds:
#             purefact = conds.split('NOT ')[1]
#             taggedfact = tagger.tag(word_tokenize(purefact))
#             print('taggedfact:')
#             print(taggedfact)
#             last = taggedfact[len(taggedfact)-1]
#             print('last"')
#             print(last)
#             print(last[0])
#             print(last[1])
#             if last[1] == 'V':
#                 Verb = last[0].replace('_', ' ')
#                 if Verb != 'است':
#                     N_Verb = 'ن'+Verb
#                     print('N_Verb: '+N_Verb)
#                     mainfact = purefact.split(Verb)[0]
#                     print('mainfact: '+mainfact)
#                     return mainfact + N_Verb
#                 elif Verb == 'است':
#                     N_Verb = 'نیست'
#                     print('N_Verb: '+N_Verb)
#                     mainfact = purefact.split(Verb)[0]
#                     print('mainfact: '+mainfact)
#                     return mainfact + N_Verb
#                 elif Verb == 'باشد':
#                     priorlast = taggedfact[len(taggedfact)-2]
#                     Verb = priorlast[0]
#                     N_Verb = 'ن'+Verb
#                     print('N_Verb: '+N_Verb)
#                     N_fact = purefact.replace(Verb, N_Verb)
#                     return N_fact
#             else:
#                 return conds
#         else:
#             return conds

#             # if 'است' in purefact:
#             #     mainfact = purefact.split('است')[0]
#             #     return mainfact +'نیست'
#             # else:
#             #     mainfact = purefact.split('می باشد')[0]
#             #     return  mainfact +'نمی باشد'
#         # else:
#         #         return conds
#     else:
#         tempres = ''
#         splited = conds.partition(' AND ')
#         if ' AND ' not in splited:
#             if ' OR ' not in splited[0]:
#                 tempres += to_persian(splited[0])
#                 return tempres
#             else:
#                 splited = splited[0].partition(' OR ')
#                 tempres += to_persian(splited[0])
#                 tempres += ' یا '
#                 tempres += to_persian(splited[2])
#                 return tempres
#         else:
#             tempres += to_persian(splited[0])
#             tempres += ' و '
#             tempres += to_persian(splited[2])
#             return  tempres


