from django.contrib import admin

from .models import Comments, Expertmodel

admin.site.register(Comments)
admin.site.register(Expertmodel)