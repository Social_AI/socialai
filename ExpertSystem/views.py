from django.views.decorators.csrf import csrf_exempt
from RuleManager.models import Facts, Rules
from ServiceUser.models import Subscription
from .inferenceengineclass import inferenceengine
import os, csv,jsonpickle,json
from django.http import JsonResponse, HttpResponse
from django.template import loader
from django.shortcuts import render
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.db.models import Q


user_login_required = user_passes_test(lambda user: user.is_staff)
def staff_user_required(view_func):
    decorated_view_func = login_required(user_login_required(view_func))
    return decorated_view_func

@csrf_exempt
def update_facts(request):
    if len(request.POST):
        db_id = request.POST.get('db_id', '')
        facts = Facts.objects.filter(domain_id=db_id).filter(fact_effect=Facts._CONDITION).all()
        data = json.dumps(list(facts.values('fact', 'id')),ensure_ascii=False)
        # print('data in updat:')
        # print(data)
        return HttpResponse(data, content_type="text/json-comment-filtered")

    else:
        return HttpResponse('not valid request')

def extractfact(cond):
    foundfacts = []
    splited = cond.partition(' AND ')
    if (' AND ' not in splited):
        if (' OR ' not in splited):
            foundfacts.append(splited[0])
        else:
            foundfacts.append(splited[0])
            extractfact(splited[2])
    else:
        foundfacts.append(splited[0])
        extractfact(splited[2])
    return foundfacts


@login_required
def expert_contract_byajax(request):
    if len(request.POST):
        print('in expert_contract_byajax--post')
        if 'userinput'in request.POST:
            # print('in expert_contract_byajax--post--userinput in request.POST')
            # print('in posr- ce--answer')
            answer = request.POST.get('userinput', '')
            # print('in expert_contract_byajax--post--userinput in request.POST--ans is=='+answer)
            if 'instance' in request.session:
                expertinstance = jsonpickle.decode(request.session['instance'])
            else:
                expertinstance = inferenceengine()
            expertinstance.evalanswer(answer)
            request.session['instance'] = jsonpickle.encode(expertinstance)

            data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': expertinstance.whythis, 'howthisanswer': expertinstance.howthis, 'step': expertinstance.currentstatus}
            if expertinstance.whythis == '' :
                data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': 'اطلاعاتی برای نمایش وجود ندارد', 'howthisanswer': expertinstance.howthis, 'step': expertinstance.currentstatus}
            if expertinstance.howthis == '':
                data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': expertinstance.whythis, 'howthisanswer': 'اطلاعاتی برای نمایش وجود ندارد', 'step': expertinstance.currentstatus}
            data['user'] = request.user.get_username()
            print('in expert_contract_byajax--post data to view:'+str(data))
            list_key_value = [[k, v] for k, v in data.items()]
            return JsonResponse(list_key_value, safe=False)
            # return HttpResponse('with answer')
        else:
            return HttpResponse('انتخاب کاربر شناسایی نگردید!! لطفا با برگست به عقب مجددا سعی نمایید.')
    else:
        print('not ajax')
        # print('rg:'+str(request))
        # print('ins in else:'+str(request.session.keys()))
        if 'instance' in request.session.keys():
            # expertinstance = request.session['instance']
            expertinstance = jsonpickle.decode(request.session['instance'])
            # print('in')
        else:
            expertinstance = inferenceengine()
            # print('not in')
        data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': expertinstance.whythis,
                    'howthisanswer': expertinstance.howthis, 'step': expertinstance.currentstatus}

        if 'data' in request.session:
             data = jsonpickle.decode(request.session['data'])
        # print('else:data to view in ajax:'+str(data))
        return render(request, 'ExpertSystem/expert_contract_byajax.html',data)

def rewindengine(request):
    # print('testing')
    if len(request.POST):
        # print('testing--in rewindengine--post-go to back')
        expertinstance = jsonpickle.decode(request.session['instance'])
        expertinstance.stepback()
        print('testing--in rewindengine--post--after back')
        request.session['instance'] = jsonpickle.encode(expertinstance)
        data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
            , 'whythisquestion': expertinstance.whythis, 'howthisanswer': expertinstance.howthis,
                'step': expertinstance.currentstatus}
        if expertinstance.whythis == '':
            data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': 'اطلاعاتی برای نمایش وجود ندارد',
                    'howthisanswer': expertinstance.howthis,'step': expertinstance.currentstatus}
        if expertinstance.howthis == '':
            data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
                , 'whythisquestion': expertinstance.whythis,
                'howthisanswer': 'اطلاعاتی برای نمایش وجود ندارد', 'step': expertinstance.currentstatus}
        print('data after rewind:'+str(data))
        list_key_value = [[k, v] for k, v in data.items()]
        return JsonResponse(list_key_value, safe=False)
            # return HttpResponse('with answer')
    else:
        # print('testing--in rewindengine--not post')
        # print(request)
        expertinstance = jsonpickle.decode(request.session['instance'])
        data = {'question': expertinstance.currentquestion, 'question_type': expertinstance.question_type
            , 'whythisquestion': 'ایراد در پردازش اطلاعات',
                'howthisanswer': 'ایراد در پردازش اطلاعات', 'step': expertinstance.currentstatus}
        print('data after rewind:'+str(data))
        return render(request, 'ExpertSystem/expert_contract_byajax.html', data)

def about(request):
    if request.user.is_authenticated:
        print('auth')
        user=request.user
    else:
        print('not auth')
        user='not_signed_in'

    data={'user':user}
    return render(request, 'ExpertSystem/about.html',data)

def luncher(request):
    if request.user.is_authenticated:
        # print('auth')
        user=request.user.username
    else:
        # print('not auth')
        user='not_signed_in'
    # print(user)
    freecost = Subscription.objects.get(subscription_type=Subscription._TRIAL).monthlycost()
    basiccost = Subscription.objects.get(subscription_type=Subscription._BASIC).monthlycost()
    procost = Subscription.objects.get(subscription_type=Subscription._PRO).monthlycost()
    premiumcost = Subscription.objects.get(subscription_type=Subscription._PREMIUM).monthlycost()
    customacc = Subscription.objects.get(Q(subscription_type='CUSTOM') & Q(creation_mode=Subscription._SYS_MODE))
    print(customacc)
    customcust = customacc.monthlycost()
    data = {'free':freecost, 'standard':basiccost, 'pro':procost,'premium':premiumcost,'custom':customcust,
            'curracc':customacc, 'curruser':user}
    return render(request, 'ExpertSystem/home.html',data)



def _load_data_fromfile( knowledgebase):
        # knowledgebase: folder name that Fact data and Rule data are stored in 2 seperate file
        # with file name formated like these:
        # for Facts in format "knowledgebase_facts"
        # for Rules in format "knowledgebase_rules"
        #in first we retrive all facts from file and store them in database and then
        # retrive all rules from file and store them in database
        if knowledgebase == 'Default' and Rules.objects.count() == 0 :
            return 'norule'
        if knowledgebase == 'Default' and Facts.objects.count() ==0:
            return 'nofact'
        basepath = os.path.join(os.getcwd(), 'ExpertSystem\ModelsData',knowledgebase)
        defaultpath = os.path.join(os.getcwd(), 'ExpertSystem\ModelsData')
        if knowledgebase == 'Default':
            basepath = defaultpath
            if not os._exists(os.path.join(defaultpath, 'Default_facts.csv')):
                return
            if not os._exists(os.path.join(defaultpath, 'Default_rules.csv')):
                return
        # if we choose use new data to load in system then save primary data to csv files
        else:
            # store all primary facts to a csv file by name:Default_facts.csv
            with open(os.path.join(defaultpath, 'Default_facts.csv'),'w', 1, encoding='utf-8', newline='') as csvfile:
                fieldnames = ['fact', 'fact_type']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                for fact in Facts.objects.all():
                    writer.writerow({'fact': fact.fact, 'fact_type': fact.fact_type})
            # store all primary rules to a csv file by name:Default_rules.csv
            with open(os.path.join(defaultpath, 'Default_rules.csv'),'w', 1, encoding='utf-8', newline='') as csvfile:
                fieldnames = ['condition', 'consequence', 'pub_date' ]
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                for rule in Rules.objects.all():
                    writer.writerow({'condition': rule.condition, 'consequence': rule.consequence,'pub_date': rule.pub_date})
        # in first we collect facts data from file
        with open(os.path.join(basepath, knowledgebase+'_facts.csv'), encoding='utf-8', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            Facts.objects.all().delete()
            for row in reader:
                afact = Facts(fact = row['fact'],fact_type=row['fact_type'])
                afact.save()
        # #now we collect rules data from file
        with open(os.path.join(basepath, knowledgebase + '_rules.csv'),encoding='utf-8', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            Rules.objects.all().delete()
            for row in reader:
                condition = row['condition']
                consequencedata = row['consequence']
                tmpfact = Facts.objects.get(fact__iexact=consequencedata)
                pub_data = row['pub_date']
                arule = Rules(condition=condition,consequence=tmpfact,pub_date=pub_data)
                arule.save()



