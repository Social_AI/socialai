from django.urls import  path
from . import forms, views
from plans.views import PricingView
from django.contrib.auth.decorators import login_required

app_name = 'ExpertSystem'
urlpatterns = [
    path('contractdemo', forms.initengine, name='expert_initial'),
    path('classiccontractdemo', forms.initengine_classic, name='expert_initial_classic'),
    path('update_detailfacts/', forms.update_detailfacts, name='update_detailfacts'),
    # path('', views.luncher, name = 'luncher'),
    # path('', login_required(PricingView.as_view(template_name='ExpertSystem/home.html')), name = 'luncher'),
    path('', PricingView.as_view(template_name='ExpertSystem/home.html'), name = 'luncher'),
    path("expert_contract/", forms.Creat_Expert, name='expert_contract'),
    path('ContractExpert/', forms.Creat_Expert, name='expert_contract'),
    path('expert_contract_byajax/', views.expert_contract_byajax, name='expert_contract_byajax'),
    path('rewindengine/', views.rewindengine, name='rewindengine'),
    path('update_facts/', views.update_facts, name='update_facts'),

    path('about/', views.about, name='about'),
    path('contact/', forms.contact, name='contact'),
    # path('RuleManager/', include('RuleManager.urls')),
]
